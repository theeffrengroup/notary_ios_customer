//
//  ChatCouchDBManager.swift
//  LiveM
//
//  Created by Rahul Sharma on 21/12/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

class ChatCouchDBManager {
    
    static let sharedInstance = ChatCouchDBManager()
    
    
    
    /// Method to get Chat details from Couch DB
    ///
    /// - parameter: bookingId - Particular bopoking id to get chat details
    /// - Returns: chat details saved in couch DB
    func getChatDocumentDetailsFromCouchDB(bookingId:String) -> [Any] {
        
        let doc: CBLDocument = CouchDBDocument.sharedInstance().getDocument(database: CouchDBObject.sharedInstance().database, documentId: Utility.chatDocId)
        let dic: [String: AnyObject] = doc.properties! as [String : AnyObject]
        
        if let chatArray = dic["Value"] as? [Any] {
            
            if chatArray.count > 0 {
                
                if let chatDict = chatArray[0] as? [String:Any] {
                    
                    if chatDict.index(forKey: bookingId) != nil {
                        
                        if let chatDetails = chatDict[bookingId] as? [Any] {
                            
                            return chatDetails
                        }
                    }

                }
                
            }
            
        }
        return []
        
    }
    
    
    
    /// Method to update chat details in couch DB Document
    func updateChatDetailsToCouchDBDocument(arrayOfMessages:[Any], bookingId:String) {
        
        let doc: CBLDocument = CouchDBDocument.sharedInstance().getDocument(database: CouchDBObject.sharedInstance().database, documentId: Utility.chatDocId)
        let dic: [String: AnyObject] = doc.properties! as [String : AnyObject]
        
        var chatDict:[String:Any] = [:]
        
        if let chatArray = dic["Value"] as? [Any] {
            
            if chatArray.count > 0 {
                
                if let chatDictionary = chatArray[0] as? [String:Any] {
                    
                    chatDict = chatDictionary
                }
            }
        }
        
        chatDict[bookingId] = arrayOfMessages

        let chatArray:[Any] = [chatDict]
        
        CouchDBDocument.sharedInstance().updateDocument(database: CouchDBObject.sharedInstance().database, documentId: Utility.chatDocId, documentArray: chatArray as [AnyObject])
    }
    
    /// Method to update each chat message in couch DB Document
    func updateEachChatDetailsToCouchDBDocument(message:[String:Any], bookingId:String) {
        
        let doc: CBLDocument = CouchDBDocument.sharedInstance().getDocument(database: CouchDBObject.sharedInstance().database, documentId: Utility.chatDocId)
        let dic: [String: AnyObject] = doc.properties! as [String : AnyObject]
        
        var chatDict:[String:Any] = [:]
        
        if let chatArray = dic["Value"] as? [Any] {
            
            if chatArray.count > 0 {
                
                if let chatDictionary = chatArray[0] as? [String:Any] {
                    
                    chatDict = chatDictionary
                }
            }
        }
        
        var eachBookingChatArray:[Any] = []
        
        if chatDict.index(forKey: bookingId) != nil {
                
            if let chatDetails = chatDict[bookingId] as? [Any] {
                    
                eachBookingChatArray = chatDetails
            }
        }
        
        eachBookingChatArray.append(message)
        
        chatDict[bookingId] = eachBookingChatArray
        
        let updatedChatArray:[Any] = [chatDict]
        
        CouchDBDocument.sharedInstance().updateDocument(database: CouchDBObject.sharedInstance().database, documentId: Utility.chatDocId, documentArray: updatedChatArray as [AnyObject])
    }
    
    
    
    //Remove default chat details document from couchDB
    func deleteParticularBookingChatCouchDBDocument(bookingId:String) {
        
        let doc: CBLDocument = CouchDBDocument.sharedInstance().getDocument(database: CouchDBObject.sharedInstance().database, documentId: Utility.chatDocId)
        let dic: [String: AnyObject] = doc.properties! as [String : AnyObject]
        
        var chatDict:[String:Any] = [:]
        
        if let chatArray = dic["Value"] as? [Any] {
            
            if chatArray.count > 0 {
                
                if let chatDictionary = chatArray[0] as? [String:Any] {
                    
                    chatDict = chatDictionary
                }
            }
        }
        
        if chatDict.index(forKey: bookingId) != nil {
            
            chatDict.removeValue(forKey: bookingId)
        }
        
        let chatArray:[Any] = [chatDict]
        
        CouchDBDocument.sharedInstance().updateDocument(database: CouchDBObject.sharedInstance().database, documentId: Utility.chatDocId, documentArray: chatArray as [AnyObject])
        
    }
    
    func deleteChatCouchDBDocument() {

        CouchDBDocument.sharedInstance().deleteDocument(database: CouchDBObject.sharedInstance().database, id: Utility.chatDocId)
    }
    

}

