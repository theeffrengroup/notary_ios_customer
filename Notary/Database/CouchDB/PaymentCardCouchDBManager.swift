//
//  PaymentCardCouchDBManager.swift
//  LiveM
//
//  Created by Rahul Sharma on 24/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

class PaymentCardCouchDBManager {
    
    static let sharedInstance = PaymentCardCouchDBManager()
    
    
    
    /// Method to get default card details from Couch DB
    ///
    /// - Parameter documentId: default card document id
    /// - Returns: default card details
    func getDefaultCardDocumentDetailsFromCouchDB() -> [Any] {
        
        let doc: CBLDocument = CouchDBDocument.sharedInstance().getDocument(database: CouchDBObject.sharedInstance().database, documentId: Utility.defaultCardDocId)
        let dic: [String: AnyObject] = doc.properties! as [String : AnyObject]
        
        if let dicArray = dic["Value"] as? [Any] {
            
            return dicArray
            
        } else {
            
            return []
        }
        
    }
    
    
    
    
    /// Method to update default card details in couch DB Document
    func updateDefaultCardDetailsToCouchDBDocument(defaultCard:[Any]) {
        
        CouchDBDocument.sharedInstance().updateDocument(database: CouchDBObject.sharedInstance().database, documentId: Utility.defaultCardDocId, documentArray: defaultCard as [AnyObject])
    }
    
    
    
    //Remove default card details document from couchDB
    func deletePaymentCardCouchDBDocument() {
        
        CouchDBDocument.sharedInstance().deleteDocument(database: CouchDBObject.sharedInstance().database, id: Utility.defaultCardDocId)
        
    }
}
