//
//  CouchDBManager.swift
//  LiveM
//
//  Created by Rahul Sharma on 24/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

class CouchDBManager {
    
    static let sharedInstance = CouchDBManager()
    
    
    /// Method to create Initial Couch DB Documents
    func createCouchDB() {
        
        let searchAddressDocID = UserDefaults.standard.string(forKey: COUCH_DB.SEARCH_ADDRESS_DOCUMENT_ID)
        let manageAddressDocID = UserDefaults.standard.string(forKey: COUCH_DB.SEARCH_ADDRESS_DOCUMENT_ID)
        let defaultCardDocId = UserDefaults.standard.string(forKey: COUCH_DB.DEFAULT_CARD_DOCUMENT_ID)
        let chatDocId = UserDefaults.standard.string(forKey: COUCH_DB.CHAT_DOCUMENT_ID)

        //Create Search Address Document
        if searchAddressDocID == nil {
            
            let customer = RecentDB()
            var dict = [String: AnyObject]()
            dict[customer.nameKey] = customer.customerName as AnyObject
            dict[customer.valueKey] = customer.value as AnyObject
            
            let doc: CBLDocument? = CouchDBDocument.sharedInstance().createDocument(database: CouchDBObject.sharedInstance().database, withArray: (dict))
            let docID: String = doc!.documentID
            UserDefaults.standard.set(docID, forKey: COUCH_DB.SEARCH_ADDRESS_DOCUMENT_ID)
            UserDefaults.standard.synchronize()
        }
        
        if manageAddressDocID == nil {
            
            let customer = RecentDB()
            var dict = [String: AnyObject]()
            dict[customer.nameKey] = customer.customerName as AnyObject
            dict[customer.valueKey] = customer.value as AnyObject
            
            let doc: CBLDocument? = CouchDBDocument.sharedInstance().createDocument(database: CouchDBObject.sharedInstance().database, withArray: (dict))
            let docID: String = doc!.documentID
            UserDefaults.standard.set(docID, forKey: COUCH_DB.MANAGE_ADDRESS_DOCUMENT_ID)
            UserDefaults.standard.synchronize()
        }
        
        if defaultCardDocId == nil {
            
            let customer = RecentDB()
            var dict = [String: AnyObject]()
            dict[customer.nameKey] = customer.customerName as AnyObject
            dict[customer.valueKey] = customer.value as AnyObject
            
            let doc: CBLDocument? = CouchDBDocument.sharedInstance().createDocument(database: CouchDBObject.sharedInstance().database, withArray: (dict))
            let docID: String = doc!.documentID
            UserDefaults.standard.set(docID, forKey: COUCH_DB.DEFAULT_CARD_DOCUMENT_ID)
            UserDefaults.standard.synchronize()
        }
        
        if chatDocId == nil {
            
            let customer = RecentDB()
            var dict = [String: AnyObject]()
            dict[customer.nameKey] = customer.customerName as AnyObject
            dict[customer.valueKey] = customer.value as AnyObject
            
            let doc: CBLDocument? = CouchDBDocument.sharedInstance().createDocument(database: CouchDBObject.sharedInstance().database, withArray: (dict))
            let docID: String = doc!.documentID
            UserDefaults.standard.set(docID, forKey: COUCH_DB.CHAT_DOCUMENT_ID)
            UserDefaults.standard.synchronize()
        }
        
    }

}
