//
//  MessageNotificationDelegate.swift
//  MQTT Chat Module
//
//  Created by Rahul Sharma on 31/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import MQTTClient
import FirebaseMessaging

class MQTTDelegate: NSObject, MQTTSessionManagerDelegate {
    
    let mqttOnDemandAppManager = MQTTOnDemandAppManager.sharedInstance()
    
    func handleMessage(_ data: Data!, onTopic topic: String!, retained: Bool) {
        
        do {
            guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] else { return }
            
            print("data received on topic \(String(describing: topic)) message \(json)")
            
            mqttOnDemandAppManager.getNewMessage(withData: json, in: topic)
            
        } catch let jsonError {
            
            print("Response Data Error !!!",jsonError) // if there is any error in parsing then it is going to be print here.
        }
    }
    
    
    func sessionManager(_ sessionManager: MQTTSessionManager!, didDeliverMessage msgID: UInt16) {
        print("Message delivered")
    }
    
    func messageDelivered(_ session: MQTTSession!, msgID: UInt16, topic: String!, data: Data!, qos: MQTTQosLevel, retainFlag: Bool) {
        
        DDLogDebug( "\(msgID)Message delivered")
        
        if Utility.customerId.length > 0 {
            
            session.persistence.deleteAllFlows(forClientId: Utility.customerId)
            
        } else {
            
            return
        }
    }
    
    func sessionManager(_ sessionManager: MQTTSessionManager!, didChange newState: MQTTSessionManagerState) {
        
        switch newState {
            
        case .connected:
            print("connected")
            MQTT.sharedInstance().isConnected = true
            MQTT.sharedInstance().subscrtibeToChannels()
            
        case .closed:
            
            MQTT.sharedInstance().isConnected = false
            DDLogDebug("disconnected")
        
        case .error:
            
            print("error \(String(describing: sessionManager.lastErrorCode))")
            
        default:
            MQTT.sharedInstance().isConnected = false
            DDLogDebug("disconnected")
            
        }
    }
 
}
