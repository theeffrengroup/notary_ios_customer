//
//  ZenDeskManager.swift
//  LiveM
//
//  Created by Rahul Sharma on 10/01/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

class ZenDeskManager {
    
    static var obj:ZenDeskManager? = nil
    
    class func sharedInstance() -> ZenDeskManager {
        
        if obj == nil {
            
            obj = ZenDeskManager()
        }
        
        return obj!
    }
    
    var needToShowAlert:Bool = false
    
    var alertController:UIAlertController!
    
    
    /// Method to maintain Zendesk messages from Push
    func ZendeskMessageFromPush() {
        
        if SplashLoading.obj != nil {//Checking currently showing Splash loading or not
            
            needToShowAlert = true
            
        } else {
            
            showZendeskAlertMessage()
        }
            
        
    }
    
    
    func showZendeskAlertMessage() {
        
        Helper.playLocalNotificationSound()
        
        needToShowAlert = false
        
        if Helper.getCurrentVC() is TicketsViewController {
            
//            if ChatViewController.sharedInstance().bookingId != chatMessageDetail.bookingId && chatMessageDetail.bookingId.length > 0 {
//
//                ChatViewController.sharedInstance().bookingId = chatMessageDetail.bookingId
//                ChatViewController.sharedInstance().musicianId = chatMessageDetail.musicianId
//
//                ChatViewController.sharedInstance().title = "\(ALERTS.BOOKING_FLOW.EventID)" + " : " + chatMessageDetail.bookingId
//
//                ChatViewController.sharedInstance().fetchData(val:"0")
//
//            } else {
//
//                if delegate != nil {
//
//                    delegate?.chatMessageRecievedFromMQTT(chatMessageDetails: chatMessageDetail)
//                }
//
//            }
            
        } else if Utility.sessionToken.length > 0 {
            
            // create the alert
            if alertController != nil {
                
                return
               // alertController.dismiss(animated: true, completion: nil)
            }
            
            alertController = UIAlertController(title: ALERTS.Message,
                                                message: ALERTS.ZenDeskMessage,
                                                preferredStyle: UIAlertController.Style.alert)
            
            let newAlertWindow = UIWindow(frame: UIScreen.main.bounds)
            newAlertWindow.rootViewController = UIViewController()
            newAlertWindow.windowLevel = UIWindow.Level.alert + 1
            newAlertWindow.makeKeyAndVisible()
            
            let DestructiveAction = UIAlertAction(title: ALERTS.Ok, style: UIAlertAction.Style.destructive) {
                (result : UIAlertAction) -> Void in
                
                newAlertWindow.resignKey()
                newAlertWindow.removeFromSuperview()
                self.alertController = nil
                
                print("Destructive")
            }
            
            // Replace UIAlertActionStyle.Default by UIAlertActionStyle.default
            
            let okAction = UIAlertAction(title: ALERTS.VIEW, style: UIAlertAction.Style.default) {
                (result : UIAlertAction) -> Void in
                
                newAlertWindow.resignKey()
                newAlertWindow.removeFromSuperview()
                
                self.alertController = nil
                
                self.goToZenDeskVC()
            }
            
            alertController.addAction(DestructiveAction)
            alertController.addAction(okAction)
            
//            if Helper.newAlertWindow == nil {
//
//                Helper.newAlertWindow = UIWindow(frame: UIScreen.main.bounds)
//                Helper.newAlertWindow?.rootViewController = UIViewController()
//                Helper.newAlertWindow?.windowLevel = UIWindowLevelAlert + 1
//                Helper.newAlertWindow?.makeKeyAndVisible()
//                Helper.newAlertWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
//
//
//            } else {
            
                newAlertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
//            }
            
        }
        
    }
    
    func goToZenDeskVC() {
        
        let zenDeskVC:TicketsViewController = Helper.getCurrentVC().storyboard!.instantiateViewController(withIdentifier: VCIdentifier.zenDeskVC) as! TicketsViewController
        
        Helper.getCurrentVC().navigationController?.pushViewController(zenDeskVC, animated: true)
    }
    
}
