//
//  Stripe.swift
//  LiveM
//
//  Created by Rahul Sharma on 14/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import Stripe

class StripeManager {
    
    /// Shared instance object
    static var obj:StripeManager? = nil
    
    /// Payment Textfield object reference
    var paymentTextField:StripePaymentTextField!
    
    /// Stripe PublishableKey used to publish the data to stripe server
    var stripePublishableKey:String!
    

    
    /// Shared instance object for gettting the singleton object
    class func sharedInstance() -> StripeManager {
        
        if obj == nil {
            
            obj = StripeManager()
        }
        
        return obj!
    }
    
    
    
    /// Set Default Stripe Publishable Key
    ///
    /// - Parameter publishableKey: Key used to publish the data to stripe server
    func setStripeDefaultPublishableKey(publishableKey:String) {
        
        stripePublishableKey = publishableKey
        Stripe.setDefaultPublishableKey(publishableKey)
    }
    
    
    
    /// Default PublishableKey Value
    ///
    /// - Returns: currently using stripe publish key
    func defaultPublishableKey() -> String {
        
        return stripePublishableKey
    }


    
    /// Create the Payment textfield with the required frame
    ///
    /// - Parameter frame: textfield frame value
    /// - Returns: created StripePaymentTextField
    func getPaymentTextField(frame:CGRect) -> StripePaymentTextField {
        
        paymentTextField = StripePaymentTextField()
        
        paymentTextField.setTextFieldFrame(frame:frame)
        
        return paymentTextField
    }
    
    
    
    /// Validating The Card Details
    ///
    /// - Parameters:
    ///   - cardNumber: cardNumber entered by user
    ///   - expiryMonth: expiryMonth entered by user
    ///   - expiryYear: expiryYear entered by user
    ///   - cvv: cvv enterd by user
    ///   - completion: validating result (true or false)
    func validateCardDetails(cardNumber:String,
                             expiryMonth:UInt,
                             expiryYear:UInt,
                             cvv:String,
                             completion:@escaping (_ valid:Bool) -> Void) {
        
        let cardParameters = STPCardParams()
        
        cardParameters.number =  cardNumber
        cardParameters.expMonth = expiryMonth
        cardParameters.expYear = expiryYear
        cardParameters.cvc = cvv
        
        
        if STPCardValidator.validationState(forCard: cardParameters) == STPCardValidationState.valid {
            //vani 19/11/2019
            paymentTextField.cardParams = STPPaymentMethodCardParams(cardSourceParams: cardParameters) //cardParameters;
            completion(true)
            
        }
        else{
            
             completion(false)
        }

    }

    
    
    /// Create the Stripe Card token
    ///
    /// - Parameters:
    ///   - cardParams: card details entered by user
    ///   - completion: token created from stripe server or error given by stripe server
    func createStripeCardToken(cardParams:STPPaymentMethodCardParams,
                               completion:@escaping (_ tokenId:String?,_ error:Error?) -> Void) {
        let cardParameters = STPCardParams()
        
        cardParameters.number = cardParams.number
        cardParameters.expMonth = cardParams.expMonth as! UInt
        cardParameters.expYear = cardParams.expYear as! UInt
        cardParameters.cvc = cardParams.cvc
        STPAPIClient.shared().createToken(withCard: cardParameters, completion: { (token:STPToken?, error:Error?) in
            
            if (error != nil) {
                
                completion(nil,error)
            }
            else {
                
                print("Card Token: \((token?.tokenId)!)")
                completion((token?.tokenId)!,error)
            
            }
            
        })

    }
    
    
    
    
}
