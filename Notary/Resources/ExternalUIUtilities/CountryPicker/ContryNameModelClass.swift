//
//  ContryNameModelClass.swift
//  Iserve
//
//  Created by Rahul Sharma on 12/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class ContryNameModelClass: NSObject {
    static let sharedInstance = ContryNameModelClass()
    // MARK: - Variable Deceleration -
    var countries = [[String: String]]()
    var countriesFiltered = [Country]()
    var countriesModel = [Country]()
   
    
    /// Read data from jason File
    ///
    /// - Returns: Return Dictionary of Countries
    func getCoutryDetailsFormjsonSerial() -> [[String: String]]{
        let data = try? Data(contentsOf: URL(fileURLWithPath: Bundle.main.path(forResource: "countries", ofType: "json")!))
        do {
            let parsedObject = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)
            countries = parsedObject as! [[String : String]]
        }catch{
            print("not able to parse")
        }
        return countries
    }
    override init() {
        
        super.init()
        
        /// Read data from jason File
        let data = try? Data(contentsOf: URL(fileURLWithPath: Bundle.main.path(forResource: "countries", ofType: "json")!))
        do {
            
            let parsedObject = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)
            countries = parsedObject as! [[String : String]]
            
            self.collectCountries()
            
        } catch {
            
            DDLogDebug("not able to parse")
        }
        
    }
    
    func collectCountries() {
        for country in countries  {
            let code = country["code"] ?? ""
            let name = country["name"] ?? ""
            let dailcode = country["dial_code"] ?? ""
            countriesModel.append(Country(country_code:code,dial_code:dailcode, country_name:name))
        }
    }
    func localCountry(_ searchText: String) -> Country{
        let county = countriesModel[0]
        for contry in countriesModel {
            let nameString = contry.country_code
            if nameString == searchText {
                return contry
            }
        }
        return county
    }
    
}
