//
//  ImageViewLoader.swift
//  DayRunner
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

///Custom UIImageView with Loading Indicator
@IBDesignable class ImageViewLoader: UIImageView {
    
    ///Image loader integator
    @IBInspectable var activityIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    
    ///Add activity Indicator as Center of UIImageView and Start animation
    @IBInspectable var indicator: Bool {
        get {
            return false
        } set {
            activityIndicator.style = .whiteLarge
            self.addSubview(activityIndicator)
            
            activityIndicator.translatesAutoresizingMaskIntoConstraints = false
            self.addConstraint(NSLayoutConstraint(item: activityIndicator, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0))
            self.addConstraint(NSLayoutConstraint(item: activityIndicator, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0))
            
            activityIndicator.startAnimating()
        }
    }
}
