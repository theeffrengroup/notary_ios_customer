//
//  String+Base64Encode.swift
//  Sales Paddock
//
//  Created by 3Embed on 16/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

extension String
{
        func enCodeToBase64() -> String {
        var res:String? = ""
        let utf = self.data(using: String.Encoding.utf8)
        res = utf?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue:0))
        return res!
    }
}
