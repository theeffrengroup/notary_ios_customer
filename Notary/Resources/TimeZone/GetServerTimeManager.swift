//
//  GetServerTime.swift
//  Notary
//
//  Created by 3Embed on 28/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import RxCocoa
import RxSwift

class GetServerTimeManager {
    
    static let sharedInstance = GetServerTimeManager()
    
    let rxServerTimeAPICall = ServerTimeAPI()
    let disposebag = DisposeBag()
    var appDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)

    /// Method to call get list of saved address service API
    func getServerTime() {
        
        if !NetworkHelper.sharedInstance.networkReachable() || AccessTokenRefresh.sharedInstance().isAlreadyCalled {

            return
        }
        
        if !rxServerTimeAPICall.serverTime_Response.hasObservers {
            
            rxServerTimeAPICall.serverTime_Response
                .subscribe(onNext: {response in
                    
                    self.WebServiceResponse(response: response, requestType: RequestType.GetServerTime)
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
        
        rxServerTimeAPICall.getServerTimeServiceAPICall()
        
    }

    
    
    
    //MARK - WebService Response -
    func WebServiceResponse(response:APIResponseModel, requestType:RequestType)
    {
        let serviceResponseDate = Date()

        if (response.data[SERVICE_RESPONSE.Error] != nil) {
            
            Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
            return
        }
        
        switch response.httpStatusCode
        {
        case HTTPSResponseCodes.SuccessResponse.rawValue:
            
            switch requestType
            {
            case RequestType.GetServerTime:
                
                if let currentGMTTimeStamp = response.data[SERVICE_RESPONSE.DataResponse] as? Int64 {
                    
                    appDelegate?.isDateChanged = false
                    
                    let currentDateFromServer = Date.init(timeIntervalSince1970: TimeInterval(currentGMTTimeStamp))
                    
                    let differenceTimeInterval = currentDateFromServer.timeIntervalSince(serviceResponseDate)
                    
                    print("\(differenceTimeInterval)")
                    
                    var differenceTime = differenceTimeInterval
                    
                    if differenceTimeInterval < 0 {
                        
                        differenceTime = differenceTimeInterval * -1
                    }
                    
                    if differenceTime > 60 {
                        
                        UserDefaults.standard.set(differenceTimeInterval, forKey: USER_DEFAULTS.TimeZone.differenceTimeStamp)
                        
                    } else {
                        
                        UserDefaults.standard.set(0.0, forKey: USER_DEFAULTS.TimeZone.differenceTimeStamp)
                    }
                    
                    UserDefaults.standard.synchronize()
                }
                
            default:
                break
            }
            break
            
        default:
            
            break
        }
        
    }
    
}
