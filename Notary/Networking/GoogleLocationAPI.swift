//
//  GoogleLocationAPI.swift
//  Notary
//
//  Created by 3Embed on 10/04/20.
//  Copyright © 2020 Rahul Sharma. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire
import CoreLocation
import GoogleMaps
import MapKit

class GoogleLocationAPI {
    
    let disposebag = DisposeBag()
    let location_Response = PublishSubject<GoogleLOcationResponse>()
    
    
    /// Method to call Login Service API
    ///
    /// - Parameter loginModel: Contains Login Request Details
    func locationServiceAPICall(locationCoordinates: CLLocationCoordinate2D){
        
        let strURL = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + "\(locationCoordinates.latitude),\(locationCoordinates.longitude)" + "&sensor=false&location_type=ROOFTOP&key=AIzaSyCkFIV-RxsXWDVAYn_-5lPIaGPEvD2LdEg"


        RxAlamofire
        .requestJSON(.post, strURL ,
                     parameters:nil,
                     encoding:JSONEncoding.default,
                     headers: nil)
        .subscribe(onNext: { (r, json) in
            
            print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
            
            if  let dict  = json as? [String:Any]{
                
                let statuscode:Int = r.statusCode
                self.checkResponse(responseDict: dict)
            }
            
            Helper.hidePI()
            
        }, onError: {  (error) in
            
            DDLogError("API Response \(strURL)\nError:\(error.localizedDescription)")
            
            Helper.hidePI()
            Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
            
        }).disposed(by: disposebag)

    }
    /// Method to parse Service API Response
    ///
    /// - Parameters:
    ///   - statusCode: HTTPS Response status code
    ///   - responseDict: service response dictionary
    func checkResponse(responseDict: [String:Any]){
                
//        switch statusCode {
//
//            case HTTPSResponseCodes.BadRequest.rawValue:
//
//                if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
//
//                    Helper.showAlert(head: ALERTS.Error, message: errorMessage)
//
//                }
//
//                break
//
//            case HTTPSResponseCodes.InternalServerError.rawValue:
//
//                if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
//
//                    Helper.showAlert(head: ALERTS.Error, message: errorMessage)
//
//                }
//                break
//
//            case HTTPSResponseCodes.UserLoggedOut.rawValue:
//
//                Helper.logOutMethod()
//
//                if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
//
//                    Helper.showAlert(head: ALERTS.Error, message: errorMessage)
//
//                }
//
//                break
//
//
//            default:
        if !responseDict.isEmpty {
                let responseModel:GoogleLOcationResponse!
                responseModel = GoogleLOcationResponse.init(dataResponse: responseDict)
                self.location_Response.onNext(responseModel)
        }
        else {
            
//                break
        }
    }
}
