//
//  NetworkHelper.swift
//  Trustpals
//
//  Created by 3Embed on 15/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class NetworkHelper {
    
    static let sharedInstance = NetworkHelper()
    
    
    /// Network Error
    var networkError: Error {
        
        let userInfo: [AnyHashable: Any] = [NSLocalizedDescriptionKey       : NSLocalizedString("No network available.", comment: "No network available."),
                                            NSLocalizedFailureReasonErrorKey: NSLocalizedString("Failed to connect to server.", comment: "Failed to connect to server.")]
        return NSError(domain: "", code: -57, userInfo: userInfo as? [String : Any])
    }
    
    func networkReachable() -> Bool {
    
        return (NetworkReachabilityManager()?.isReachable)!

    }
    
    
    /// Athenticate Header
    ///
    /// - Returns: Dict of Athentication
    func getAOTHHeader() -> [String: String] {
        
        // Authentication
        let sessionToken = Utility.sessionToken
        let params: [String: String] = [
                                        "authorization": (sessionToken),
                                        "lan":Helper.getLanguageCode()
                                       ]
        
        DDLogVerbose("Sessoin Token:\(sessionToken)")
        return params
        
    }
    
    
    // AceessToken Athenticate Header
    ///
    /// - Returns: Dict of Athentication
    func getAceessTokenAOTHHeader() -> [String: String] {
        
        // Authentication
        let accessToken = Utility.accessToken
        let params: [String: String] = [
                                        "authorization": (accessToken),
                                        "lan":Helper.getLanguageCode()
                                       ]
        
        DDLogVerbose("Access Token:\(accessToken)")
        return params
        
    }
    
    
    //Cancel Request
    func cancellRequest() {
        
        let sessionManager = Alamofire.SessionManager.default
        sessionManager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
            dataTasks.forEach { $0.cancel() }
            uploadTasks.forEach { $0.cancel() }
            downloadTasks.forEach { $0.cancel() }
        }
    }
    
}

