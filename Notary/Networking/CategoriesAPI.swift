//
//  CategoriesAPI.swift
//  Notary
//
//  Created by Rahul Sharma on 08/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire

class CategoriesListAPI {
    
    let disposebag = DisposeBag()
    let categoriesList_Response = PublishSubject<APIResponseModel>()
    
    
    /// Method to call Get Categories List service API
    ///
    /// - Parameters:
    ///   - pickupLat: user current address's latitude
    ///   - pickupLong: user current address's longitude
    func getCategoriesServiceAPICall(pickupLat:Double,
                                     pickupLong:Double) {
        
        if !NetworkHelper.sharedInstance.networkReachable() || AccessTokenRefresh.sharedInstance().isAlreadyCalled {
            
            return
        }
        
        let strURL = API.BASE_URL + "\(API.METHOD.GET_ALL_CATEGORIES)/\(pickupLat)/\(pickupLong)/\(Utility.iPAddress)"
        
        DDLogVerbose("Get Categories URL:- \(strURL)")
        
        
        RxAlamofire
            .requestJSON(.get, strURL ,
                         parameters:nil,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
                
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
            }).disposed(by: disposebag)
    }
        

    /// Method to parse Service API Response
    ///
    /// - Parameters:
    ///   - statusCode: HTTPS Response status code
    ///   - responseDict: service response dictionary
    func checkResponse(statusCode:Int, responseDict: [String:Any]){
        
        switch statusCode {
            
            case HTTPSResponseCodes.BadRequest.rawValue:
                
                Helper.hidePI()
                break
            
            case HTTPSResponseCodes.InternalServerError.rawValue:
                
                Helper.hidePI()
                break
            
            default:
                
                let responseModel:APIResponseModel!
                responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
                self.categoriesList_Response.onNext(responseModel)
                break
        }
    }
    
}

