//
//  SetPasswordAPI.swift
//  LiveM
//
//  Created by Rahul Sharma on 23/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire

class SetPasswordAPI {
    
    let disposebag = DisposeBag()
    let changePasswordBeforeLogin_Response = PublishSubject<APIResponseModel>()
    let changePasswordAfterLogin_Response = PublishSubject<APIResponseModel>()
    
    
    /// Method to call change password service API Before Login
    ///
    /// - Parameters:
    ///   - reEnterPasswordText: new password to change
    ///   - userId: user id of the customer to change password
    func changePasswordBeforeLoginServiceAPICall(reEnterPasswordText:String,
                                                 userId:String) {
        
        let strURL = API.BASE_URL + API.METHOD.CHANGE_PASSWORD
        
        let requestParams: [String: Any] = [
            
            SERVICE_REQUEST.Change_Password  : reEnterPasswordText ,
            SERVICE_REQUEST.OTP.userType     : "1" ,
            SERVICE_REQUEST.OTP.userId       : userId
        ]
        
        
        
        Helper.showPI(_message: PROGRESS_MESSAGE.ChangingPassword)
        
        RxAlamofire
            .requestJSON(.patch, strURL ,
                         parameters:requestParams,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.changePassword)
                    
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    
    
    
    /// Method to call change password service API After Login
    ///
    /// - Parameters:
    ///   - reEnterPasswordText: new password to change
    ///   - oldPasswordText: user's old password to change
    func changePasswordAfterLoginServiceAPICall(reEnterPasswordText:String,
                                                 oldPasswordText:String) {
        
        let strURL = API.BASE_URL + API.METHOD.UPDATE_PASSWORD
        
        let requestParams: [String: Any] = [
            
            SERVICE_REQUEST.Old_Password  : oldPasswordText ,
            SERVICE_REQUEST.New_Password  : reEnterPasswordText
        ]
        
        
        Helper.showPI(_message: PROGRESS_MESSAGE.ChangingPassword)
        
        RxAlamofire
            .requestJSON(.patch, strURL ,
                         parameters:requestParams,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.updatePassword)
                    
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }

    
    /// Method to parse Service API Response
    ///
    /// - Parameters:
    ///   - statusCode: HTTPS Response status code
    ///   - responseDict: service response dictionary
    ///   - requestType: service Request type
    func checkResponse(statusCode:Int, responseDict: [String:Any], requestType:RequestType){
        
        switch statusCode {
            
        case HTTPSResponseCodes.BadRequest.rawValue:
            
            Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
            break
            
        case HTTPSResponseCodes.InternalServerError.rawValue:
            
            Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
            break
            
        case HTTPSResponseCodes.UserLoggedOut.rawValue:
            
            Helper.logOutMethod()
            if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage)
                
            }
            break
            
            
        default:
            
            let responseModel:APIResponseModel!
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            
            switch requestType {
                
            case .changePassword:
                
                self.changePasswordBeforeLogin_Response.onNext(responseModel)
                
            case .updatePassword:
                
                self.changePasswordAfterLogin_Response.onNext(responseModel)
                
            default:
                break
            }
            
            break
        }
    }
    
}
