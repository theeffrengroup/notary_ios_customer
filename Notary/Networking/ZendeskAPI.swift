//
//  PostTicketZendesk.swift
//  Zendesk
//
//  Created by Vengababu Maparthi on 26/12/17.
//  Copyright © 2017 Vengababu Maparthi. All rights reserved.
//


import UIKit
import RxSwift
import RxCocoa
import RxAlamofire
import CocoaLumberjack

class ZendeskAPI:NSObject {
    
    let disposebag = DisposeBag()
    let Zendesk_Response = PublishSubject<APIResponseModel>()
    

    /// creating new ticket
    ///
    /// - Parameter paramDict:subject, comment, priority
    func postTheNewTicket(paramDict: [String:Any]){
        
        let strURL = API.BASE_URL + API.METHOD.CreateTicket
        
        Helper.showPI(_message: PROGRESS_MESSAGE.CreatingTicket)

        
        RxAlamofire
            .requestJSON(.post, strURL,parameters:paramDict, headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                Helper.hidePI()
                if  let dict  = json as? [String:Any]{
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
            }, onError: {  (error) in
                
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                self.Zendesk_Response.onError(error)
                
            }).disposed(by: disposebag)
    }
    
    
    ///get the tickets data using request id or email id
    func getTheTicketData(id: String){
        
        let strURL = API.BASE_URL + API.METHOD.GetTicketDetails + id
        
        Helper.showPI(_message: PROGRESS_MESSAGE.GettingTicket)

        
        RxAlamofire
            .requestJSON(.get, strURL, headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                Helper.hidePI()
                if  let dict  = json as? [String:Any]{
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
            }, onError: {  (error) in
                
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                self.Zendesk_Response.onError(error)
                
            }).disposed(by: disposebag)
    }
    
    
    /// get the ticketid data using ticket id
    ///
    /// - Parameter id: ticket id
    func getTheTicketHistory(id: String){

        let strURL = API.BASE_URL + API.METHOD.GetTicketHistoryDetails + id

        Helper.showPI(_message: PROGRESS_MESSAGE.Loading)

        
        RxAlamofire
            .requestJSON(.get, strURL, headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                Helper.hidePI()
                if  let dict  = json as? [String:Any]{
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
            }, onError: {  (error) in
                
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                self.Zendesk_Response.onError(error)
                
            }).disposed(by: disposebag)
    }
    
    
    
    /// update to new comment to a ticket with id and requestid
    ///
    /// - Parameter paramDict: new comments for ticketid
    func postTheNewTicketComment(paramDict: [String:Any]){
        
        let strURL = API.BASE_URL + API.METHOD.CreateTicketComments

        Helper.showPI(_message: PROGRESS_MESSAGE.SendingTicket)
        
        RxAlamofire
            .requestJSON(.put, strURL,parameters:paramDict, headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                Helper.hidePI()
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
            }, onError: {  (error) in
                
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                self.Zendesk_Response.onError(error)
                
            }).disposed(by: disposebag)
    }
    
    
    /// Method to parse Service API Response
    ///
    /// - Parameters:
    ///   - statusCode: HTTPS Response status code
    ///   - responseDict: service response dictionary
    func checkResponse(statusCode:Int, responseDict: [String:Any]){
        
        switch statusCode {
            
        case HTTPSResponseCodes.BadRequest.rawValue:
            
            Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
            break
            
        case HTTPSResponseCodes.InternalServerError.rawValue:
            
            Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
            break
            
        case HTTPSResponseCodes.UserLoggedOut.rawValue:
            
            Helper.logOutMethod()
            if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage)
                
            }
            break
            
            
        default:
            
            let responseModel:APIResponseModel!
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            
            self.Zendesk_Response.onNext(responseModel)
            
            break
        }
    }
    
}



