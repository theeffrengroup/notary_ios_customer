//
//  AddressAPI.swift
//  LiveM
//
//  Created by Rahul Sharma on 24/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire

class AddressAPI {
    
    let disposebag = DisposeBag()
    let getAddressList_Response = PublishSubject<APIResponseModel>()
    let deleteAddress_Response = PublishSubject<APIResponseModel>()
    let addAddress_Response = PublishSubject<APIResponseModel>()
    let updateAddress_Response = PublishSubject<APIResponseModel>()
    
    
    
    /// Method to call get list of addresses service API
    func getListOfAddressesServiceAPICall(){
        
        let strURL = API.BASE_URL + API.METHOD.GET_ADDRESS
        
        RxAlamofire
            .requestJSON(.get, strURL ,
                         parameters:nil,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.GetAddress)
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    
    
    /// Method to call delete particular address Sercvice API
    ///
    /// - Parameter addressId: address Id to delete
    func deleteAddressServiceAPICall(addressId:String){
        
        let strURL = API.BASE_URL + API.METHOD.DELETE_ADDRESS + "/\(addressId)"
        
        Helper.showPI(_message: PROGRESS_MESSAGE.Deleting)
        
        RxAlamofire
            .requestJSON(.delete, strURL ,
                         parameters:nil,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.deleteAddress)
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    
    
    
    /// Method to call add new address service API
    ///
    /// - Parameter addressRequestModel: model contains address details
    func addAddressServiceAPICall(addressRequestModel:AddressAPIRequestModel) {
        
        let strURL = API.BASE_URL + API.METHOD.ADD_ADDRESS
        
        let requestParams: [String: Any] = [
            
            SERVICE_REQUEST.Address.address1:addressRequestModel.addressLine1,
            SERVICE_REQUEST.Address.address2:addressRequestModel.addressLine2,
            SERVICE_REQUEST.Address.city:addressRequestModel.city,
            SERVICE_REQUEST.Address.state:addressRequestModel.state,
            SERVICE_REQUEST.Address.country:addressRequestModel.country,
            SERVICE_REQUEST.Address.pincode:addressRequestModel.pincode,
            SERVICE_REQUEST.Address.latitude:addressRequestModel.latitude,
            SERVICE_REQUEST.Address.longitude:addressRequestModel.longitude,
            SERVICE_REQUEST.Address.taggedAs:addressRequestModel.addressType,
            SERVICE_REQUEST.Address.userType:Utility.userType,
        ]

        
        
        Helper.showPI(_message: PROGRESS_MESSAGE.Adding)
        
        RxAlamofire
            .requestJSON(.post, strURL ,
                         parameters:requestParams,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.addAddress)
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }

    
    /// Method to call update address details service API
    ///
    /// - Parameter addressRequestModel: model contains address details
    func updateAddressesServiceAPICall(addressRequestModel:AddressAPIRequestModel) {
        
        let strURL = API.BASE_URL + API.METHOD.UPDATE_ADDRESS
        
        let requestParams: [String: Any] = [
            
            SERVICE_REQUEST.Address.address1:addressRequestModel.addressLine1,
            SERVICE_REQUEST.Address.address2:addressRequestModel.addressLine2,
            SERVICE_REQUEST.Address.city:addressRequestModel.city,
            SERVICE_REQUEST.Address.state:addressRequestModel.state,
            SERVICE_REQUEST.Address.country:addressRequestModel.country,
            SERVICE_REQUEST.Address.pincode:addressRequestModel.pincode,
            SERVICE_REQUEST.Address.latitude:addressRequestModel.latitude,
            SERVICE_REQUEST.Address.longitude:addressRequestModel.longitude,
            SERVICE_REQUEST.Address.taggedAs:addressRequestModel.addressType,
            SERVICE_REQUEST.Address.userType:Utility.userType,
            SERVICE_REQUEST.Address.addressId:addressRequestModel.addressId
        ]

        
        Helper.showPI(_message: PROGRESS_MESSAGE.Saving)
        
        RxAlamofire
            .requestJSON(.patch, strURL ,
                         parameters:requestParams,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.UpdateAddress)
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }


    
    /// Method to parse Service API Response
    ///
    /// - Parameters:
    ///   - statusCode: HTTPS Response status code
    ///   - responseDict: service response dictionary
    ///   - requestType: service Request type
    func checkResponse(statusCode:Int, responseDict: [String:Any], requestType:RequestType){
        
        switch statusCode {
            
        case HTTPSResponseCodes.BadRequest.rawValue:
            
            Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
            break
            
        case HTTPSResponseCodes.InternalServerError.rawValue:
            
            Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
            break
            
        case HTTPSResponseCodes.UserLoggedOut.rawValue:
            
            Helper.logOutMethod()
            
            if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage)
                
            }
            
            break
            
            
        default:
            
            let responseModel:APIResponseModel!
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            
            switch requestType {
                
                case .GetAddress:
                    self.getAddressList_Response.onNext(responseModel)
                
                case .deleteAddress:
                    self.deleteAddress_Response.onNext(responseModel)
                
                case .addAddress:
                    self.addAddress_Response.onNext(responseModel)
                
                case .UpdateAddress:
                    self.updateAddress_Response.onNext(responseModel)
                
                default:
                    break
            }
            
            break
        }
    }
    
}
