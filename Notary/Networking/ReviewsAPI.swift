//
//  ReviewsAPI.swift
//  LiveM
//
//  Created by Rahul Sharma on 25/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire

class ReviewsAPI {
    
    let disposebag = DisposeBag()
    let getReviewsList_Response = PublishSubject<APIResponseModel>()
    
    
    /// Method to call get list of reviews reviewd by provider service API
    ///
    /// - Parameters:
    ///   - methodName: name of the method
    ///   - pageNo: current page number to get reviews
    func getReviewsListServiceAPICall(methodName:String,pageNo:Int) {
        
        let strURL = API.BASE_URL + methodName
        
        if pageNo == 0 {
            
            Helper.showPI(_message: PROGRESS_MESSAGE.Loading)
        }
        
        RxAlamofire
            .requestJSON(.get, strURL,
                         parameters:nil,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.GetReviews)
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
                Helper.hidePI()
//                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                self.getReviewsList_Response.onError(error)
                
                
            }).disposed(by: disposebag)
        
        
    }
    
    
    /// Method to parse Service API Response
    ///
    /// - Parameters:
    ///   - statusCode: HTTPS Response status code
    ///   - responseDict: service response dictionary
    ///   - requestType: service Request type
    func checkResponse(statusCode:Int, responseDict: [String:Any], requestType:RequestType){
        
        switch statusCode {
            
            case HTTPSResponseCodes.BadRequest.rawValue,HTTPSResponseCodes.InternalServerError.rawValue:
                
                if requestType == .GetReviews {
                    
                    let responseModel:APIResponseModel!
                    responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
                    self.getReviewsList_Response.onNext(responseModel)
                    
                } else {
                    
                     Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
                }
                
                break
                
            
            case HTTPSResponseCodes.UserLoggedOut.rawValue:
                
                Helper.logOutMethod()
                if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
                    
                    Helper.showAlert(head: ALERTS.Error, message: errorMessage)
                    
                }
                break
                
                
            default:
                
                let responseModel:APIResponseModel!
                responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
                
                switch requestType {
                    
                    case .GetReviews:
                        
                        self.getReviewsList_Response.onNext(responseModel)
                        
                    default:
                        break
                }
                
                break
            }
    }
    
}
