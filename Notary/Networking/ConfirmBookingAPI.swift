//
//  ConfirmBookingAPI.swift
//  LiveM
//
//  Created by Rahul Sharma on 24/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire

class ConfirmBookingAPI {
    
    let disposebag = DisposeBag()
    let liveBooking_Response = PublishSubject<APIResponseModel>()
    let scheduleBooking_Response = PublishSubject<APIResponseModel>()
    let customerLastDue_Response = PublishSubject<APIResponseModel>()
    let validatePromocode_Response = PublishSubject<APIResponseModel>()
    
    
    /// Method to call Live Booking Service API
    ///
    /// - Parameter confirmBookingModel: model contails livebooking request details
    func liveBookingServiceAPICall(confirmBookingModel:ConfirmBookingModel){
        
        let strURL = API.BASE_URL + API.METHOD.LIVE_BOOKING
        
        
        var requestParams = [
            
            SERVICE_REQUEST.LiveBooking.bookingModel:confirmBookingModel.bookingModel.rawValue,
            SERVICE_REQUEST.LiveBooking.bookingType:confirmBookingModel.appointmentLocationModel.bookingType.rawValue,
            SERVICE_REQUEST.LiveBooking.address1:confirmBookingModel.appointmentLocationModel.pickupAddress,
            SERVICE_REQUEST.LiveBooking.latitude:confirmBookingModel.appointmentLocationModel.pickupLatitude,
            SERVICE_REQUEST.LiveBooking.longitude:confirmBookingModel.appointmentLocationModel.pickupLongitude,
            SERVICE_REQUEST.LiveBooking.categoryId:confirmBookingModel.categoryId,
            SERVICE_REQUEST.LiveBooking.cartId:confirmBookingModel.cartId,
            SERVICE_REQUEST.LiveBooking.jobDescription:"",
            SERVICE_REQUEST.LiveBooking.deviceDateAndTime:TimeFormats.currentDateTime,
            
        ] as [String : Any]
        
        if confirmBookingModel.paymentmethodTag == 0 {
            
            requestParams[SERVICE_REQUEST.LiveBooking.paymentMethod] = 2
            
            if confirmBookingModel.selectedCardModel != nil {
                
                requestParams[SERVICE_REQUEST.LiveBooking.paymentCardId] = confirmBookingModel.selectedCardModel.id
            }
            
        } else {
            
            requestParams[SERVICE_REQUEST.LiveBooking.paymentMethod] = 1
        }
        
        if confirmBookingModel.appointmentLocationModel.bookingType == BookingType.Schedule {
            
            let dateformat = DateFormatter.initTimeZoneDateFormat()
            dateformat.dateFormat = "yyyy-MM-dd HH:mm:ss"
            //send time in int64
            let scheduleTime = Int64(confirmBookingModel.appointmentLocationModel.scheduleDate.timeIntervalSince1970)
//            requestParams["bookingDate"] = "\(confirmBookingModel.appointmentLocationModel.scheduleDate.timeIntervalSince1970)"
            requestParams["bookingDate"] = "\(scheduleTime)"
            //dateformat.string(from: confirmBookingModel.appointmentLocationModel.scheduleDate)
            requestParams["scheduleTime"] = "45"//"\((confirmBookingModel.appointmentLocationModel.selectedEventStartTag + 1) * 30)"
            
        }
        
        if confirmBookingModel.bookingModel == .MarketPlace {
            
            requestParams[SERVICE_REQUEST.ScheduleBooking.providerId] = confirmBookingModel.providerModel.providerId
            
        } else {
            
            requestParams[SERVICE_REQUEST.ScheduleBooking.providerId] = ""
        }
        
        if confirmBookingModel.promoCodeText.length > 0{
            
            requestParams[SERVICE_REQUEST.LiveBooking.promoCode] = confirmBookingModel.promoCodeText
            
        }
        
//        else {
        
//            requestParams["eventStartTime"] = (confirmBookingModel.selectedEventStartTag + 1) * 30
//        }

        
        Helper.showPI(_message: PROGRESS_MESSAGE.ConfirmBooking)
        
        RxAlamofire
            .requestJSON(.post, strURL ,
                         parameters:requestParams,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.liveBooking)
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    
    
    /// Method to call Schedule Booking Service API
    ///
    /// - Parameter confirmBookingModel: model contails livebooking request details
    func scheduleBookingServiceAPICall(confirmBookingModel:ConfirmBookingModel){
        
        let strURL = API.BASE_URL + API.METHOD.SCHEDULE_BOOKING
        
        let dateformat = DateFormatter.initTimeZoneDateFormat()
        dateformat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let scheduleTime = Int64(confirmBookingModel.appointmentLocationModel.scheduleDate.timeIntervalSince1970)
        var requestParams = [
            
            SERVICE_REQUEST.ScheduleBooking.latitude:confirmBookingModel.appointmentLocationModel.pickupLatitude,
            SERVICE_REQUEST.ScheduleBooking.longitude:confirmBookingModel.appointmentLocationModel.pickupLongitude,
            /*
             send date in int64
             */
//            SERVICE_REQUEST.ScheduleBooking.scheduleDate: "\(confirmBookingModel.appointmentLocationModel.scheduleDate.timeIntervalSince1970)",
            SERVICE_REQUEST.ScheduleBooking.scheduleDate: "\(scheduleTime)",
            
            SERVICE_REQUEST.ScheduleBooking.deviceDateAndTime:TimeFormats.currentDateTime,
            
            SERVICE_REQUEST.ScheduleBooking.scheduleTime:"45",
            /*
            Cart id is missing
            owner : vani
            Date : 26/02/2020
            Trello bug ID: N6QPBANL
            */
            SERVICE_REQUEST.LiveBooking.cartId:confirmBookingModel.cartId,
            
            ] as [String : Any]
        
        if confirmBookingModel.bookingModel == .MarketPlace {
            
            requestParams[SERVICE_REQUEST.ScheduleBooking.providerId] = confirmBookingModel.providerModel.providerId

        } else {
            
            requestParams[SERVICE_REQUEST.ScheduleBooking.providerId] = ""
        }
        
        
        Helper.showPI(_message: PROGRESS_MESSAGE.Loading)
        
        RxAlamofire
            .requestJSON(.post, strURL ,
                         parameters:requestParams,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.ScheduleBookingDetails)
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    
    
    /// Method to Get Customer Last dues
    ///
    /// - Parameter confirmBookingModel: model contails livebooking request details
    func getCustomerLastDuesServiceAPICall(confirmBookingModel:ConfirmBookingModel){
        
        let strURL = API.BASE_URL + API.METHOD.LAST_DUES
       
        Helper.showPI(_message: PROGRESS_MESSAGE.Loading)
        
        RxAlamofire
            .requestJSON(.get, strURL ,
                         parameters:nil,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.CustomerLastDues)
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    
    /// Method to Validate Promocode Service API
    ///
    /// - Parameter confirmBookingModel: model contails livebooking request details
    func validatePromocodeServiceAPICall(confirmBookingModel:ConfirmBookingModel,promoCode:String){
        
        let strURL = API.BASE_URL + API.METHOD.VALIDATE_PROMO_CODE
        
        var requestParams = [
            
            SERVICE_REQUEST.Promocode.promoCode:promoCode,
            SERVICE_REQUEST.Promocode.latitude:confirmBookingModel.appointmentLocationModel.pickupLatitude,
            SERVICE_REQUEST.Promocode.longitude:confirmBookingModel.appointmentLocationModel.pickupLongitude,
            SERVICE_REQUEST.Promocode.cartId:confirmBookingModel.cartId,
            
            ] as [String : Any]
        
        if confirmBookingModel.paymentmethodTag == 0 {
            
            requestParams[SERVICE_REQUEST.Promocode.paymentMethod] = 2
            
        } else {
            
            requestParams[SERVICE_REQUEST.Promocode.paymentMethod] = 1
        }
        
        
        Helper.showPI(_message: PROGRESS_MESSAGE.ValidatingPromoCode)
        
        RxAlamofire
            .requestJSON(.post, strURL ,
                         parameters:requestParams,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.ValidatePromoCode)
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    
    /// Method to parse Service API Response
    ///
    /// - Parameters:
    ///   - statusCode: HTTPS Response status code
    ///   - responseDict: service response dictionary
    ///   - requestType: service Request type
    func checkResponse(statusCode:Int, responseDict: [String:Any], requestType:RequestType){
        
        switch statusCode {
            
        case HTTPSResponseCodes.BadRequest.rawValue:
            
            Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
            break
            
        case HTTPSResponseCodes.InternalServerError.rawValue:
            
            Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
            break
            
        case HTTPSResponseCodes.UserLoggedOut.rawValue:
            
            Helper.logOutMethod()
            if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage)
                
            }
            break
            
            
        default:
            
            let responseModel:APIResponseModel!
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            
            switch requestType {
                
                case .liveBooking:
                    
                    self.liveBooking_Response.onNext(responseModel)
                
                case .ScheduleBookingDetails:
                    
                    self.scheduleBooking_Response.onNext(responseModel)
                
                case .CustomerLastDues:
                    
                    self.customerLastDue_Response.onNext(responseModel)
                
                case .ValidatePromoCode:
                    
                    self.validatePromocode_Response.onNext(responseModel)
                
                default:
                    break
            }
            
            break
        }
    }
    
}
