//
//  ProfileAPI.swift
//  LiveM
//
//  Created by Rahul Sharma on 23/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire

class ProfileAPI {
    
    let disposebag = DisposeBag()
    let logout_Response = PublishSubject<APIResponseModel>()
    let getProfile_Response = PublishSubject<APIResponseModel>()
    let updateProfile_Response = PublishSubject<APIResponseModel>()
    
    
    
    /// Method to call Logout Service API
    func logoutServiceAPICall() {
        
        let strURL = API.BASE_URL + API.METHOD.LOGOUT
        
        Helper.showPI(_message: PROGRESS_MESSAGE.LogOut)
        
        RxAlamofire
            .requestJSON(.post, strURL ,
                         parameters:nil,
                         encoding:JSONEncoding.default,
                         headers:NetworkHelper.sharedInstance.getAOTHHeader() )
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.signOut)
                    
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    
    
    
    
    /// Method to call Get Current user details Service API
    func getProfileDetailsServiceAPICall() {
        
        let strURL = API.BASE_URL + API.METHOD.GET_PROFILE
        
        Helper.showPI(_message: PROGRESS_MESSAGE.Loading)
        
        RxAlamofire
            .requestJSON(.get, strURL ,
                         parameters:nil,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.getProfileData)
                    
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    
    
    
    /// Method to Call Update Profile Details service API
    ///
    /// - Parameter profileRequestModel: model contains current customer details
    func updateProfileDetailsServiceAPICall(profileRequestModel:ProfileRequestModel) {
        
        let strURL = API.BASE_URL + API.METHOD.GET_PROFILE
        
        var requestParams: [String: Any] = [
            
            SERVICE_REQUEST.FirstName : profileRequestModel.firstName,
            SERVICE_REQUEST.LastName  : profileRequestModel.lastName ,
            SERVICE_REQUEST.Genres    : profileRequestModel.generes,
            SERVICE_REQUEST.DOB       : profileRequestModel.dateOfBirth,
            SERVICE_REQUEST.ProfilePic: profileRequestModel.profilePicURL
        ]
        
        if profileRequestModel.about.length > 0 {
            
            requestParams[SERVICE_REQUEST.About] = profileRequestModel.about
        }
        
        
        Helper.showPI(_message: PROGRESS_MESSAGE.Saving)
        
        RxAlamofire
            .requestJSON(.patch, strURL ,
                         parameters:requestParams,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.updateProfileData)
                    
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    
    
    
    /// Method to parse Service API Response
    ///
    /// - Parameters:
    ///   - statusCode: HTTPS Response status code
    ///   - responseDict: service response dictionary
    ///   - requestType: service Request type
    func checkResponse(statusCode:Int, responseDict: [String:Any], requestType:RequestType){
        
        switch statusCode {
            
        case HTTPSResponseCodes.BadRequest.rawValue:
            
            Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
            break
            
        case HTTPSResponseCodes.InternalServerError.rawValue:
            
            Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
            break
            
        case HTTPSResponseCodes.UserLoggedOut.rawValue:
            
            Helper.logOutMethod()
            if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage)
                
            }
            break
            
            
        default:
            
            let responseModel:APIResponseModel!
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            
            switch requestType {
                
            case .signOut:
                
                self.logout_Response.onNext(responseModel)
                
            case .getProfileData:
                
                self.getProfile_Response.onNext(responseModel)
                
            case .updateProfileData:
                
                self.updateProfile_Response.onNext(responseModel)
                
            default:
                break
            }
            
            break
        }
    }
    
}
