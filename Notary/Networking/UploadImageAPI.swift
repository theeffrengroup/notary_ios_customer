//
//  UploadImageAPI.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 03/01/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import Foundation
import Alamofire
class ImageUploadAPI:NSObject{
    
    func requestWith(imageData: Data?, parameters: [String : Any], onCompletion: ((Bool,String) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        
        let url = "http://138.197.78.50:8009/upload"
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            for (key, value) in parameters {
                if let data = (value as AnyObject).data(using: String.Encoding.utf8.rawValue) {
                    multipartFormData.append(data, withName: key)
                }
            }
            multipartFormData.append(imageData!, withName: "photo", fileName: "image.jpg", mimeType: "image/jpeg")
        },
                         to: url,method:HTTPMethod.post,
                         headers:NetworkHelper.sharedInstance.getAOTHHeader(), encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload
                                    .validate()
                                    .responseJSON { response in
                                        switch response.result {
                                        case .success(let value):
                                            do {
                                                if let dictonary = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String:Any]{
                                                    onCompletion!(true,dictonary["url"] as! String)
                                                }
                                            } catch let error as NSError {
                                                print(error)
                                            }
                                            
                                            print("responseObject: \(value)")
                                        case .failure(let responseError):
                                            print("responseError: \(responseError)")
                                            onCompletion!(false,"")
                                        }
                                }
                            case .failure(let encodingError):
                                onCompletion!(false,"")
                                print("encodingError: \(encodingError)")
                            }
        })
    }
}
