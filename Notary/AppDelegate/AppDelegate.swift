//
//  AppDelegate.swift
//  iDeliverDriver
//
//  Created by Rahul Sharma on 21/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import CoreData
import GoogleMaps
import GooglePlaces
import Stripe
import FacebookCore
import UserNotifications
import AWSS3
import FirebaseMessaging
import FirebaseCore
import Fabric
import Crashlytics
//import CocoaLumberjack

@_exported import CocoaLumberjack

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    let defaults = UserDefaults.standard
    var keyboardDelegate: KeyboardDelegate? = nil
    var accessTokenDelegate: AccessTokeDelegate? = nil
    
    var timer = Timer()
    var isDateChanged = false

    
    //MARK: - Initial Methods -
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        isDateChanged = true
        InitializeData()
        
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        
        return true
    }
    
    
    
    //MARK: - Custom Methods -
    
    /// Method to initialize initial data (Push Notification ,FCM, Amazon-AWS, GoogleMaps, Splash Loading, Fabric-Crashlytics)
    func InitializeData() {
        
        Helper.getIPAddress()
        
        //Initialize Google Maps
        GMSServices.provideAPIKey(Google.Mapkey)
        GMSPlacesClient.provideAPIKey(Google.Mapkey)
        
        LocationManager.sharedInstance().start()
        
        //DDLog
        DDLog.add(DDASLLogger.sharedInstance)
        DDLog.add(DDTTYLogger.sharedInstance!)
        
        DDTTYLogger.sharedInstance!.colorsEnabled = true
        DDTTYLogger.sharedInstance!.setForegroundColor(#colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1), backgroundColor: nil, for: DDLogFlag.verbose)
        DDTTYLogger.sharedInstance!.setForegroundColor(#colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1), backgroundColor: nil, for: DDLogFlag.debug)
        DDTTYLogger.sharedInstance!.setForegroundColor(#colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1), backgroundColor: nil, for: DDLogFlag.error)
        DDTTYLogger.sharedInstance!.setForegroundColor(#colorLiteral(red: 0.7254902124, green: 0.4784313738, blue: 0.09803921729, alpha: 1), backgroundColor: nil, for: DDLogFlag.warning)

        
        DDLogDebug("Welcome to DDLog")

        
        if Utility.sessionToken.length > 0 {
            
            self.showSplashLoading()
//            MixPanelManager.sharedInstance.initializeMixPanel()
        }
        
        
        //Register For Keyboard Notification
        self.registerForKeyboardNotifications()
        
        //Set Navigation Properties
        UINavigationBar.appearance().backgroundColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font:UIFont.init(name: FONTS.TitilliumWebSemiBold, size: 17.5)! ,NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.2823529412, green: 0.2823529412, blue: 0.2823529412, alpha: 1)]
        
        //Method to Register Remote Notification
        registerForRemoteNotification()
        
        
        //Initialize Amazon wrapper class to upload images
        AmazonManager.sharedInstance().setConfigurationWithRegion(AWSRegionType.USEast2,
                                                                  accessKey: AmazonAccessKey,
                                                                  secretKey: AmazonSecretKey)
        
        
        //Configure FCM Push
        
        FirebaseApp.configure()
        
        Messaging.messaging().delegate = self
        
        
        
        
        
        
        //Get Device UDID
        let UUIDValue = UIDevice.current.identifierForVendor!.uuidString
        let defaults = UserDefaults.standard
        defaults.set(UUIDValue, forKey: USER_DEFAULTS.USER.DEVICEID)
        print("Device ID Generated: \(UUIDValue)")
        defaults.synchronize()
        
        //Initialize Fabric
        Fabric.with([Crashlytics.self])
        
//        NotificationCenter.default.addObserver(self, selector: #selector(timeChangedNotification(notification:)), name: NSNotification.Name.NSSystemClockDidChange, object: nil)
        
    }
    
    // Method get called user changed the system time manually.
    @objc func timeChangedNotification(notification:NSNotification){
        
        DDLogDebug("Device time has been changed...")
        isDateChanged = true
    }
    
    
    
    /// Method to switch controller from another controller
    ///
    /// - Parameters:
    ///   - viewControllerToBeDismissed: View Controller To Be Dismissed
    ///   - controllerToBePresented: controller To Be Presented
    func switchControllers(viewControllerToBeDismissed:UIViewController,controllerToBePresented:UIViewController) {
        if (viewControllerToBeDismissed.isViewLoaded && (viewControllerToBeDismissed.view.window != nil)) {
            // viewControllerToBeDismissed is visible
            //First dismiss and then load your new presented controller
            viewControllerToBeDismissed.dismiss(animated: false, completion: {
                self.window?.rootViewController?.present(controllerToBePresented, animated: true, completion: nil)
            })
        } else {
            
        }
    }
    
    
    
    //MARK: - Register For Notification Methods -
    
    /// Method to request authorization for Push Notification
    ///
    /// - Parameter completionHandler: authorization details completion block
    private func requestAuthorization(completionHandler: @escaping (_ success: Bool) -> ()) {
        // Request Authorization
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (success, error) in
                if let error = error {
                    print("Request Authorization Failed (\(error), \(error.localizedDescription))")
                }
                completionHandler(success)
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    
    /// Method to Register for Push Notification
    func registerForRemoteNotification() {
        
        if #available(iOS 10.0, *) {
            
            let center = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in
                
                //                if granted {
                DispatchQueue.main.async(execute: {() -> Void in

                    UIApplication.shared.registerForRemoteNotifications()
                //                }
                    
                })
                
            }
            
        }
        else {
            
            if Helper.SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(version: "8.0") {
                
                UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: ([.sound, .alert, .badge]), categories: nil))
                UIApplication.shared.registerForRemoteNotifications()
                
            }
            else {
                
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    
    
    //MARK: - Register Notification Delegate Methods -
    
    /// Push Notification Successfully Registered Delegate Method
    ///
    /// - Parameters:
    ///   - application: UIApplication object
    ///   - deviceToken: device Unique Id
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        Messaging.messaging().setAPNSToken(deviceToken, type: MessagingAPNSTokenType.unknown)
        
    }
    
    
    /// Push Notification Failed to Register Delegate Method
    ///
    /// - Parameters:
    ///   - application: UIApplication object
    ///   - error: failure details
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // Print the error to console (you should alert the user that registration failed)
        print("APNs registration failed: \(error)")
    }
    
    
    
    //MARK: - Push notification received Methods -
    
    /// Push Notification Recieved Delegate method
    ///
    /// - Parameters:
    ///   - application: UIApplication object
    ///   - data: data recieved in push
    func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any]) {
        
        //Parse notification payload data
        
        if !Helper.SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(version: "10.0") {
            
            self.handlePushNotification(pushNotification: data)

        }
        
    }
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        //Parse notification payload data
        self.handlePushNotification(pushNotification: response.notification.request.content.userInfo)

        
        //        return completionHandler()
    }
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        //Parse notification payload data
        self.handlePushNotification(pushNotification: notification.request.content.userInfo)
        
        //        return completionHandler([.alert,.sound, .badge])
    }
    
    
    func handlePushNotification(pushNotification:[AnyHashable : Any]) {
        
        if Utility.sessionToken.length == 0 {//Checking user session is available or not

            return
        }
        
        //Parse notification payload data
        if let pushType = pushNotification[AnyHashable("pushType")] as? String {
        
            switch Int(pushType)! {
                
                case PushType.BookingFlow.rawValue:
                    
                    if let pushData = Helper.convertToDictionary(text: pushNotification[AnyHashable("data")]! as! String)  {
                        
                        DDLogVerbose("Booking Flow Push notification received: \(pushData)")
                        
                        let pushResponseModel = BookingStatusResponseModel.init(pushbookingStatusMessage: pushData)
                        
                        BookingStatusResponseManager.sharedInstance().bookingStatusMessageFromPushOrMQTT(isFromPush: true, bookingStatusModel: pushResponseModel)
                    }
                    break
                
                case PushType.Chatting.rawValue:
                    
                    if let pushData = Helper.convertToDictionary(text: pushNotification[AnyHashable("data")]! as! String)  {
                        
                        DDLogVerbose("Chat Push notification received: \(pushData)")
                        
                        MQTTSimpleSingleChatManager.sharedInstance().chatMessageFromPushOrMQTT(chatMessage: pushData)
                    }
                    break
                
                case PushType.BannedCustomer.rawValue:
                
                    if let pushAPSData = pushNotification[AnyHashable("aps")] as? [String:Any] {
                    
                        DDLogVerbose("Banned Customer Push notification received: \(pushAPSData)")
                        
                        Helper.playLocalNotificationSound()
                        
                        Helper.logOutMethod()
                        
                        if let pushAlertData = pushAPSData["alert"] as? [String:Any] {
                            
                            if let pushAlertBody = pushAlertData["body"] as? String {
                                
                                var title = ALERTS.Message
                                
                                if let pushAlertTitle = pushAlertData["title"] as? String {
                                    
                                    title = pushAlertTitle
                                }
                                
                                Helper.alertVC(title: title, message: pushAlertBody)
                                
                            }
                        }
                        
                    
                    }
                
                
                case PushType.ZenDesk.rawValue:
                
                    if let pushData = Helper.convertToDictionary(text: pushNotification[AnyHashable("data")]! as! String){
                    
                        DDLogVerbose("ZenDesk Push notification received: \(pushData)")
                        ZenDeskManager.sharedInstance().ZendeskMessageFromPush()
                    }
                
                case PushType.Other.rawValue:
                
                    self.handleDemoPushNotifications(pushNotification: pushNotification)
                
                default:
                    
                    self.handleDemoPushNotifications(pushNotification: pushNotification)
                    break
            }
            
        } else {
            
            self.handleDemoPushNotifications(pushNotification: pushNotification)
        }
        
    }
    
    
    func handleDemoPushNotifications(pushNotification:[AnyHashable : Any]) {
        
        if let pushAPSData = pushNotification[AnyHashable("aps")] as? [String:Any] {
            
            DDLogVerbose("Demo Push notification received: \(pushAPSData)")
            
            Helper.playLocalNotificationSound()
            
            if let pushAlertData = pushAPSData["alert"] as? [String:Any] {
                
                if let pushAlertBody = pushAlertData["body"] as? String {
                    
                    var title = ALERTS.Message
                    
                    if let pushAlertTitle = pushAlertData["title"] as? String {
                        
                        title = pushAlertTitle
                    }
                    
                    Helper.alertVC(title: title, message: pushAlertBody)
                    
                }
            }
        }
    }
    
    
    //MARK: - UIApplication Default Methods -
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        //        doUpdate()
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
        //        if !Utility.sessionToken.isEmpty {
        //
        //            configure()
        //        }
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
        application.applicationIconBadgeNumber = 1
        application.applicationIconBadgeNumber = 0
        
        LocationManager.sharedInstance().start()
        
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        NotificationCenter.default.addObserver(self, selector: #selector(networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        Reach().monitorReachabilityChanges()
        
        self.checkDateChanged()
    }
    
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        if #available(iOS 10.0, *) {
            //            self.saveContext()
        } else {
            // Fallback on earlier versions
        }
    }
    
    func applicationSignificantTimeChange(_ application: UIApplication) {
        
        DDLogDebug("Device time has been changed...")
        isDateChanged = true
    }
    
    
    //MARK: - Facebook and Gmail Login Methods -
    
    func application(_ application: UIApplication,
                     open url: URL,
                     options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        //        let urlString: String = url.absoluteString
        
        //        if urlString.contains("com.googleusercontent.apps") {
        
        //            return GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        //        }else {
        
        return ApplicationDelegate.shared.application(application, open: url, options: options)
        //        }
    }
    
    
    
    //MARK: - Network Reachable Status Method -
    
    /// Network status changed delegate method
    ///
    /// - Parameter notification: Network status changed notification details
    @objc func networkStatusChanged(_ notification: Notification) {
        //  let userInfo = (notification as NSNotification).userInfo
        
        let status = Reach().connectionStatus()
        switch status {
        case .unknown, .offline:
            ReachabilityView.instance.show()
            print("Network is not reachable")
            
        case .online(.wwan):
            ReachabilityView.instance.hide()
            connectToMQTT()
            Helper.getIPAddress()
//            self.checkDateChanged()
            print("Network is rechable now")
            
        case .online(.wiFi):
            ReachabilityView.instance.hide()
            connectToMQTT()
            Helper.getIPAddress()
//            self.checkDateChanged()
            print("Network is rechable now")
        }
        
        
        
    }
    
    func checkDateChanged() {
        
        if isDateChanged {
            
            GetServerTimeManager.sharedInstance.getServerTime()
                
//            MusiciansListManager.sharedInstance().getCategoriesServiceAPI(currentLat: LocationManager.sharedInstance().latitute, currentLong: LocationManager.sharedInstance().longitude)
        }
    }
    
    
    //Mark: - MQTT Methods -
    
    
    /// Method to connect MQTT server
    func connectToMQTT() {
        
        let mqttModel = MQTT.sharedInstance()
        mqttModel.isConnected = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            mqttModel.createConnection()
        }
        
    }
    
    
        
    //MARK: - Splash Loading Methods -
    
    /// Method to show Splash loading screen
    func showSplashLoading() {
        
        Helper.showSplashLoading()
        
        self.perform(#selector(self.closeSplashLoading), with: nil, afterDelay: 30)
        
    }
    
    
    /// Method to close Splash loading screen
    @objc func closeSplashLoading(){
        
        Helper.closeSplashLoading()
    }
    
    
}

//MARK: - FCM Methods -

extension AppDelegate:MessagingDelegate {
    
    /// FCM Refresh Registration token delegate method
    ///
    /// - Parameters:
    ///   - messaging: messaging object
    ///   - fcmToken: Registered FCM token string
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        
        // NOTE: It can be nil here
        guard let contents = Messaging.messaging().fcmToken
            else {
                return
        }
        // let refreshedToken = FIRInstanceID.instanceID().token()!
        print("FCM Token Key: \(contents)")
        
        // UserDefaults.standardUserDefaults().set(contents, forKey: "deviceToken");
        // Connect to FCM since connection may have failed when attempted before having a token.
        
        connectToFcm()
        
    }
    
    
    /// FCM Notification message recieved
    ///
    /// - Parameters:
    ///   - messaging: messaging object
    ///   - remoteMessage: Registered FCM token string
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        
    }
    
    
    /// Method to connect FCM server
    func connectToFcm()
    {
        // Won't connect since there is no token
        guard Messaging.messaging().fcmToken != nil else
        {
            return;
        }
        
        defaults.set(Messaging.messaging().fcmToken, forKey: USER_DEFAULTS.TOKEN.PUSH)
        
    }
    
}




