//
//  BookingCreatedView.swift
//  Notary
//
//  Created by 3Embed on 17/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

class BookingCreatedView:UIView {
    
    
    @IBOutlet weak var checkBoxAnimateView: M13Checkbox!

    
    //MARK: - Initial Methods -
    
    static var share: BookingCreatedView? = nil
    
    static var sharedInstance: BookingCreatedView {
        
        if share == nil {
            
            share = Bundle(for: self).loadNibNamed("BookingCreatedView",
                                                   owner: nil,
                                                   options: nil)?.first as? BookingCreatedView
            
            share?.frame = (WINDOW_DELEGATE??.frame)!
            
        }
        
//        share?.showInitialDetails()
        
        return share!
    }
    
    func showInitialDetails() {
        
        checkBoxAnimateView.stateChangeAnimation = .spiral
        checkBoxAnimateView.setCheckState(.checked, animated: true)
        checkBoxAnimateView.enableMorphing = true
        checkBoxAnimateView.setMarkType(markType: .checkmark, animated: true)
        checkBoxAnimateView.boxType = .circle
    }
    
    
    
    //MARK: - UIButton Actions -
    
    func closeView() {
   
        Helper.newBookingCreatedWindow?.resignKey()
        Helper.newBookingCreatedWindow?.removeFromSuperview()
        Helper.newBookingCreatedWindow = nil
        
        self.removeFromSuperview()
        BookingCreatedView.share = nil
        
    }
    
    
}
