//
//  ServiceDescriptionView.swift
//  Notary
//
//  Created by Rahul Sharma on 14/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

class ServiceDescriptionView:UIView {

    @IBOutlet var topView: UIView!
    
    @IBOutlet var serviceNameLabel: UILabel!
    
    @IBOutlet var serviceAmountLabel: UILabel!

    @IBOutlet var serviceDescriptionLabel: UILabel!
    
    @IBOutlet var closeButton: UIButton!

    
    //MARK: - Initial Methods -

    static var obj: ServiceDescriptionView? = nil
    
    static var shared: ServiceDescriptionView {//Creating static object of ServiceDescriptionView
        if obj == nil {
            if let views = Bundle(for: self).loadNibNamed("ServiceDescription", owner: nil, options: nil) {
                obj = views.first as? ServiceDescriptionView
                obj?.frame = (WINDOW_DELEGATE??.frame)!
            }
        }
        return obj!
    }
    
    
    @IBAction func closeButtonAction(_ sender: Any) {

        topView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
        
        
        UIView.animate(withDuration: 0.6,
                       delay: 0.2,
                       options: UIView.AnimationOptions.beginFromCurrentState,
                       animations: {
                        
                        self.topView.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
                        
        }) { (finished) in
            
            self.removeFromSuperview()
            ServiceDescriptionView.obj = nil
            
        }
    }
    
}
