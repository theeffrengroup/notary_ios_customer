//
//  CheckOutVCAPICallsExt.swift
//  Notary
//
//  Created by Rahul Sharma on 13/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension CheckOutViewController {
    
    func getServicesAPI() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        self.tableView.isHidden = true
        self.checkoutBottomView.isHidden = true
        
        checkOutViewModel.CBModel = CBModel
        
        checkOutViewModel.getServicesAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.GetServices, action:0)
        }
        
    }
    
    func getCurrentCartDetails() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        checkOutViewModel.CBModel = CBModel
        
        checkOutViewModel.getCurrentCartAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.GetCurrentCartDetails, action:0)
        }
        
    }
    
    func addServicesToCart() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        checkOutViewModel.CBModel = CBModel
        
        checkOutViewModel.addOrRemoveServicesFromCartAPICall(serviceId: serviceIdToAddOrRemove,
                                                             quantity: quantityValue,
                                                             action:1) { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.AddOrRemoveServiceFromCart, action:1)
        }
        
    }
    
    func removeServicesFromCart() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        checkOutViewModel.CBModel = CBModel
        
        checkOutViewModel.addOrRemoveServicesFromCartAPICall(serviceId: serviceIdToAddOrRemove,
                                                             quantity: quantityValue,
                                                             action:2) { (statCode, errMsg, dataResp) in
                                                                
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.AddOrRemoveServiceFromCart, action:2)
        }
        
    }
    
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType, action:Int)
    {
        switch statusCode {
                
            case HTTPSResponseCodes.TokenExpired.rawValue:
                
                if let dataRes = dataResponse as? String {
                    
                    AppDelegate().defaults.set(dataRes, forKey: USER_DEFAULTS.TOKEN.ACCESS)
                    
                    self.apiTag = requestType.rawValue
                    
                    var progressMessage = PROGRESS_MESSAGE.Loading
                    
                    switch requestType {
                        
                        case .AddOrRemoveServiceFromCart:
                            
                            if action == 1 {
                                
                                self.apiTag = RequestType.AddServiceToCart.rawValue
                                progressMessage = PROGRESS_MESSAGE.AddingService
                                
                            } else {
                                
                                self.apiTag = RequestType.RemoveServiceFromCart.rawValue
                                progressMessage = PROGRESS_MESSAGE.RemovingService
                            }
                        
                        case .GetServices,.getCardDetails:
                            progressMessage = PROGRESS_MESSAGE.Loading
                        
                        default:
                            
                            break
                    }
                    
                    self.acessClass.getAcessToken(progressMessage: progressMessage)
                    
                }
                
                break
                
            case HTTPSResponseCodes.UserLoggedOut.rawValue:
                
                Helper.hidePI()
                Helper.logOutMethod()
                if errorMessage != nil {
                    
                    Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
                }
                break
            
            case HTTPSResponseCodes.Cart.CartNotFound.rawValue, HTTPSResponseCodes.Cart.CartAlreadyCreated.rawValue:
                
                arrayOfSelectedServices = []
                self.tableView.reloadData()
                self.showCartDetails()
                break
                
            case HTTPSResponseCodes.SuccessResponse.rawValue:
                
                switch requestType {
                        
                    case RequestType.GetServices:
                        
                        arrayOfSubCategories = []
                        arrayOfSubCategoriesResponse = []
                        arrayOfSelectedServices = []
                        arrayOfServicesResponse = []

                        if let subCategories = dataResponse as? [[String:Any]] {
                            
                            arrayOfSubCategoriesResponse = subCategories
                            if subCategories.count > 0 {
                                
                                getCurrentCartDetails()
                                
                                for eachSubCategory in subCategories {
                                    
                                    let subCategoriesModel = SubCategoriesModel.init(subCategoriesResponse: eachSubCategory)
                                    
                                    if let servicesArray = eachSubCategory["service"] as? [[String:Any]] {
                                        
                                        for eachServices in servicesArray {
                                            
                                            arrayOfServicesResponse.append(eachServices)
                                        }
                                        
                                    }
                                    
                                    arrayOfSubCategories.append(subCategoriesModel)
                                }
                                
                            } else {
                                
                                self.tableView.reloadData()
                                self.showCartDetails()
                            }
                            
                        } else {
                            
                            self.tableView.reloadData()
                            self.showCartDetails()
                        }
                        
                        break
                    
                    case RequestType.GetCurrentCartDetails, RequestType.AddOrRemoveServiceFromCart:
                    
                        arrayOfSelectedServices = []
                        
                        if let cartResponse = dataResponse as? [String:Any] {
                            
                            self.cartId = GenericUtility.strForObj(object: cartResponse["_id"])
                            
                            if requestType == RequestType.AddOrRemoveServiceFromCart {
                                
                                CBModel.promoCodeText = ""
                                CBModel.discountType = ""
                                CBModel.discountValue = 0.0
                            }
                            
                            if let totalAmount = cartResponse["totalAmount"] as? Int {
                                
                                self.totalCartAmount = Double(totalAmount)
                            }
                            
//                            self.currencySymbol = GenericUtility.strForObj(object: cartResponse["currencySymbol"])

                            
                            if let selectedServices = cartResponse["item"] as? [[String:Any]] {
                                
                                if selectedServices.count > 0 {
                                    
                                    for eachSelectedservice in selectedServices {
                                        
                                        if let serviceId = eachSelectedservice["serviceId"] as? String {
                                            
                                            self.removeServiceDetailsUsingServiceId(serviceId: serviceId)
                                            
                                            let serviceModel = getServiceDetailsFromServiceId(serviceId: serviceId)//ServicesModel.init(selectedServiceDetails: eachSelectedservice)
                                            
                                            let serviceStatus = GenericUtility.intForObj(object: eachSelectedservice["status"])
                                            
                                            if let serviceTotalAmount =  eachSelectedservice["amount"] as? Double {
                                                
                                                serviceModel.serviceTotalAmount = serviceTotalAmount
                                            }
                                            
                                            if serviceModel.isForQuantity {//For quantity service
                                                
                                                if let selectedQuantityValue = eachSelectedservice["quntity"] as? Int {
                                                    
                                                    serviceModel.selectedQuantity = selectedQuantityValue
                                                    
                                                    if serviceStatus == 1 {//Add Service
                                                        
                                                        serviceModel.isSelected = true
                                                        arrayOfSelectedServices.append(serviceModel)
                                                        
                                                    } else {//Remove Service
                                                        
                                                        if selectedQuantityValue > 0 {
                                                            
                                                            serviceModel.isSelected = true
                                                            arrayOfSelectedServices.append(serviceModel)

                                                        } else {
                                                            
                                                            serviceModel.isSelected = false
                                                        }
                                                        
                                                    }
                                                }
                                            } else {//For normal service
                                                
                                                if serviceStatus == 1 {//Add Service
                                                    
                                                    serviceModel.isSelected = true
                                                    arrayOfSelectedServices.append(serviceModel)
                                                    
                                                } else {//Remove Service
                                                    
                                                    serviceModel.isSelected = false
                                                }
                                            }
                                            
                                        }
                                        
                                    }
                                    
                                    self.tableView.reloadData()
                                    self.showCartDetails()
                                }
                            }
                            
                        }
                        self.tableView.reloadData()
                        self.showCartDetails()
                        
                    break
                    
                    default:
                        
                        break
                }
                
                break
                
            default:
                
                if requestType == .GetServices {
                    
                    Helper.hidePI()
                }
                
                if errorMessage != nil {
                    
                    Helper.showAlert(head: ALERTS.Error, message: errorMessage!)
                }
                
                break
        }
        
    }
    
    
}
