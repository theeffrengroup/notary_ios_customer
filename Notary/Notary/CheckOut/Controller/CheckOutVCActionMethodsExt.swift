//
//  CheckOutVCActionMethodsExt.swift
//  Notary
//
//  Created by Rahul Sharma on 13/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension CheckOutViewController {
    
    @IBAction func navigationLeftButtonAction(_ sender: Any) {
        
        TransitionAnimationWrapperClass.caTransitionAnimationType(CATransitionType.reveal.rawValue,
                                                                  subType: CATransitionSubtype.fromBottom.rawValue,
                                                                  for: (self.navigationController?.view)!,
                                                                  timeDuration: 0.3)
        
        
        if CBModel.bookingModel == .OnDemand {
            
            self.navigationController?.popToRootViewController(animated: false)
            
        } else {
            
            self.navigationController?.popViewController(animated: false)
        }
        
    }
    
    @IBAction func checkOutButtonAction(_ sender: Any) {
        
        CBModel.cartId = self.cartId
        CBModel.servicesTotalAmount = self.totalCartAmount
        
        let confirmBookingVC:ConfirmBookingScreen = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.confirmBookingVC) as! ConfirmBookingScreen
        
        confirmBookingVC.CBModel = CBModel
        confirmBookingVC.selectedServices = arrayOfSelectedServices
        confirmBookingVC.currencySymbol = currencySymbol

        self.navigationController?.pushViewController(confirmBookingVC, animated: true)
    }
    
    @objc func viewMoreButtonAction(viewMoreButton:UIButton) {
        
        let buttonPosition = viewMoreButton.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: buttonPosition) {
            
            let serviceModel = arrayOfSubCategories[indexPath.section].arrayOfServices[indexPath.row]
            
            serviceDescriptionView = ServiceDescriptionView.shared
            
            serviceDescriptionView.serviceNameLabel.text = serviceModel.serviceName
            
            serviceDescriptionView.serviceAmountLabel.text = Helper.getValueWithCurrencySymbol(data:String(format:"%.2f", serviceModel.serviceAmount), currencySymbol: serviceModel.currencySymbol)
            
            serviceDescriptionView.serviceDescriptionLabel.text = serviceModel.serviceDescription
            
            WINDOW_DELEGATE??.addSubview(serviceDescriptionView)
            
            serviceDescriptionView.topView.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
            
            
            UIView.animate(withDuration: 0.5,
                           delay: 0.0,
                           options: UIView.AnimationOptions.beginFromCurrentState,
                           animations: {
                            
                            self.serviceDescriptionView.topView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
                            
            }) { (finished) in
                
                
            }
        }
        
        
    }
    
    @objc func addOrRemoveServiceButtonAction(addOrRemoveServiceButton:UIButton) {
        
        let buttonPosition = addOrRemoveServiceButton.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: buttonPosition) {
            
            if let serviceCell = self.tableView.cellForRow(at: indexPath) as? CheckOutVCServicesTableViewCell {
                
//                let serviceModel = arrayOfSubCategories[indexPath.section].arrayOfServices[indexPath.row]
                
                self.serviceIdToAddOrRemove = serviceCell.serviceModelDetails.serviceId

                if serviceCell.addServiceButton.title(for: .normal) == "ADD" {
                    
                    self.addServicesToCart()
                    
                } else {
                    
                    self.removeServicesFromCart()
                }
            }
        }
        
    }
    
    @objc func quantityPlusButtonAction(quantityPlusButton:UIButton) {
        
        let buttonPosition = quantityPlusButton.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: buttonPosition) {
            
            if let serviceCell = self.tableView.cellForRow(at: indexPath) as? CheckOutVCServicesTableViewCell {
                
                DDLogVerbose("\(serviceCell)")

//                let serviceModel = arrayOfSubCategories[indexPath.section].arrayOfServices[indexPath.row]
                
                if (serviceCell.serviceModelDetails.selectedQuantity + 1) > serviceCell.serviceModelDetails.maxQuantity {
                    
                    Helper.showAlert(head: ALERTS.Message, message: ALERTS.CHECK_OUT.MaximumQuantity)
                } else {
                   
                    self.serviceIdToAddOrRemove = serviceCell.serviceModelDetails.serviceId
                    self.addServicesToCart()
                }
                
            }
        }
    }
    
    @objc func quantityMinusButtonAction(quantityMinusButton:UIButton) {
        
        let buttonPosition = quantityMinusButton.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: buttonPosition) {
            
            if let serviceCell = self.tableView.cellForRow(at: indexPath) as? CheckOutVCServicesTableViewCell {
                
                DDLogVerbose("\(serviceCell)")
                
//                let serviceModel = arrayOfSubCategories[indexPath.section].arrayOfServices[indexPath.row]
                
                self.serviceIdToAddOrRemove = serviceCell.serviceModelDetails.serviceId
                
                self.removeServicesFromCart()
                
            }
        }
    }
}
