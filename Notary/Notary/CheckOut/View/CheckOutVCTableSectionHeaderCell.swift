//
//  CheckOutVCTableSectionHeaderCell.swift
//  Notary
//
//  Created by Rahul Sharma on 13/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

class CheckOutVCTableSectionHeaderCell:UITableViewCell {
    
    @IBOutlet weak var topDivider: UIView!
    
    @IBOutlet weak var bottomDivider: UIView!
    
    @IBOutlet weak var headerTitleLabel: UILabel!
    
    @IBOutlet weak var headerDescriptionLabel: UILabel!
    
    
    
}
