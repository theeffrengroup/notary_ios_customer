//
//  CheckOutVCServicesTableViewCell.swift
//  Notary
//
//  Created by Rahul Sharma on 13/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

class CheckOutVCServicesTableViewCell:UITableViewCell {
    
    
    @IBOutlet weak var serviceTitleLabel: UILabel!
    
    @IBOutlet weak var serviceAmountLabel: UILabel!
    
    @IBOutlet weak var perServiceLabel: UILabel!
    
    @IBOutlet weak var addOnServiceLabel: UILabel!
    
    @IBOutlet weak var addServiceButtonFullBackView: UIView!
    
    @IBOutlet weak var addServiceButtonBackView: UIView!
    
    @IBOutlet weak var addServiceButton: UIButton!
    
    @IBOutlet weak var quantityServiceBackView: UIView!
    
    @IBOutlet weak var minusQuantityButton: UIButton!
    
    @IBOutlet weak var plusQuantityButton: UIButton!
    
    @IBOutlet weak var quantityLabel: UILabel!
    
    @IBOutlet weak var viewMoreButton: UIButton!
    
    @IBOutlet weak var divider: UIView!
    
    var serviceModelDetails:ServicesModel!
    
    func showServiceDetails(serviceModel:ServicesModel) {
        
        self.serviceModelDetails = serviceModel
        serviceTitleLabel.text = serviceModel.serviceName
        
        serviceAmountLabel.text = Helper.getValueWithCurrencySymbol(data:String(format:"%.2f", serviceModel.serviceAmount), currencySymbol: serviceModel.currencySymbol)
        
        perServiceLabel.text = serviceModel.perServiceDescription
        
        if serviceModel.isPlusOneCostAvailable {
                
            addOnServiceLabel.text = "+ " + Helper.getValueWithCurrencySymbol(data:String(format:"%.2f", serviceModel.plusOnePrice), currencySymbol: serviceModel.currencySymbol) + " After that"
            
        } else {
            
            addOnServiceLabel.text = "Average is 4-6 Signature"
        }

        self.addServiceButtonBackView.layer.borderColor = APP_COLOR.cgColor
        self.plusQuantityButton.layer.borderColor = APP_COLOR.cgColor
        self.minusQuantityButton.layer.borderColor = APP_COLOR.cgColor

        if serviceModel.isSelected {
            
            self.addServiceButton.setTitle("REMOVE", for: UIControl.State.normal)
            
            if serviceModel.isForQuantity {
                
                self.addServiceButtonBackView.isHidden = true
                self.quantityServiceBackView.isHidden = false
                self.quantityLabel.text = "\(serviceModel.selectedQuantity)"
                
            } else {
                
                self.addServiceButtonBackView.isHidden = false
                self.quantityServiceBackView.isHidden = true
                self.quantityLabel.text = "0"
            }
            
        } else {
            
            self.addServiceButton.setTitle("ADD", for: UIControl.State.normal)
            self.addServiceButtonBackView.isHidden = false
            self.quantityServiceBackView.isHidden = true
            self.quantityLabel.text = "0"
        }
        
    }
    
}
