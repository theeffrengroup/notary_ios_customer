//
//  CheckOutViewModel.swift
//  Notary
//
//  Created by Rahul Sharma on 13/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift


class CheckOutViewModel {

    var CBModel:ConfirmBookingModel!
    let disposebag = DisposeBag()
    let rxCheckOutAPI = CheckOutAPI()
    
    func getServicesAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        rxCheckOutAPI.getServicesServiceAPICall(confirmBookingModel: CBModel)
        
        if !rxCheckOutAPI.getServices_Response.hasObservers {
            
            rxCheckOutAPI.getServices_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.hidePI()
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
    }
    
    func getCurrentCartAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        rxCheckOutAPI.getCurrentCartDetailsServiceAPICall(confirmBookingModel: CBModel)
        
        if !rxCheckOutAPI.getCurrentCart_Response.hasObservers {
            
            rxCheckOutAPI.getCurrentCart_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
    }
    
    func addOrRemoveServicesFromCartAPICall(serviceId:String,
                                                   quantity:Int,
                                                   action:Int,
                                                   completion:@escaping (Int,String?,Any?) -> ()) {
        
        rxCheckOutAPI.addOrRemoveServicesFromCartServiceAPICall(confirmBookingModel: CBModel,
                                                                serviceId: serviceId,
                                                                quantity: quantity,
                                                                action: action)
        
        if !rxCheckOutAPI.addOrRemoveServiceFromCart_Response.hasObservers {
            
            rxCheckOutAPI.addOrRemoveServiceFromCart_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
    }
    
}

