//
//  FAQVCAPICallsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension SupportListViewController {
    
    //MARK - WebService Call -
    func sendServiceToSupportList() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        supportListViewModel.getSupportListAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.GetSupportDetails)
        }

    }
    
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType)
    {
        switch statusCode
        {
            case HTTPSResponseCodes.TokenExpired.rawValue:
                
                if let dataRes = dataResponse as? String {
                    
                    AppDelegate().defaults.set(dataRes, forKey: USER_DEFAULTS.TOKEN.ACCESS)
                
                    self.apiTag = requestType.rawValue
                    var progressMessage = PROGRESS_MESSAGE.Loading
                    
                    switch requestType {
                        
                        case .GetSupportDetails:
                            
                            progressMessage = PROGRESS_MESSAGE.Loading
                            
                        default:
                            
                            break
                    }
                    
                    self.acessClass.getAcessToken(progressMessage: progressMessage)
                    
                }
                
                break
                
            case HTTPSResponseCodes.UserLoggedOut.rawValue:
                
                Helper.logOutMethod()
                if errorMessage != nil {
                    
                    Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
                }
                
                break
                
            case HTTPSResponseCodes.SuccessResponse.rawValue:
                
                switch requestType
                {
                    case RequestType.GetSupportDetails:
                        
                        arrayOfSupportList = []
                        
                        if let dataRes = dataResponse as? [Any] {
                            
                            for eachSupport in dataRes {
                                
                                let supportModel = SupportDetailsModel.init(supportResponse:eachSupport)
                                arrayOfSupportList.append(supportModel)
                                
                            }
                            
                            tableView.reloadData()
                            
                        }
                        
                        break
                        
                    default:
                        break
                }
                
            default:
                
                if errorMessage != nil {
                    
                    Helper.showAlert(head: ALERTS.Error, message: errorMessage!)
                }
                
                
                break
        }
        
    }
    
}
