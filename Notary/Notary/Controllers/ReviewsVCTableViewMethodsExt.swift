//
//  ReviewsVCTableViewMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension ReviewsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if reviewsArray.count > 0 {
            
            noReviewsMessageView.isHidden = true
//            self.tableHeaderView.isHidden = false
            
        } else {
            
            noReviewsMessageView.isHidden = false
//            self.tableHeaderView.isHidden = true
        }

        return reviewsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "reviewsCell", for: indexPath)
            as! ReviewsTableViewCell
        
        cell.showReviewDetails(reviewModel:reviewsArray[indexPath.row])
        
        return cell
    }
    
}

extension ReviewsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let label = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: tableView.bounds.size.width - 75, height: 0))
        
        label.font = UIFont.init(name: FONTS.HindRegular, size: 13)
        
        label.text = reviewsArray[indexPath.row].comment
        
        let height = Helper.measureHeightLabel(label, width: SCREEN_WIDTH! - 75)
        
        return height + 65
        
    }
    
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        cell.contentView.transform = CGAffineTransform(translationX: -500, y: 0)
//        UIView.animate(withDuration: 0.6) {
//            cell.contentView.transform = .identity
//        }
    }
    
    
}
