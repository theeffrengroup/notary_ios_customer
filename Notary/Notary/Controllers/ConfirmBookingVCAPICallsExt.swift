//
//  ConfirmBookingVCAPICallsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension ConfirmBookingScreen {
    
    @objc func liveBookingAPI() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        confirmBookingViewModel.CBModel = CBModel
        
        confirmBookingViewModel.liveBookingAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.liveBooking)
        }

    }
    
    func scheduleBookingAPI() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        self.disableBookButton()
        confirmBookingViewModel.CBModel = CBModel
        
        confirmBookingViewModel.scheduleBookingAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.ScheduleBookingDetails)
        }
        
    }
    
    func customerLastDueAPI() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        confirmBookingViewModel.CBModel = CBModel
        
        confirmBookingViewModel.customerLastDueAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.CustomerLastDues)
        }
        
    }
    
    func validatePromocodeAPI() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        confirmBookingViewModel.CBModel = CBModel
        confirmBookingViewModel.promoCode = (promocodeCell.promocodeTextField?.text)!
        
        confirmBookingViewModel.validatePromocodeAPICall{ (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.ValidatePromoCode)
        }
        
    }
    
    
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType)
    {
        switch statusCode {
            
            case HTTPSResponseCodes.TokenExpired.rawValue:
                
                if let dataRes = dataResponse as? String {
                    
                    AppDelegate().defaults.set(dataRes, forKey: USER_DEFAULTS.TOKEN.ACCESS)
                    
                    self.apiTag = requestType.rawValue
                    
                    var progressMessage = PROGRESS_MESSAGE.Loading
                    
                    switch requestType {
                        
                        case .liveBooking:
                            
                            progressMessage = PROGRESS_MESSAGE.ConfirmBooking
                        
                        case .ScheduleBookingDetails,.CustomerLastDues:
                            
                            progressMessage = PROGRESS_MESSAGE.Loading
                        
                        case .ValidatePromoCode:
                            
                            progressMessage = PROGRESS_MESSAGE.ValidatingPromoCode
                            
                        default:
                            
                            break
                    }
                    
                    self.acessClass.getAcessToken(progressMessage: progressMessage)
                    
                }
                
                break
                
            case HTTPSResponseCodes.UserLoggedOut.rawValue:
                
                Helper.logOutMethod()
                if errorMessage != nil {
                    
                    Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
                }
                break
            
            case HTTPSResponseCodes.LastDue.NoDues.rawValue:
                
                self.perform(#selector(self.liveBookingAPI), with: nil, afterDelay: 0.5)

                break
                
            case HTTPSResponseCodes.SuccessResponse.rawValue:
                
                switch requestType {
                    
                    case RequestType.liveBooking:
                        
                        AppoimtmentLocationModel.sharedInstance.bookingRequested = true
                        
                        UserDefaults.standard.set(true, forKey: USER_DEFAULTS.USER.NewBookingisCreated)
                        UserDefaults.standard.synchronize()
                        
                        self.navigationController?.popToRootViewController(animated: false)
                        LeftMenuTableViewController.sharedInstance().changeViewController(LeftMenu.myEvent)
                        
                        Helper.showBookingCreated()
                        
                        break
                    
                    case .ScheduleBookingDetails:
                        
//                        if let dataRes = dataResponse as? [String:Any] {
//
//                            if let scheduleBookingAvailable = dataRes["available"] as? String {
//
//                                if scheduleBookingAvailable == "1" {
                        
                                    self.showBookButton()
//                                }
//                            }

//                        }
                        break
                    
                    case .CustomerLastDues:
                        
                        //Show Last due or call live booking API
                        DDLogVerbose("Response:\(dataResponse!)")
                        
                        if let dictDataResponse = dataResponse as? [String:Any] {
                            
                            if let lastDueAmount = dictDataResponse["lastDues"] as? Double {
                                
                                DDLogVerbose("Last Due Amount:\(lastDueAmount)")
                            }
                        }
                        
                        if errorMessage != nil {
                            
                            lastDueTitleLabel.text = errorMessage!
                            
                            lastDueAddressLabel.text = "Service to \(CBModel.appointmentLocationModel.pickupAddress)"
                            
                            lastDueDateLabel.text = TimeFormats.currentDateTimeWith12HrFormat
                        }
                        
                        self.showLastDueView()
                        
                        break
                    
                    case RequestType.ValidatePromoCode:
                        
                        if let dictDataResponse = dataResponse as? [String:Any] {
                            
                            let discAmount = GenericUtility.strForObj(object: dictDataResponse["discountAmount"])
                            self.showValidatedPromoCodeDetails(discountAmount: Double(discAmount)!)
                            
                            self.tableView.reloadData()
                            
                            if errorMessage != nil {
                                
                                Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
                            }
                            
                        }
                        
                        break
                    
                    default:
                        
                        break
                }
                
                break
                
            default:
                
                if requestType == RequestType.ValidatePromoCode {//Got Error in Validate Promocode Service API
                    
                    self.clearPromoCodeDetails()
                    self.tableView.reloadData()
                }
                
                if errorMessage != nil {
                    
                    Helper.showAlert(head: ALERTS.Error, message: errorMessage!)
                }                
                break
        }
        
    }
    
    
}
