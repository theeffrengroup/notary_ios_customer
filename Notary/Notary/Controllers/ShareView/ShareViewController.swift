//
//  ShareViewController.swift
//  LiveM
//
//  Created by Rahul Sharma on 09/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Social
import MessageUI
import FBSDKShareKit
import FacebookShare

class ShareViewController: UIViewController,MFMailComposeViewControllerDelegate {
    
    // MARK: - Outlets -
    //label
    @IBOutlet weak var codeLabel: UILabel!
    
    // View
    @IBOutlet weak var facebookbottomView: UIView!
    @IBOutlet weak var emalBottomView: UIView!
    @IBOutlet weak var messageBottomView: UIView!
    @IBOutlet weak var watsappBottomView: UIView!
    @IBOutlet weak var twitterBottomView: UIView!
    
    // MARK: - Variable Decleration -
    weak var delegate: LeftMenuProtocol?
    
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    var shareViewModel = ShareViewModel()
    let acessClass = AccessTokenRefresh.sharedInstance()
    var apiTag:Int!

    
    // MARK: - Default Class Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getReferralCode()

        Helper.editNavigationBar(navigationController!)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        setupGestureRecognizer()
//        codeLabel.text = Utility.referralCode
        acessClass.acessDelegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
      
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        acessClass.acessDelegate = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil, completion: { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            guard let vc = (self.slideMenuController()?.mainViewController as? UINavigationController)?.topViewController else {
                return
            }
            if vc.isKind(of: ShareViewController.self)  {
                self.slideMenuController()?.removeLeftGestures()
                self.slideMenuController()?.removeRightGestures()
            }
        })
    }
    
    // MARK: - Action Methods -
    
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func facebookAction(_ sender: Any) {
//        if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeFacebook)
//        {
//            let post = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
//            post?.add(UIImage(named: "Icon-App-60"))
//            post?.add(URL(string: API.AppStoreURL))
//            post?.setInitialText(String(format: ALERTS.ShareMessage, Utility.referralCode))
//            
//            self.present(post!, animated: true, completion: nil)
//            
//        } else {
//            self.showAlert(ALERTS.Error , message: ALERTS.FacebookError )
//        }
        
        
        
        /*let content = FBSDKShareLinkContent()
        
        content.contentURL = URL(string: "http://www.livem.today")
//        content.quote = String(format: SHARE_MSG.Facebook, Utility.referralCode, Appstore.AppLink)
        
        let dialog = FBSDKShareDialog()
        dialog.mode = FBSDKShareDialogMode.automatic
        dialog.fromViewController = self
        dialog.shareContent = content
        dialog.show()*/
        
        //vani 19/11/2019
//        let content = LinkShareContent.init(url: URL(string:Links.AppstoreLink)!, quote: String(format: SHARE_MSG.Facebook,Utility.referralCode, Links.AppstoreLink))
//        
//        let dialog = ShareDialog.init(content: content)
//        dialog.mode = ShareDialogMode.automatic
//        
//        do {
//            
//            try dialog.show()
//            
//        } catch let error {
//            
//            DDLogError(error.localizedDescription)
//        }
        /*
         Facebook share
         owner: Vani
         Date: 26/02/20202
         */
        let shareContent = ShareLinkContent()
        shareContent.contentURL = URL.init(string: Links.AppstoreLink)!
        shareContent.quote = Links.AppstoreLink
        
        let dialog = ShareDialog()
        dialog.shareContent = shareContent
        
        dialog.show()
    }

    
    @IBAction func emailAction(_ sender: Any) {
        configuredMailComposeViewController()
    }
    
    @IBAction func messageAction(_ sender: Any) {
        
        if MFMessageComposeViewController.canSendText() {
            
            let messageVC = MFMessageComposeViewController.init()
            
            messageVC.body = String(format: SHARE_MSG.Messenger,Utility.referralCode,Links.AppstoreLink)
            messageVC.messageComposeDelegate = self;
            self.present(messageVC, animated: false, completion: nil)
            
        } else {
            
            Helper.showAlert(head: ALERTS.Error, message: ALERTS.MessageError)

        }
        
    }
    
    @IBAction func watsAppAction(_ sender: Any) {

//        let whatsAppURL = "whatsapp://send?text=\(String(format: SHARE_MSG.Whatsapp,Utility.referralCode,Appstore.AppLink))"
        
        openWhatsappinBrowser()
        
//        if let urlString = whatsAppURL.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
//            
//            if let whatsappURL = NSURL(string: urlString) {
//                
//                if UIApplication.shared.canOpenURL(whatsappURL as URL) {
//                    
//                    UIApplication.shared.openURL(whatsappURL as URL)
//                    
//                } else {
//                    
//                    openWhatsappinBrowser()
//                    print("Please install watsapp")
//                }
//            } else {
//                
//                openWhatsappinBrowser()
//            }
//        }
    }
    
    func openWhatsappinBrowser() {
        
        let whatsAppsBrowserURLString = "https://api.whatsapp.com/send?text=\(String(format: SHARE_MSG.Whatsapp,Utility.referralCode,Links.AppstoreLink))"
        
        let escapedShareString = whatsAppsBrowserURLString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        // cast to an url
        let url = URL(string: escapedShareString)
        
        UIApplication.shared.openURL(url!)
    }

    
    
    @IBAction func twitterAction(_ sender: Any) {
        
        let tweetUrl = Links.AppstoreLink
        
        let shareString = "https://twitter.com/intent/tweet?text=\(String(format: SHARE_MSG.Twitter,Utility.referralCode, Links.AppstoreLink))&url=\(tweetUrl)"
        
        // encode a space to %20 for example
        let escapedShareString = shareString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        // cast to an url
        let url = URL(string: escapedShareString)
        
        // open in safari
        UIApplication.shared.openURL(url!)
    }
    
    
    func configuredMailComposeViewController() {
        
        if MFMailComposeViewController.canSendMail() {
            
            let mailComposerVC = MFMailComposeViewController()
            mailComposerVC.mailComposeDelegate = self
            mailComposerVC.navigationBar.isTranslucent = false
            mailComposerVC.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.3882352941, green: 0.4235294118, blue: 0.7725490196, alpha: 1),NSAttributedString.Key.backgroundColor : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)]
            mailComposerVC.navigationBar.tintColor = #colorLiteral(red: 0.3882352941, green: 0.4235294118, blue: 0.7725490196, alpha: 1)
            mailComposerVC.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            mailComposerVC.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
            mailComposerVC.setSubject("Download Notary-90210 app")
            mailComposerVC.setMessageBody(String(format: SHARE_MSG.Email, Utility.referralCode, Links.AppstoreLink), isHTML: false)
            self.present(mailComposerVC, animated: true, completion: nil)
            
        } else {
            
//            self.showSendMailErrorAlert()
            Helper.showAlert(head: ALERTS.Error, message: ALERTS.EmailError)

        }
        
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
}

extension ShareViewController:AccessTokeDelegate {
    
    func recallApi() {
        
        switch apiTag {
            
        case RequestType.GetReferralCode.rawValue:
            
            getReferralCode()
            
            break
            
        default:
            break
        }
    }
    
}

extension ShareViewController: UINavigationControllerDelegate {
    
    internal func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}

extension ShareViewController {
    
    func setupGestureRecognizer() {
        
        guard (navigationController?.viewControllers.count)! > 1 else {
            
            return
        }
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
        
    }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
            
        case .began:
            
            self.navigationController?.delegate = self
            _ = navigationController?.popViewController(animated: true)
            
        case .changed:
            
            if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                percentDrivenInteractiveTransition.finish()
            } else {
                percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
            percentDrivenInteractiveTransition.cancel()
            
        default:
            break
        }
    }
    
    
}



extension ShareViewController:MFMessageComposeViewControllerDelegate {
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
    }
}


extension ShareViewController {
    
    /// Initiall SetUP For Animations
    func initiallSetUp() {
        facebookbottomView.transform = CGAffineTransform(translationX: 0, y: 1000)
        emalBottomView.transform = CGAffineTransform(translationX: 0, y: 1000)
        messageBottomView.transform = CGAffineTransform(translationX: 0, y: 1000)
        watsappBottomView.transform = CGAffineTransform(translationX: 0, y: 1000)
        twitterBottomView.transform = CGAffineTransform(translationX: 0, y: 1000)
    }
    
    /// Initiall Animations after the view did Appear
    func initiallAnimation() {
        UIView.animate(withDuration: 0.4, delay: 0.3, usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: .allowAnimatedContent, animations: {
            self.facebookbottomView.transform = .identity
        }) { (true) in
            UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: .allowAnimatedContent, animations: {
                //                self.instagramBottomView.transform = .identity
            }) { (true) in
                UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: .allowAnimatedContent, animations: {
                    self.emalBottomView.transform = .identity
                }) { (true) in
                    UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: .allowAnimatedContent, animations: {
                        self.messageBottomView.transform = .identity
                    }) { (true) in
                        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: .allowAnimatedContent, animations: {
                            self.watsappBottomView.transform = .identity
                        }) { (true) in
                            UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: .allowAnimatedContent, animations: {
                                //                                self.snapChatBottomView.transform = .identity
                            }) { (true) in
                                UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: .allowAnimatedContent, animations: {
                                    self.twitterBottomView.transform = .identity
                                })
                            }
                        }
                    }
                }
            }
        }
    }
    
    
}


