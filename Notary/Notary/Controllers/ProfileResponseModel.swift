//
//  ProfileResponseModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 23/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation


class ProfileResponseModel {
    
    var firstName = ""
    var lastName = ""
    var email = ""
    var dateOfBirth = ""
    var countryCode = ""
    var countryCodeSymbol = ""
    var phoneNumber = ""
    var about = ""
    var profilePic = ""
//    var genres = [Any]()
    var musicGenres = ""

    
    
    
    /// Creating User Profile model from user details
    ///
    /// - Parameter profileDetails: user details
    init(profileDetails: Any) {
        
        if let profileDetail = profileDetails as? [String:Any] {
            
            firstName = GenericUtility.strForObj(object: profileDetail[SERVICE_RESPONSE.FirstName])
            lastName = GenericUtility.strForObj(object: profileDetail[SERVICE_RESPONSE.LastName])
            email = GenericUtility.strForObj(object: profileDetail[SERVICE_RESPONSE.Email])
            dateOfBirth = GenericUtility.strForObj(object: profileDetail[SERVICE_RESPONSE.Dob])
            countryCode = GenericUtility.strForObj(object: profileDetail[SERVICE_RESPONSE.CountryCode])
            countryCodeSymbol = GenericUtility.strForObj(object: profileDetail[SERVICE_RESPONSE.CountryCodeSymbol])
            phoneNumber = GenericUtility.strForObj(object: profileDetail[SERVICE_RESPONSE.PhoneNumber])
            about = GenericUtility.strForObj(object: profileDetail[SERVICE_RESPONSE.About])
            profilePic = GenericUtility.strForObj(object: profileDetail[SERVICE_RESPONSE.ProfilePic])
            
            if let musGen = profileDetail[SERVICE_RESPONSE.Genres] as? String {
                
                musicGenres = musGen
            }
            //            genres = GenericUtility.arrayForObj(object: profileDetail[SERVICE_RESPONSE.Genres]) as! [[String : Any]]
            
        }
    }
    
    
}
