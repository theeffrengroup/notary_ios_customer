//
//  MusicianDetailsVCCustomMethods.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import Kingfisher

extension MusicianDetailsViewController {
    
    func initialSetup() {
        
//        arrayOfHeaderTitles = [
//            "Gig Time",
//            "About Me",
//            "Music Genre",
//            "Events",
//            "Rules",
//            "Instruments",
//            "Reviews"
//        ]
        
//        states = [Bool](repeating: true, count: arrayOfHeaderTitles.count)

        
        /*var frame = tableHeaderView.frame
        
        switch Int(SCREEN_HEIGHT!) {
            
        case 480,568://iPhone4s & iPhone 5s
            frame.size.height = 320
            
            break
            
        case 667://iPhone7
            frame.size.height = 350
            
            break
            
        case 812://iPhoneX
            
            frame.size.height = 410

            self.navigationTopViewHeightConstraint.constant = NavigationBarHeight
            self.navigationTopView.layoutIfNeeded()
            
            break
            
        default://iPhone7Plus
            
            frame.size.height = 380
            
            break
        }*/
        
        if Helper.SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(version: "11.0") {
            
            self.tableViewTopConstraint.constant = -NavigationBarHeight
            tableView.layoutIfNeeded()
        }
        
        self.navigationTopViewHeightConstraint.constant = NavigationBarHeight
        self.navigationTopView.layoutIfNeeded()
        
//        tableHeaderHeight = frame.size.height - 120.0
        
//        tableHeaderView.frame = frame
        tableView.layoutIfNeeded()
        
        
        sendRequestTogetProviderDetails()
        
        bookButtonBackView.transform = CGAffineTransform(translationX: 0, y: 100)
        
        Helper.setShadowFor(self.navigationTopView, andWidth: SCREEN_WIDTH!, andHeight: NavigationBarHeight)
        
    }
    
    /// spring animation for bottomViews
    func springAnimation(){
        
        UIView.animate(withDuration: 0.8,
                       delay: 0.2,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 5, options: [], animations: {
                        
                        self.bookButtonBackView.transform = .identity
                        
        })
    }
    
    
    func showProviderDetails() {
        
        musicianNameLabel.text = musicianFullDetailsModel.firstName + " " + musicianFullDetailsModel.lastName
        
        navigationTitleLabel.text = musicianNameLabel.text
        
        milesLabel.text = Helper.getDistanceDependingMileageMetricFromServer(distance: providerDetailFromPrevController.distance, mileageMatric: musicianFullDetailsModel.mileageMatric)//String(format:"%.2f miles away",providerDetailFromPrevController.distance)
        
        bookButton.setTitle("BOOK \(musicianFullDetailsModel.firstName.uppercased())", for: UIControl.State.normal)
        
        
        numberOfJobsLabel.text = String(format:"%d",musicianFullDetailsModel.totalBookingCount)
        averageRatingLabel.text = String(format:"%.1f",musicianFullDetailsModel.overallRating)
        numberOfReviewsLabel.text = "\(musicianFullDetailsModel.noOfReviews)"//"\(musicianFullDetailsModel.reviewsArray.count.description) reviews"
        

//        ratView.rating = Float(musicianFullDetailsModel.overallRating)
        
        
        bookButtonBackViewHeightConstraint.constant = 60
        self.bookButtonBackView.isHidden = false
        
        if providerDetailFromPrevController.status == 0 {
            
            musicianOnlineImageView.image = #imageLiteral(resourceName: "offline_image")
            //            bookButtonBackViewHeightConstraint.constant = 15
            //            self.bookButtonBackView.isHidden = true
            
        }else {
            
            musicianOnlineImageView.image = #imageLiteral(resourceName: "online_image")
            //            bookButtonBackViewHeightConstraint.constant = 60
            self.bookButtonBackView.isHidden = false
        }
        
        if musicianFullDetailsModel.profilePic.length > 0 {
            
            musicianImageView.kf.setImage(with: URL(string: musicianFullDetailsModel.profilePic),
                                          placeholder:#imageLiteral(resourceName: "myevent_profile_default_image"),
                                          options: [.transition(ImageTransition.fade(1))],
                                          progressBlock: { receivedSize, totalSize in
            },
                                          completionHandler: { image, error, cacheType, imageURL in
                                            
            })
            
        } else {
            
            musicianImageView.image = #imageLiteral(resourceName: "myevent_profile_default_image")
        }
        
        
        bookButtonBackView.layoutIfNeeded()
        
        self.tableView.isHidden = false
        
        CBModel = createConfirmBookingModel()
        
        self.tableHeaderView.transform = CGAffineTransform(translationX: SCREEN_WIDTH! + 60, y: 0)
        
        
        states = [Bool](repeating: true, count: musicianFullDetailsModel.arrayOfMetaData.count + 1)
        
        
        UIView.animate(withDuration: 0.8,
                       delay: 0.2,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 0,
                       options: [],
                       animations: {
                        
                        self.tableHeaderView.transform = .identity
                        
        }, completion: { (completed) in
            
            self.tableView.reloadData()
            
            if self.isFromLiveTrackVC {
                
                self.bookButtonBackViewHeightConstraint.constant = 15
                self.bookButtonBackView.isHidden = true
                
            } else {
                
                self.springAnimation()
            }
            
        })
        
        if musicianFullDetailsModel.noOfReviews <= 5 {
            
            self.tableView.es.removeRefreshFooter()
        }
        
        
    }
    
    
    func createConfirmBookingModel() -> ConfirmBookingModel {
        
        let CBModel = ConfirmBookingModel.sharedInstance()
        
        var defaultCardArray:[Any] = []
        
        if !Utility.defaultCardDocId.isEmpty {
            
            defaultCardArray = PaymentCardManager.sharedInstance.getDefaultCardDocumentDetailsFromCouchDB(documentId: Utility.defaultCardDocId)
        }
        
        if defaultCardArray.count > 0 {
            
            CBModel.selectedCardModel = CardDetailsModel.init(selectedCardDetails: defaultCardArray[0])
        }
        
        CBModel.bookingModel = .MarketPlace

        CBModel.providerModel = providerDetailFromPrevController
        CBModel.providerFullDetalsModel = musicianFullDetailsModel
        
        CBModel.appointmentLocationModel = AppoimtmentLocationModel.sharedInstance
        
        return CBModel
    }
    
}
