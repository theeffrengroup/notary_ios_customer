//
//  HomeVCBookLaterMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 09/12/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

extension HomeScreenViewController {

    func showBookLaterDetails() {
        
        if appointmentLocationModel.bookingRequested {
            
            appointmentLocationModel.bookingRequested = false
            
            Helper.clearLaterBookingRequestDetails()
            hideSelectedBookLaterDetails()
            selectedDate = scheduleDate
            datePickerView.date = selectedDate
            selectedEventStartTag = 0
            
            appointmentLocationModel.pickupLatitude = locationObj.latitute
            appointmentLocationModel.pickupLongitude = locationObj.longitude
            appointmentLocationModel.pickupAddress = locationObj.address
            
            self.setTimeZonesToCalendar()
            
            self.addressLabel.text = locationObj.address
            
            musiciansListManager.checkZoneisChanged(currentLat: appointmentLocationModel.pickupLatitude, currentLong: appointmentLocationModel.pickupLongitude)
            
            self.showCurrentLocation()
            
        } else {
            
            if appointmentLocationModel.bookingType == BookingType.Schedule {
                
                selectedDate = appointmentLocationModel.scheduleDate
                selectedEventStartTag = appointmentLocationModel.selectedEventStartTag
                datePickerView.date = selectedDate
                showSelectedBookLaterDetails()
                
            } else {
                
                hideSelectedBookLaterDetails()
                selectedDate = scheduleDate
                datePickerView.date = selectedDate
                selectedEventStartTag = 0
            }
        }
        
        self.eventStartCollectionView.reloadData()
    }
    
    func updateSelectedBookLaterDetails(){
        
        if selectedDate != nil {
            
            let dateformat = DateFormatter.initTimeZoneDateFormat()
            dateformat.dateFormat = "dd MMM yyyy hh:mm a"
            
            dateformat.timeZone = Helper.getTimeZoneFromLoaction(latitude: appointmentLocationModel.pickupLatitude,
                                                                 longitude: appointmentLocationModel.pickupLongitude)
            
            self.selectedBookLaterDateLabel.text = dateformat.string(from:selectedDate)
        }
        
    }

    func showSelectedBookLaterDetails(){
        
        updateSelectedBookLaterDetails()
        self.selectedBookLaterBackView.isHidden = false
    }
    
    func hideSelectedBookLaterDetails(){
        
        self.selectedBookLaterBackView.isHidden = true
    }
    

}
