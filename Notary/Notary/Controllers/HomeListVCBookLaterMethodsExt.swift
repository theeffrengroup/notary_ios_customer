//
//  HomeListVCBookLaterMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 09/12/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

extension HomeListViewController {
    
    func showBookLaterDetails() {
        
        if appointmentLocationModel.bookingType == BookingType.Default {
            
            scheduleDate = TimeFormats.roundDate(Date.dateWithDifferentTimeInterval().addingTimeInterval(3600 * 2))
            datePickerView.minimumDate = scheduleDate
            appointmentLocationModel.scheduleDate = scheduleDate
            selectedEventStartTag = 0
            selectedDate = appointmentLocationModel.scheduleDate
            datePickerView.date = selectedDate
            hideSelectedBookLaterDetails()
            
        } else {
            
            selectedDate = appointmentLocationModel.scheduleDate
            datePickerView.date = selectedDate
            selectedEventStartTag = appointmentLocationModel.selectedEventStartTag
            showSelectedBookLaterDetails()
        }
        
        self.eventStartCollectionView.reloadData()
        
    }
    
    func updateSelectedBookLaterDetails(){
        
        if selectedDate != nil {
            
            let dateformat = DateFormatter.initTimeZoneDateFormat()
            dateformat.dateFormat = "dd MMM yyyy hh:mm a"
            
            dateformat.timeZone = Helper.getTimeZoneFromLoaction(latitude: appointmentLocationModel.pickupLatitude,
                                                                 longitude: appointmentLocationModel.pickupLongitude)
            
            self.selectedBookLaterDateLabel.text = dateformat.string(from:selectedDate)
        }
        
    }
    
    func showSelectedBookLaterDetails(){
        
        updateSelectedBookLaterDetails()
        self.selectedBookLaterBackView.isHidden = false
    }
    
    func hideSelectedBookLaterDetails(){
        
        self.selectedBookLaterBackView.isHidden = true
    }
    
    func reloadTableViewDependingOnBookingType() {
        
        self.parseBookNowAndLaterNotaries()
        //Reload TableView
        self.tableView.reloadData()
    }
    
    
}
