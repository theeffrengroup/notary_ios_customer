//
//  MDGigTimeCollectionViewCell.swift
//  LiveM
//
//  Created by Apple on 08/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

class MDGigTimeCollectionViewCell:UICollectionViewCell {
    
    @IBOutlet var timeLabel: UILabel!
    
    @IBOutlet var selectedDivider: UIView!
}
