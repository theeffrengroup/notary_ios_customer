//
//  ConfirmBookingVCTextFieldDelegatesExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension ConfirmBookingScreen:UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == promocodeCell.promocodeTextField {
            
            self.tableView.scrollToRow(at: IndexPath.init(row: 0, section: 3),
                                       at: UITableView.ScrollPosition.middle,
                                       animated: true)
        }
        
        return true
    }
    
    /**
     *   This method will called when the user textfield text is changed
     */
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if isPromoCodeValid {
            
            isPromoCodeValid = false
            self.clearPromoCodeDetails()
            self.tableView.reloadData()
        }
        
        
        var textFieldText = String()
        
        if range.length == 0 {//Entered New Character
            
            textFieldText = textField.text! + string
        }
        else {//Entered BackSpace
            
            textFieldText = String(textField.text!.dropLast())
        }
        
        promocodeCell.applyButton.setTitle("Apply", for: UIControl.State.normal)
        
        if textFieldText.length == 0 {
            
            promocodeCell.applyButton.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
            promocodeCell.applyButton.isUserInteractionEnabled = false
            
        } else {
            
            promocodeCell.applyButton.setTitleColor(APP_COLOR, for: UIControl.State.normal)
            promocodeCell.applyButton.isUserInteractionEnabled = true
        }
        
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        if (promocodeCell.promocodeTextField.text?.length)! > 0 && isPromoCodeValid == false {
            
            self.validatePromocodeAPI()
        }
        
        return true
    }
    
}
