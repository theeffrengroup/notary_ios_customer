//
//  SupportViewController.swift
//  LiveM
//
//  Created by Rahul Sharma on 25/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//   FaqToSupport

import UIKit

class SupportInnerListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var titleView: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    
    
    var arrayOfSupportList = [SupportDetailsModel]()
    var webURL: String!
    var tittle: String!
    let acessClass = AccessTokenRefresh.sharedInstance()
    var apiTag:Int!
    
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleLabel.text = tittle
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        setupGestureRecognizer()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Action methods -
    @IBAction func backAction(_ sender: Any) {

        self.navigationController?.popViewController(animated: true)
    }

}

extension SupportInnerListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrayOfSupportList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "supportCell", for: indexPath) as! SupportTableViewCell
        
        let supportDetailsModel = arrayOfSupportList[indexPath.row]
        
        cell.tittleLabel.text = supportDetailsModel.name
        
        return cell
    }
    
}

extension SupportInnerListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let supportDetailsModel = arrayOfSupportList[indexPath.row]
        
        if supportDetailsModel.innerSupport.count > 0 {
            
            let supportInnerListVC:SupportInnerListViewController = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.supportInnerVC) as! SupportInnerListViewController
            
            var arrayofSupportInnerList = [SupportDetailsModel]()
            
            for eachInnerSupport in supportDetailsModel.innerSupport {
                
                let supportModel = SupportDetailsModel.init(supportResponse:eachInnerSupport)
                arrayofSupportInnerList.append(supportModel)
                
            }
            
            supportInnerListVC.arrayOfSupportList = arrayofSupportInnerList
            supportInnerListVC.tittle = supportDetailsModel.name
            
            self.navigationController!.pushViewController(supportInnerListVC, animated: true)
            
        } else {
            
            let supportWebVC:SupportWebViewController = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.supportWebVC) as! SupportWebViewController
            
            supportWebVC.webURL = supportDetailsModel.link
            supportWebVC.navTitle = supportDetailsModel.name
            
            self.navigationController!.pushViewController(supportWebVC, animated: true)
            
        }
        

        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.contentView.transform = CGAffineTransform(translationX: -500, y: 0)
        UIView.animate(withDuration: 0.6) {
            cell.contentView.transform = .identity
        }
        
    }
    
    
    
}

extension SupportInnerListViewController: UINavigationControllerDelegate {
    
    internal func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}

extension SupportInnerListViewController {
    
    func setupGestureRecognizer() {
        
        guard (navigationController?.viewControllers.count)! > 1 else {
            
            return
        }
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
        
    }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
            
        case .began:
            
            self.navigationController?.delegate = self
            _ = navigationController?.popViewController(animated: true)
            
        case .changed:
            
            if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                percentDrivenInteractiveTransition.finish()
            } else {
                percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
            percentDrivenInteractiveTransition.cancel()
            
        default:
            break
        }
    }
    
    
}



