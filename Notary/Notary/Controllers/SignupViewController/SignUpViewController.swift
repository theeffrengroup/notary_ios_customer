//
//  SignUpViewController.swift
//  DayRunner
//
//  Created by Rahul Sharma on 23/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import GoogleSignIn
import Kingfisher
import Firebase

class SignUpViewController: UIViewController ,UIImagePickerControllerDelegate ,UINavigationControllerDelegate {
    
    var activeTextField = UITextField()
    var isConditionsAgreed = Bool()
    var addressLine1: String = ""
    var addressLine2: String = ""
    var zipCode: String = "123"
    var companyLat: Float!
    var companyLong: Float!
    var Code: String = ""
    var userData = UserDataModel()
    var profileUrl : String = ""
    var socialMediaId = ""
    var validationFlag = false
    var locationCurrent = LocationManager()
    
    @IBOutlet var mainScrollView: UIScrollView!
    
    @IBOutlet var contentScrollView: UIView!
    @IBOutlet weak var addressButton: UIButton!
    
    @IBOutlet var checkBoxTermsNCond: UIButton!
    
    @IBOutlet var lastNameTF: HoshiTextField!
    
    @IBOutlet weak var refferelCodeTF: HoshiTextField!
    @IBOutlet var emailTF: HoshiTextField!
    
    @IBOutlet weak var phoneNoTF: UITextField!
    
    @IBOutlet var passwordTF: HoshiTextField!
    
    @IBOutlet weak var companyTF: HoshiTextField!
    
    @IBOutlet weak var companyAddTF: HoshiTextField!
    
    @IBOutlet weak var companyNameHeight: NSLayoutConstraint?
    @IBOutlet weak var heightOfPassword: NSLayoutConstraint!
    
    @IBOutlet weak var seperatorPassword: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var countryImage: UIImageView!
    
    @IBOutlet weak var countryCode: UILabel!
    
    @IBOutlet weak var individualButton: UIButton!
    
    @IBOutlet weak var businessButton: UIButton!
    
    @IBOutlet weak var termsAndConditionsLbl: UILabel!
    
    @IBOutlet weak var lNCheckMark: UIImageView!
    
    @IBOutlet weak var cNCheckMark: UIImageView!
    
    @IBOutlet weak var pNCheckMark: UIImageView!
    
    @IBOutlet weak var EcheckMark: UIImageView!
    
    @IBOutlet weak var passwordCheckMark: UIImageView!
    
    @IBOutlet weak var businessTypeView: UIView!
    
    @IBOutlet weak var createAccountBtn: UIButton!
    
    let fbHandler:FBLoginHandler = FBLoginHandler.sharedInstance()
    let googleHandler:GmailLoginHandler = GmailLoginHandler.sharedInstance()
    var selectedType: SignUpAccountType? = .individual
    var selectedLoginType: SignUpLoginType?
    var selectedValidationType: SignUpValidationType? = nil
    let amazonWrapper:AmazonWrapper = AmazonWrapper.sharedInstance()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector:#selector(self.keyboardWillAppear(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
            NotificationCenter.default.addObserver(self, selector:#selector(self.keyboardWillDisappear(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
            let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
            self.view.addGestureRecognizer(tap)
            self.lastNameTF.addTarget(self, action: #selector(LoginViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
            self.phoneNoTF.addTarget(self, action: #selector(LoginViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
            self.passwordTF.addTarget(self, action: #selector(LoginViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
            self.companyTF.addTarget(self, action: #selector(LoginViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
            self.companyAddTF.addTarget(self, action: #selector(LoginViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
            self.emailTF.addTarget(self, action: #selector(LoginViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
            self.passwordTF.addTarget(self, action: #selector(LoginViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
            let attributes = [
                NSFontAttributeName : UIFont(name: "ClanPro-NarrNews", size: 12)! // Note the !
            ]
            self.addDoneButtonOnTextField(tf:self.phoneNoTF)
            self.phoneNoTF.attributedPlaceholder = NSAttributedString(string: "PHONE NUMBER*", attributes:attributes)
            self.updateUI()
            self.amazonWrapper.delegate = self
        locationCurrent = LocationManager.shared
    }
    override func viewDidDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    func dismissKeyboard() {
        
        view.endEditing(true)
    }
    
    /*****************************************************************/
    //MARK: - Keyboard Methods
    /*****************************************************************/
    
    func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        {
            self .moveViewUp(activeView: activeTextField, keyboardHeight: keyboardSize.height)
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            self.moveViewDown()
        }
    }
    
    /// Move View Up When keyboard Appears
    ///
    /// - Parameters:
    ///   - activeView: View that has Became First Responder
    ///   - keyboardHeight: Keyboard Height
    
    func moveViewUp(activeView: UIView, keyboardHeight: CGFloat) {
        
        // Get Max Y of Active View with respect their Super View
        var viewMAX_Y = activeView.frame.maxY
        
        // Check if SuperView of ActiveView lies in Nexted View context
        // Get Max for every Super of ActiveTextField with respect to Views SuperView
        if var view: UIView = activeView.superview {
            
            // Check if Super view of View in Nested Context is View of ViewController
            while (view != self.view.superview) {
                viewMAX_Y += view.frame.minY
                view = view.superview!
            }
        }
        
        // Calculate the Remainder
        let remainder = self.view.frame.height - (viewMAX_Y + keyboardHeight)
        
        // If Remainder is Greater than 0 does mean, Active view is above the and enough to see
        if (remainder >= 0) {
            // Do nothing as Active View is above Keyboard
        }
        else {
            // As Active view is behind keybard
            // ScrollUp View calculated Upset
            UIView.animate(withDuration: 0.4,
                           animations: { () -> Void in
                            self.mainScrollView.contentOffset = CGPoint(x: 0, y: -remainder)
            })
        }
        
        // Set ContentSize of ScrollView
        var contentSizeOfContent: CGSize = self.contentScrollView.frame.size
        contentSizeOfContent.height += keyboardHeight
        self.mainScrollView.contentSize = contentSizeOfContent
    }
    
    /// Move View Down / Normal Position When keyboard disappears
    func moveViewDown() {
        // Resign back to Normal Position having set Content Size as Initial
        self.mainScrollView.contentSize = self.contentScrollView.frame.size
        self.mainScrollView.contentOffset = CGPoint(x: 0, y: 0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /*****************************************************************/
    //MARK: - Service Methods
    /*****************************************************************/
    
    func signupMethod() {
        
        if (lastNameTF.text?.isEmpty)! {
            self.present(Helper.alertVC(title: ALERTS.Message, message: ALERTS.LName), animated: true, completion: nil)
        }
        else if !Helper.validateName(value: lastNameTF.text!) {
            self.present(Helper.alertVC(title: ALERTS.Message, message: ALERTS.NameVerify), animated: true, completion: nil)
        }else if (phoneNoTF.text?.isEmpty)! {
            self.present(Helper.alertVC(title: ALERTS.Message, message: ALERTS.Mobile), animated: true, completion: nil)
        }
        else if (emailTF.text?.isEmpty)! {
            self.present(Helper.alertVC(title: ALERTS.Message, message:ALERTS.Email_Miss as NSString), animated: true, completion: nil)
        }
        else if (passwordTF.text?.isEmpty)! {
            self.present(Helper.alertVC(title: ALERTS.Message, message:ALERTS.Pasword as NSString), animated: true, completion: nil)
        }
        else if !isConditionsAgreed  {
            self.present(Helper.alertVC(title: ALERTS.Message, message:ALERTS.AgreeTerms), animated: true, completion: nil)
        }else {
            self.performSegue(withIdentifier: "verifyCodeSegue", sender: self)
        }
    }
    
    func updateUI() {
        
        let picker1 = CountryPicker()
        let picker = picker1.countryContent(Google.myCOuntry)//dialCode(code: "IN")
        countryCode.text = picker.dialCode
        Code = picker.dialCode
        countryImage.image = picker.flag
        
        lNCheckMark.isHidden = true
        cNCheckMark.isHidden = true
        pNCheckMark.isHidden = true
        EcheckMark.isHidden = true
        passwordCheckMark.isHidden = true
        
        if selectedType?.rawValue == 1 {
            self.companyNameHeight?.constant = 0
            self.businessTypeView.isHidden = true
            individualButton.isSelected = true
            businessButton.isSelected = false
        }else{
            self.companyNameHeight?.constant = 100
            self.businessTypeView.isHidden = false
            individualButton.isSelected = false
            businessButton.isSelected = true
        }
        
        self.createAccountBtn.isEnabled = false
        if userData.mailId != "" && userData.firstName != "" && userData.userId != ""{
            emailTF.text = userData.mailId
            lastNameTF.text = userData.firstName+" "+userData.lastName
            passwordTF.text = userData.userId
            passwordTF.isUserInteractionEnabled = false;
            profileImage.kf.setImage(with: userData.imageUrl,
                                     placeholder:#imageLiteral(resourceName: "signup_profile_default_image"),
                                     options: [.transition(ImageTransition.fade(1))],
                                     progressBlock: { receivedSize, totalSize in
            },
                                     completionHandler: { image, error, cacheType, imageURL in
            })
            profileUrl = String(describing: userData.imageUrl!)
            selectedValidationType = .email
            sendRequestForValidation()
            heightOfPassword.constant = 0
            seperatorPassword.isHidden = true
            passwordCheckMark.isHidden = true
        }
        else {
            heightOfPassword.constant = 45
            seperatorPassword.isHidden = false
            passwordCheckMark.isHidden = true
        }
        validateAll(value: (selectedType?.rawValue)!)
    }
    
    func generateToken() {
        let token = FIRInstanceID.instanceID().token()
        print("InstanceID token: \(String(describing: token))")
        let defaults = UserDefaults.standard
        defaults.set(token!, forKey: USER_DEFAULTS.TOKEN.PUSH)
        defaults.synchronize()
    }
    
    func uploadProfileimgToAmazon() {
        
        var url = String()
        url = AMAZONUPLOAD.PROFILEIMAGE + Helper.currentDateTimeWithoutSpace + ".png"
        amazonWrapper.uploadImageToAmazon(withImage: profileImage.image!, imgPath: url)
        profileUrl = "https://s3-us-west-2.amazonaws.com/dayrunner/" + url
    }
    
    /*****************************************************************/
    //MARK: - UIButtons
    /*****************************************************************/
    
    @IBAction func addProfileClicked(_ sender: AnyObject) {
        
        selectImage()
    }
    
    @IBAction func countryBtnClicked(_ sender: AnyObject) {
        
        performSegue(withIdentifier: "countrySegue", sender: self)
    }
    
    @IBAction func createAccountAction(_ sender: AnyObject) {
        if refferelCodeTF.text!.characters.count > 0 {
            let referal = RefferalCodeViewModel()
            referal.RefferalDelegate = self
            let params : [String : Any] =   [APIParameterConstants.Referal.Code     : refferelCodeTF.text!,
                                             APIParameterConstants.Referal.TypeUser : 2,
                                             APIParameterConstants.Referal.Lat      : locationCurrent.latitute,
                                             APIParameterConstants.Referal.Long     : locationCurrent.longitude]
            
            
            referal.sendRequestForReferal(data: params)
            
        }else{
            signupMethod()
        }
    }
    
    @IBAction func cancelButtonAction(_ sender: AnyObject) {
        
       self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func individualButtonAction(_ sender: AnyObject) {
        
        individualButton.isSelected = true
        businessButton.isSelected = false
        
        UIView.animate(withDuration: 0.6,
                       animations: {
                        self.companyNameHeight?.constant = 0
                        self.view.layoutIfNeeded()
        },completion:nil)
        self.businessTypeView.isHidden = true
        selectedType = .individual
        validateAll(value: (selectedType?.rawValue)!)
    }
    
    @IBAction func businessButtonAction(_ sender: AnyObject) {
        
        individualButton.isSelected = false
        businessButton.isSelected = true
        
        UIView.animate(withDuration: 0.6, animations: { 
            self.companyNameHeight?.constant = 100
            self.view.layoutIfNeeded()
        }) { (BoolVal) in
            self.businessTypeView.isHidden = false
            self.selectedType = .bussiness
            self.validateAll(value: (self.selectedType?.rawValue)!)
        }
    }
    
    @IBAction func agreeTermsNCondCheckBox(_ sender: AnyObject) {
        
        if checkBoxTermsNCond.isSelected
        {
            checkBoxTermsNCond.isSelected = false
            isConditionsAgreed = false
            self.createAccountBtn.isSelected = false
            self.createAccountBtn.isEnabled = false
        }else {
            checkBoxTermsNCond.isSelected = true
            isConditionsAgreed = true
        }
        validateAll(value: (selectedType?.rawValue)!)
    }
    
    func validateAll(value:Int) {
        if checkBoxTermsNCond.isEnabled && checkBoxTermsNCond.isSelected {
            switch value {
            case 1:
                if (lastNameTF.text?.isNotEmpty)! && (phoneNoTF.text?.isNotEmpty)! && (emailTF.text?.isNotEmpty)! && (passwordTF.text?.isNotEmpty)! {
                    self.createAccountBtn.isEnabled = true
                    self.createAccountBtn.isSelected = true
                    self.checkBoxTermsNCond.isEnabled = true
                } else {
                    self.createAccountBtn.isEnabled = false
                    self.createAccountBtn.isSelected = false
                    self.checkBoxTermsNCond.isEnabled = false
                }
                break
                
            case 2:
                if (lastNameTF.text?.isNotEmpty)! && (phoneNoTF.text?.isNotEmpty)! && (emailTF.text?.isNotEmpty)! && (passwordTF.text?.isNotEmpty)! && (companyTF.text?.isNotEmpty)! && (companyAddTF.text?.isNotEmpty)! {
                    self.createAccountBtn.isEnabled = true
                    self.createAccountBtn.isSelected = true
                    self.checkBoxTermsNCond.isEnabled = true
                } else {
                    self.createAccountBtn.isEnabled = false
                    self.createAccountBtn.isSelected = false
                    self.checkBoxTermsNCond.isEnabled = false
                }
                break
                
            default:
                break
            }
        }else{
            switch value {
            case 1:
                if (lastNameTF.text?.isNotEmpty)! && (phoneNoTF.text?.isNotEmpty)! && (emailTF.text?.isNotEmpty)! && (passwordTF.text?.isNotEmpty)! {
                    self.createAccountBtn.isEnabled = false
                    self.createAccountBtn.isSelected = false
                    self.checkBoxTermsNCond.isEnabled = true
                } else {
                    self.createAccountBtn.isEnabled = false
                    self.createAccountBtn.isSelected = false
                    self.checkBoxTermsNCond.isEnabled = false
                }
                break
                
            case 2:
                if (lastNameTF.text?.isNotEmpty)! && (phoneNoTF.text?.isNotEmpty)! && (emailTF.text?.isNotEmpty)! && (passwordTF.text?.isNotEmpty)! && (companyTF.text?.isNotEmpty)! && (companyAddTF.text?.isNotEmpty)! {
                    self.createAccountBtn.isEnabled = false
                    self.checkBoxTermsNCond.isSelected = false
                    self.checkBoxTermsNCond.isEnabled = true
                } else {
                    self.createAccountBtn.isEnabled = false
                    self.createAccountBtn.isSelected = false
                    self.checkBoxTermsNCond.isEnabled = false
                }
                break
                
            default:
                break
            }
        }
    }
    
    
    @IBAction func privacyPolicyBtnAction(_ sender: Any) {
        
        self.performSegue(withIdentifier: "termsAndCondSegue", sender: 1)
    }
    
    @IBAction func TermsBtnAction(_ sender: Any) {
        
        self.performSegue(withIdentifier: "termsAndCondSegue", sender: 2)
    }
    
    @IBAction func companyAddressAction(_ sender: AnyObject) {
        
        performSegue(withIdentifier: "companyAddSegue", sender: self)
    }
    
    @IBAction func faceBookAction(_ sender: AnyObject) {
        
        fbHandler.delegate = self
        fbHandler.login(withFacebook: self)
    }
    
    @IBAction func googleSignupAction(_ sender: AnyObject) {
        
        selectedLoginType = .google
        googleHandler.delegate = self
        googleHandler.login(withGmail: self)
    }
    
    /*****************************************************************/
    //MARK: - UIImage Picker
    /*****************************************************************/
    
    func selectImage() {
        
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetController: UIAlertController = UIAlertController(title: ALERTS.SelectImage,
                                                                         message: "",
                                                                         preferredStyle: .actionSheet)
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: ALERTS.Cancel,
                                                              style: .cancel) { action -> Void in
        }
        actionSheetController.addAction(cancelActionButton)
        
        let saveActionButton: UIAlertAction = UIAlertAction(title: ALERTS.Gallery,
                                                            style: .default) { action -> Void in
                                                                self.chooseFromPhotoGallery()
        }
        actionSheetController.addAction(saveActionButton)
        
        let deleteActionButton: UIAlertAction = UIAlertAction(title: ALERTS.Camera,
                                                              style: .default) { action -> Void in
                                                                self.chooseFromCamera()
        }
        actionSheetController.addAction(deleteActionButton)
        if profileUrl.length != 0 {
            
            let removeActionButton: UIAlertAction = UIAlertAction(title: ALERTS.RemoveImage,
                                                                  style: .default) { action -> Void in
                                                                    self.removeImage()
            }
            actionSheetController.addAction(removeActionButton)
        }
        
        
        self.present(actionSheetController, animated: true, completion: nil)
    }
    func removeImage() {
        profileUrl = ""
        self.profileImage.image = #imageLiteral(resourceName: "signup_profile_default_image")
    }
    func chooseFromPhotoGallery() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.navigationBar.isTranslucent = false
            imagePickerObj.navigationBar.barTintColor = #colorLiteral(red: 0.137254902, green: 0.7725490196, blue: 0.5647058824, alpha: 1) // Background color
            imagePickerObj.navigationBar.tintColor = UIColor.white
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            imagePickerObj.allowsEditing = false
            self.present(imagePickerObj, animated: true, completion: nil)
        }
    }
    
    func chooseFromCamera() {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
        {
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerControllerSourceType.camera;
            imagePickerObj.allowsEditing = false
            self.present(imagePickerObj, animated: true, completion: nil)
        } else {
            
        }
    }
    
    /*****************************************************************/
    //MARK: - UIImage Picker Delegate Method
    /*****************************************************************/
    
    func imagePickerController(_ picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!) {
        
        profileImage.image = image?.resizeImage(size: CGSize(width: 100, height: 100))
        self.dismiss(animated: true, completion: nil)
        uploadProfileimgToAmazon()
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "countrySegue" {
            
            let nav = segue.destination as! UINavigationController
            if let picker: CountryPicker = nav.viewControllers.first as! CountryPicker? {
                
                picker.delegate = self
            }
        }
        if segue.identifier == "verifyCodeSegue" {
            let nav = segue.destination as! UINavigationController
            if let verifyMob: VerifyMobileViewController = nav.viewControllers.first as! VerifyMobileViewController?
            {
                verifyMob.mobileNo = phoneNoTF.text!
                verifyMob.isCommingFrom = "Signup"
                
                var detail = SignupDetails()
                detail.customerName = lastNameTF.text!
                detail.loginType = selectedLoginType
                detail.phoneNumber = phoneNoTF.text!
                detail.email = emailTF.text!
                detail.companyName = companyTF.text!
                detail.companyAddress = companyAddTF.text!
                detail.password = passwordTF.text!
                detail.accountType = selectedType
                detail.addressLine1 = addressLine1
                detail.addressLine2 = addressLine2
                detail.profilePic = profileUrl
                detail.zipCode = zipCode
                detail.CountryCode = Code
                detail.socialMediaId = socialMediaId
                detail.refferalCode = refferelCodeTF.text!
                verifyMob.signUpDetail = detail
                verifyMob.delegate = self
            }
        }
        if segue.identifier == "companyAddSegue"  {
            
            let selectAddress = segue.destination as! SearchAddressController
            selectAddress.delegate = self
            selectAddress.isComming = .Signup
        }
        if segue.identifier == "termsAndCondSegue" {
            let nav = segue.destination as! UINavigationController
            if let terms: TermsWebViewController = nav.viewControllers.first as! TermsWebViewController? {
                let senderTag: Int = sender as! Int
                
                switch senderTag {
                case 1:
                    terms.StrURL = TermsConditions.PrivacyPolicy
                    terms.termsAndCondition = .conditions
                    break
                    
                default:
                    terms.StrURL = TermsConditions.Terms
                    terms.termsAndCondition = .terms
                    break
                }
            }
        }
    }
    
    /*****************************************************************/
    //MARK: - Service Methods
    /*****************************************************************/
    
    // Email And phone Validation
    func sendRequestForValidation() {
        
        Helper.showPI(string:"Validating")
        let params : [String : Any] = ["mobile"          :phoneNoTF.text!,
                                       "validationType"  :selectedValidationType!.rawValue,
                                       "ent_email"       :emailTF.text!]
        
        print("EmailValidation request parameters :",params)
        NetworkHelper.requestPOST(serviceName:API.METHOD.EMAIL_VALIDATION,
                                  params: params,
                                  success: { (response : [String : Any]) in
                                    print("EmailValidation response : ",response)
                                    if response.isEmpty == false {
                                        Helper.hidePI()
                                        if let errFlag: Int = response["errFlag"] as? Int {
                                            
                                            if errFlag == 0 {
                                                
                                            }
                                            else if errFlag == 1 {
                                                
                                                switch self.selectedValidationType!.rawValue {
                                                case 1:
                                                    self.emailTF.text = ""
                                                    self.emailTF.becomeFirstResponder()
                                                    
                                                case 2:
                                                    self.phoneNoTF.text = ""
                                                    self.phoneNoTF.becomeFirstResponder()
                                                    break
                                                default :
                                                    break
                                                }
                                                self.present(Helper.alertVC(title: ALERTS.Message, message:response["errMsg"] as! NSString), animated: true, completion: nil)
                                            }
                                        }
                                    } else {
                                        switch self.selectedValidationType!.rawValue {
                                        case 1:
                                            self.emailTF.text = ""
                                            self.emailTF.becomeFirstResponder()
                                            
                                        case 2:
                                            self.phoneNoTF.text = ""
                                            self.phoneNoTF.becomeFirstResponder()
                                            break
                                        default :
                                            break
                                        }
                                        self.present(Helper.alertVC(title: ALERTS.Message, message:response["errMsg"] as! NSString), animated: true, completion: nil)
                                    }
                                    
        }) { (Error) in
            self.present(Helper.alertVC(title: ALERTS.Message, message:Error.localizedDescription as NSString), animated: true, completion: nil)
        }
    }
}

/*****************************************************************/
//MARK: - UITextField Delegates
/*****************************************************************/

extension SignUpViewController : UITextFieldDelegate {
    
    /// Describing  textFieldShouldBeginEditing
    ///
    /// - parameter textField: textfield entered
    ///
    /// - returns: Bool
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        activeTextField = textField
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {    //delegate method
        
    }
    func addDoneButtonOnTextField(tf:UITextField){
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        keyboardToolbar.sizeToFit()
        
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                            target: nil, action: nil)
        let doneBarButton = UIBarButtonItem.init(title: "Next", style: .plain, target: self, action: #selector(self.endEditingIn(sender:)))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        tf.inputAccessoryView = keyboardToolbar
        
    }
    func endEditingIn(sender:Any) {
        emailTF.becomeFirstResponder()
    }
    /// TextFieldShould End Editing
    ///
    /// - parameter textField: textfield
    ///
    /// - returns: Bool
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {  //delegate method
        
        switch textField {
        case phoneNoTF:
            if (phoneNoTF.text?.isEmpty)! {
                
                //                self.present(Helper.alertVC(title: ALERTS.Message, message:ALERTS.Mobile), animated: true, completion: nil)
            }
            else {
                selectedValidationType = .phone
                if validationFlag {
                    self.sendRequestForValidation()
                }
            }
            break
        case emailTF:
            if !(emailTF.text?.isValidEmail)! || (emailTF.text?.isEmpty)! {
                //                self.present(Helper.alertVC(title: ALERTS.Message, message:ALERTS.Email), animated: true, completion: nil)
            }
            else {
                selectedValidationType = .email
                if validationFlag {
                    self.sendRequestForValidation()
                }
            }
            break
        default:
            break
        }
        validationFlag = false
        return true
    }
    //MARK: - Custom Methods -
    func textFieldDidChange(_ textField: UITextField) {
        if !(emailTF.text?.isValidEmail)!{
            if textField == emailTF {
                validationFlag = false
            }
            EcheckMark.isHidden = true
        }else {
            if textField == emailTF {
                validationFlag = true
            }
            EcheckMark.isHidden = false
        }
        if (phoneNoTF.text?.isEmpty)! {
            if textField == phoneNoTF {
                validationFlag = false
            }
            pNCheckMark.isHidden = true
        }else {
            if textField == phoneNoTF {
                validationFlag = true
            }
            pNCheckMark.isHidden = false
        }
        if phoneNoTF.text!.characters.count >= 16 {
            let myNSString = phoneNoTF.text! as NSString
            myNSString.substring(with: NSRange(location: 0, length: 16))
            phoneNoTF.text = myNSString as String
            emailTF.becomeFirstResponder()
        }
        if !(lastNameTF.text?.isEmpty)! {
            if !Helper.validateName(value: lastNameTF.text!) {
                lNCheckMark.isHidden = true
            }else{
                lNCheckMark.isHidden = false
            }
        }else {
            lNCheckMark.isHidden = true
        }
        if (companyTF.text?.isEmpty)!{
            cNCheckMark.isHidden = true
        }else {
            cNCheckMark.isHidden = false
        }
        if (passwordTF.text?.isEmpty)!{
            passwordCheckMark.isHidden = true
        }else {
            passwordCheckMark.isHidden = false
        }
        validateAll(value: (selectedType?.rawValue)!)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range:NSRange, replacementString string: String) -> Bool {
        
        
        return true
    }
    
    /// TextField Should Return
    ///
    /// - parameter textField:
    ///
    /// - returns: bool
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        //delegate method
        if textField .isEqual(lastNameTF) {
            if selectedType?.rawValue == 1 {
                phoneNoTF.becomeFirstResponder()
            }else{
                companyTF.becomeFirstResponder()
            }
        } else if textField .isEqual(companyTF) {
            phoneNoTF.becomeFirstResponder()
        } else if textField .isEqual(phoneNoTF) {
            emailTF.becomeFirstResponder()
        } else if textField .isEqual(emailTF) {
            passwordTF.becomeFirstResponder()
//        } else if textField.isEqual(passwordTF){
//            refferelCodeTF.becomeFirstResponder()
        }else{
            dismissKeyboard()
        }
        
        return true
    }
    
    
    
    func keyboardWillAppear(notification: NSNotification){
        // Do something here
        
        let info = notification.userInfo!
        let inputViewFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let screenSize: CGRect = UIScreen.main.bounds
        var frame = self.view.frame
        frame.size.height = screenSize.height - inputViewFrame.size.height
        if activeTextField == phoneNoTF {
            frame.size.height = screenSize.height - inputViewFrame.size.height
        }
        self.view.frame = frame
        
        
        
    }
    
    func keyboardWillDisappear(notification: NSNotification){
        // Do something here
        UIView.animate(withDuration: 0.2, animations: {() -> Void in
            let screenSize: CGRect = UIScreen.main.bounds
            var frame = self.view.frame
            frame.size.height = screenSize.height
            self.view.frame = frame
        })
    }
}

extension SignUpViewController : CountryPickerDelegate {
    /// Country picked
    ///
    /// - parameter country: country
    
    func didPickedCountry(country: Country) {
        
        countryCode.text = country.dialCode
        countryImage.image = country.flag
        Code = country.dialCode
    }
}


extension SignUpViewController : facebookLoginDelegate {
    
    func didFacebookUserLogin(withDetails userInfo:NSDictionary) {
        
        print("FBDetails =",userInfo)
        emailTF?.text = (userInfo["email"]! as AnyObject) as? String
        passwordTF?.text = (userInfo["id"]! as AnyObject) as? String
        lastNameTF?.text = (userInfo["name"]! as AnyObject) as? String
        selectedLoginType = .facebook
    }
    
    func didFailWithError(_ error: Error?) {
        
    }
    
    func didUserCancelLogin() {
        
    }
}

extension SignUpViewController : SearchAddressControllerDelegate {
    
    func didSelectAdress(lat: Float, long: Float, address1: String, address2: String, zipCode: String) {
        
        companyAddTF.text = address1 + address2
        addressLine1 = address1
        addressLine2 = address2
        self.zipCode = zipCode
        companyLat = lat
        companyLong = long
        validateAll(value: (selectedType?.rawValue)!)
    }
    
    func didSelectPickAdd(lat: Float, long: Float, address1: String, address2: String, save: Bool) {
        
    }
}


extension SignUpViewController : GmailLoginHandlerDelegate {
    
    func didGetDetailsFromGoogle(email: String, name: String, token: String, userId: String, profile: GIDProfileData) {
        emailTF.text = email
        passwordTF.text = userId
        lastNameTF.text = name
    }
}

extension SignUpViewController : VerifyMobileViewControllerDelegate {

    func didChangedPassword() {
    self.dismiss(animated: false, completion: nil)
    }
}

extension SignUpViewController : AmazonWrapperDelegate {
   
    func didImageUploadedSuccessfully(withDetails imageURL: String) {
        profileUrl = imageURL
    }
    
    func didImageFailtoUpload(_ error: Error?) {
        
    }
}
extension SignUpViewController : RefferalDelegate {
    func RefferalDelegate(success: Bool, message: String) {
        if success {
            signupMethod()
        }else{
            self.present(Helper.alertVC(title: ALERTS.Missing as NSString, message: message as NSString), animated: true, completion: nil)
        }
    }
}
