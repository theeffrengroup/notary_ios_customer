//
//  HomeMQTTDelegatesExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import GoogleMaps

extension HomeScreenViewController:MusiciansListManagerDelegate {
    
    func emptyMusicianList(errMessage:String) {
        
        if arrayOfProvidersModel.count > 0 {
            
            // Clear the Map
            mapView.clear()
            dictOfMarkers = [:]
            
            self.selectedProviderTag = 0

            arrayOfProvidersModel = []
            arrayOfMusicians = []
            
            //Maintain Notary Count
            maintainNotaryCount()
            
            currentShortestDistance = 0.0
            showETADetails()
            
        } else {
            
            if SplashLoading.obj == nil {
                
                self.showNoArtistMessage(message: errMessage)
            }
                
        }
        
    }
    
    func showInitialMusicianList(arrayOfUpdatedMusiciansModel: [MusicianDetailsModel],arrayOfMusiciansList:[Any]) {
        
        // Load First time
        arrayOfProvidersModel = arrayOfUpdatedMusiciansModel
        arrayOfMusicians = arrayOfMusiciansList
        
        // Clear the Map
        mapView.clear()
        dictOfMarkers = [:]
        
        self.selectedProviderTag = 0
        
        currentShortestDistance = 0.0
        showETADetails()
        
        showMarkersInMapView()
        
        //Maintain Notary Count
        maintainNotaryCount()
        
        if arrayOfUpdatedMusiciansModel.count > 0 && appointmentLocationModel.pickupLatitude != 0.0 {
            
            self.perform(#selector(self.closeSplashLoading), with: nil, afterDelay: 4)
        }
        
    }
    
    func updateMusicianList(arrayOfUpdatedMusicians: [MusicianDetailsModel], arrayOfRowsToAdd: [Int], arrayOfRowsToRemove: [Int], arrayOfMusicianIdToRemove:[String], arrayOfMusiciansList:[Any]) {
        
        if arrayOfProvidersModel.isEmpty {
            
            showInitialMusicianList(arrayOfUpdatedMusiciansModel: arrayOfUpdatedMusicians, arrayOfMusiciansList: arrayOfMusiciansList)
            
        } else {
            
            if arrayOfUpdatedMusicians.count > 0 {
                
                self.perform(#selector(self.closeSplashLoading), with: nil, afterDelay: 4)
            }
            
            showETADetails()
            
            arrayOfProvidersModel = arrayOfUpdatedMusicians
            arrayOfMusicians = arrayOfMusiciansList
            
            for i in 0..<arrayOfRowsToRemove.count {
                
                if dictOfMarkers.index(forKey: arrayOfMusicianIdToRemove[i]) != nil {
                    
                    let providerMarker:GMSMarker = dictOfMarkers[arrayOfMusicianIdToRemove[i]] as! GMSMarker
                    removeMarker(marker: providerMarker)
                    dictOfMarkers.removeValue(forKey: arrayOfMusicianIdToRemove[i])
                    
                }
                
            }
            
            let when = DispatchTime.now() + 0.4
            DispatchQueue.main.asyncAfter(deadline: when){
                
                self.showMarkersInMapView()
            }
        }
    }
    
    
    func maintainNotaryCount() {
        
        if arrayOfProvidersModel.count == 0 {
            
            if SplashLoading.obj == nil {
                
                self.showNoArtistMessage(message: MusiciansListManager.sharedInstance().errMessageString)
            }
            
        } else {
            
            showBookButton()
        }
        
    }
    
    func showBookButton() {
        
        bookButtonHeightConstraint.constant = 42
        self.bookButtonBackView.layoutIfNeeded()
        self.bookButtonBackView.isHidden = false
        
        self.noMusiciansMessageBackView.isHidden = true
        self.showListBackViewBottomConstraint.constant = 75
        self.currentLocationButtonBottomConstraint.constant = 75
        
    }
    
    func hideBookButton(message:String) {
        
        bookButtonHeightConstraint.constant = 0
        self.bookButtonBackView.layoutIfNeeded()
        self.bookButtonBackView.isHidden = true
        
        if message.length > 0 {
            
            self.noMusiciansMessageBackView.isHidden = false
            
        } else {
            
            self.noMusiciansMessageBackView.isHidden = true
        }
        
        
        self.showListBackViewBottomConstraint.constant = self.noMusiciansMessageBackView.bounds.size.height + 10
        self.currentLocationButtonBottomConstraint.constant = self.noMusiciansMessageBackView.bounds.size.height + 10
        
    }
    
    func showETADetails() {
        
        if arrayOfProvidersModel.count == 0 {
            
            self.showETAMessage(message: ALERTS.HOME_SCREEN.NoNotaries)
            
        } else {
            
            if currentShortestDistance != arrayOfProvidersModel[0].distance {
                
                currentShortestDistance = arrayOfProvidersModel[0].distance
                calculateETAAPICall()
                
            } else {
                
                if arrayOfProvidersModel[0].distance == 0.0 {
                    
                    self.showETAMessage(message:"1\nMIN")
                    self.hideETAIndicator()
                }
                
            }
        }
        
    } 
    
}
