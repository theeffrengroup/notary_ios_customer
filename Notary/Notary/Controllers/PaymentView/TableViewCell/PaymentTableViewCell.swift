//
//  PaymentTableViewCell.swift
//  LiveM
//
//  Created by Rahul Sharma on 09/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class PaymentTableViewCell: UITableViewCell {

    @IBOutlet weak var cardImage: UIImageView!
    
    @IBOutlet weak var tickImage: UIImageView!
    
    @IBOutlet var defaultCardLabel: UILabel!
    
    @IBOutlet weak var cardNumberLabel: UILabel!

    @IBOutlet var tickImageWidthConstraint: NSLayoutConstraint!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
