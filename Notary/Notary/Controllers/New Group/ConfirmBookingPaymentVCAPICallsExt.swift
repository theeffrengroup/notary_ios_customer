//
//  ConfirmBookingPaymentVCAPICallsExt.swift
//  Notary
//
//  Created by 3Embed on 28/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension ConfirmBookingPaymentViewController {
    
    func getCards() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        confirmBookingPaymentViewModel.getPaymentCardsListAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.getCardDetails)
        }
        
    }
    
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType)
    {
        switch statusCode
        {
            case HTTPSResponseCodes.TokenExpired.rawValue:
                
                if let dataRes = dataResponse as? String {
                    
                    AppDelegate().defaults.set(dataRes, forKey: USER_DEFAULTS.TOKEN.ACCESS)
                    
                    self.apiTag = requestType.rawValue
                    
                    var progressMessage = PROGRESS_MESSAGE.Loading
                    
                    switch requestType {
                        
                        case .getCardDetails:
                            
                            progressMessage = PROGRESS_MESSAGE.Loading
                        
                        default:
                            
                            break
                    }
                    
                    self.acessClass.getAcessToken(progressMessage: progressMessage)
                    
                }
                
                break
            
            case HTTPSResponseCodes.UserLoggedOut.rawValue:
                
                Helper.logOutMethod()
                if errorMessage != nil {
                    
                    Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
                }
                
                break
            
            case HTTPSResponseCodes.SuccessResponse.rawValue:
                
                switch requestType
                {
                    case RequestType.getCardDetails:
                        
                        if let dataRes = dataResponse as? [Any] {
                            
                            arrayOfCards = []
                            
                            if dataRes.count > 0 {
                                
                                for cardDetail in dataRes {
                                    
                                    let cardDetailModel = CardDetailsModel.init(cardDetail:cardDetail)
                                    arrayOfCards.append(cardDetailModel)
                                }
                                
                            } else {
                                
                                PaymentCardManager.sharedInstance.updateDefaultCardDocumentDetailsToCouchDB(documentId: Utility.defaultCardDocId, data: [:])
                            }
                            
                            isPaymentListChanged = false
                            
                            self.tableView.reloadData()
                            
                        }
                        
                        break
                    
                    default:
                        break
                }
            
            default:
                
                if errorMessage != nil {
                    
                    Helper.showAlert(head: ALERTS.Error, message: errorMessage!)
                    
                }
                
                break
        }
        
    }
    
    
}
