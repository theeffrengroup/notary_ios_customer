//
//  AddNewAddressViewModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift


class AddNewAddressViewModel {
    
    let disposebag = DisposeBag()
    
    var addressLine1 = ""
    var addressLine2 = ""
    var city = ""
    var state = ""
    var country = ""
    var pincode = ""
    var latitude = 0.0
    var longitude = 0.0
    var addressType = ""
    var addressId = ""
    
    var addressAPIReqModel:AddressAPIRequestModel!
    
    
    func createAddressAPIRequestModel() {
        
        addressAPIReqModel = AddressAPIRequestModel()
        
        addressAPIReqModel.addressLine1 = addressLine1
        addressAPIReqModel.addressLine2 = addressLine2
        addressAPIReqModel.city = city
        addressAPIReqModel.state = state
        addressAPIReqModel.country = country
        addressAPIReqModel.pincode = pincode
        addressAPIReqModel.latitude = latitude
        addressAPIReqModel.longitude = longitude
        addressAPIReqModel.addressType = addressType
        addressAPIReqModel.addressId = addressId
    }
    
    
    func addAddressAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        let rxAddressAPICall = AddressAPI()
    
        createAddressAPIRequestModel()
        
        if !rxAddressAPICall.addAddress_Response.hasObservers {
            
            rxAddressAPICall.addAddress_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
        
        rxAddressAPICall.addAddressServiceAPICall(addressRequestModel: addressAPIReqModel)
    }
    
    
    func updateAddressAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        let rxAddressAPI = AddressAPI()
        
        createAddressAPIRequestModel()
        
        rxAddressAPI.updateAddressesServiceAPICall(addressRequestModel:addressAPIReqModel)
        
        if !rxAddressAPI.updateAddress_Response.hasObservers {
            
            rxAddressAPI.updateAddress_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
    }
    
}
