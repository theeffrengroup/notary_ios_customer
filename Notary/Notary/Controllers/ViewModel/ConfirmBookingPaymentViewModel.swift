//
//  ConfirmBookingPaymentViewModel.swift
//  Notary
//
//  Created by 3Embed on 28/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift


class ConfirmBookingPaymentViewModel {
    
    let disposebag = DisposeBag()
    
    func getPaymentCardsListAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        let rxPaymentCardAPICall = PaymentCardAPI()
        
        Helper.showPI(_message: PROGRESS_MESSAGE.Loading)
        
        if !rxPaymentCardAPICall.getPaymentCardList_Response.hasObservers {
            
            rxPaymentCardAPICall.getPaymentCardList_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
        
        rxPaymentCardAPICall.getListOfPaymentCardsServiceAPICall()
    }
}
