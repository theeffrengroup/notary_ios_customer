//
//  AddressListViewModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift


class YourAddressViewModel {
    
    let disposebag = DisposeBag()
    var addressIdToDelete = ""

    
    func getListOfAddressesAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        let rxAddressAPICall = AddressAPI()
        
        Helper.showPI(_message: PROGRESS_MESSAGE.Loading)
        
        if !rxAddressAPICall.getAddressList_Response.hasObservers {
            
            rxAddressAPICall.getAddressList_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
        
        rxAddressAPICall.getListOfAddressesServiceAPICall()
    }
    
    
    func deleteAddressAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        let rxAddressAPI = AddressAPI()
        
        rxAddressAPI.deleteAddressServiceAPICall(addressId: addressIdToDelete)
        
        if !rxAddressAPI.deleteAddress_Response.hasObservers {
            
            rxAddressAPI.deleteAddress_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
    }

}
