//
//  CBSelectedDateTableViewCell.swift
//  LiveM
//
//  Created by Rahul Sharma on 09/01/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

class CBSelectedDateTableViewCell:UITableViewCell {
    
    
    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var dateLabel: UILabel!

    
}
