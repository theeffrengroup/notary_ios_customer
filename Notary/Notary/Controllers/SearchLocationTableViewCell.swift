//
//  SearchLocationTableViewCell.swift
//  LiveM
//
//  Created by Apple on 28/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

class SearchLocationTableViewCell:UITableViewCell {
    
    @IBOutlet weak var addressLabel1: UILabel!
    @IBOutlet weak var addressLabel2: UILabel!
    @IBOutlet weak var removeAddressButton: UIButton!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var addressImageButton: UIButton!
    @IBOutlet weak var addressLabel1HeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var removeAddressButtonWidthConstraint: NSLayoutConstraint!
    
}
