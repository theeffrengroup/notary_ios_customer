//
//  BDAmountDetailsTableViewCell.swift
//  Notary
//
//  Created by Rahul Sharma on 22/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation
//import UICircularProgressRing


class BDAmountDetailsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var amountLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
//    @IBOutlet weak var distanceLabel: UILabel!
    
    @IBOutlet weak var remainingTimerBackView: UIView!
    
    @IBOutlet var remainingTimeView: UICircularProgressRingView!
    
    @IBOutlet var remainingTimeLabel: UILabel!
    
    
    var timer = Timer()
    var intervalProgress: Double!
    var totalDuration: Double = 0.0
    var remainingTimeValue: Int = 0
    var bookingDetailModel:BookingDetailsModel!
    
    
    func showBookingTimerDetails(bookingDetail:BookingDetailsModel) {
        
        bookingDetailModel = bookingDetail
   
        let requestedAtDate = Date(timeIntervalSince1970: TimeInterval(bookingDetail.bookingRequestedAt))
        
        let endDate = Date(timeIntervalSince1970: TimeInterval(bookingDetail.bookingEndTime))
        
        totalDuration = endDate.timeIntervalSince(requestedAtDate)
        
        remainingTimeValue = Int(totalDuration)
        
        if endDate.timeIntervalSince(Date.dateWithDifferentTimeInterval()) > 0 {
            
            if requestedAtDate.timeIntervalSince(Date.dateWithDifferentTimeInterval()) < 0 {
                
                remainingTimeValue = Int(endDate.timeIntervalSince(Date.dateWithDifferentTimeInterval()))
                setRemainingTimeProgress()
                
            } else {
                
                setRemainingTimeProgress()
            }
            
        } else {
            
            remainingTimeValue = 0
            
            timer.invalidate()
            
            remainingTimeView.setProgress(value: 0, animationDuration: 0) {
                
                self.showRemainingTimeDetails()
                self.goToBookingListScreen()
            }
            
        }
        
    }
    
    func setRemainingTimeProgress() {
        
        intervalProgress = 100.0 / totalDuration
        
        scheduledTimerWithTimeInterval()
        
        self.showRemainingTimeDetails()
        
        remainingTimeView.setProgress(value: CGFloat(intervalProgress * Double(remainingTimeValue)), animationDuration: 0) {
            
        }
    }
    
    
    func scheduledTimerWithTimeInterval(){
        
        // Scheduling timer to Call the function "updateCounting" with the interval of 1 seconds
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateRemainingTimeView), userInfo: nil, repeats: true)
    }
    
    @objc func updateRemainingTimeView(){
        
        remainingTimeValue -= 1
        
        if remainingTimeValue > 0 {
            
            remainingTimeView.setProgress(value:CGFloat(intervalProgress * Double(remainingTimeValue)), animationDuration: 0, completion: {
                
                self.showRemainingTimeDetails()
            })
            
            
        } else {
            
            timer.invalidate()
            
            remainingTimeView.setProgress(value: 0, animationDuration: 0) {
                
                self.showRemainingTimeDetails()
                self.goToBookingListScreen()
            }
            
        }
        
        
    }
    
    func goToBookingListScreen() {
        
        if BookingDetailsViewController.obj != nil {
            
            if MyEventViewController.obj != nil {
                
//                MyEventViewController.sharedInstance().isBookingExpired = true
//                MyEventViewController.sharedInstance().gotoPastBooking = true
//                MyEventViewController.sharedInstance().gotoPendingBooking = false
//                MyEventViewController.sharedInstance().gotoUpcomingBooking = false
                
                MyEventViewController.sharedInstance().passExpiredPendingEventToPastEventList(bookingDetail: self.bookingDetailModel)

            }
            
            let bookingResponseModel = BookingStatusResponseModel.init(bookingDetailsModel: bookingDetailModel)
            
            BookingStatusResponseManager.sharedInstance().bookingStatusMessageFromPushOrMQTT(isFromPush: false, bookingStatusModel: bookingResponseModel)
            
            BookingDetailsViewController.sharedInstance().navigationLeftButtonAction(BookingDetailsViewController.sharedInstance().navigationLeftButton)
        }
        
        
    }
    
    func showRemainingTimeDetails() {
        
        let minute:Int = remainingTimeValue/60
        let seconds:Int = remainingTimeValue - (minute * 60)
        
        var minuteString = ""
        var secondsString = ""
        
        if minute < 10 {
            
            minuteString = "0\(minute)"
            
        } else {
            
            minuteString = "\(minute)"
        }
        
        if seconds < 10 {
            
            secondsString = "0\(seconds)"
            
        } else {
            
            secondsString = "\(seconds)"
        }
        
        remainingTimeLabel.text = minuteString + ":" + secondsString
        
    }
}
