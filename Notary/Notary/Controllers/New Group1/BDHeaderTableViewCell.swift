//
//  BDHeaderTableViewCell.swift
//  Notary
//
//  Created by Rahul Sharma on 22/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

class BDHeaderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
}
