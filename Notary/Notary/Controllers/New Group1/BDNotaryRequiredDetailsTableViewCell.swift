//
//  BDNotaryRequiredDetailsTableViewCell.swift
//  Notary
//
//  Created by 3Embed on 16/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation
import Kingfisher

class BDNotaryRequiredDetailsTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var notaryImageView: UIImageView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var notaryNameLabel: UILabel!
    
    
    func showNotaryDetails(bookingDetails:BookingDetailsModel) {
        
        if bookingDetails.providerImageURL.length > 0 {
            
            activityIndicator.startAnimating()
            
            notaryImageView.kf.setImage(with: URL(string: bookingDetails.providerImageURL),
                                        placeholder:#imageLiteral(resourceName: "myevent_profile_default_image"),
                                        options: [.transition(ImageTransition.fade(1))],
                                        progressBlock: { receivedSize, totalSize in
            },
                                        completionHandler: { image, error, cacheType, imageURL in
                                            
                                            self.activityIndicator.stopAnimating()
                                            
            })
            
        } else {
            
            notaryImageView.image = #imageLiteral(resourceName: "myevent_profile_default_image")
        }
        
        if bookingDetails.providerName.length > 0 {
            
            notaryNameLabel.text = bookingDetails.providerName.capitalized
            
        } else {
            
            notaryNameLabel.text = "Booking " + bookingDetails.bookingStatusMessage.capitalized
        }
        
    }
}
