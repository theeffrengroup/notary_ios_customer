//
//  BDPaymentDetailsTableViewCell.swift
//  Notary
//
//  Created by Rahul Sharma on 22/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

class BDPaymentDetailsTableViewCell: UITableViewCell {
    
    @IBOutlet var topView: UIView!
    
    @IBOutlet var cardAndCashBackView: UIView!
    
    @IBOutlet weak var paymentTypeTitleLabel: UILabel!
    
    @IBOutlet var cardNumberLabel: UITextField!
    
    @IBOutlet var cardDetailsbackView: UIView!
    
    @IBOutlet weak var cardImageButton: UIButton!
    
    
    func showPaymentMethodDetails(bookingDetails:BookingDetailsModel) {
        
        if bookingDetails.paymentTypeValue == 2 {
            
            paymentTypeTitleLabel.text = bookingDetails.paymentTypeText.capitalized
            
            cardDetailsbackView.isHidden = false
            
            if bookingDetails.cardNumber.length > 0 {
                
                cardNumberLabel.text = "**** **** **** " + bookingDetails.cardNumber
            }
            else {
                
                cardNumberLabel.text = ""
            }
            
            cardImageButton.setImage(Helper.cardImage(with:bookingDetails.cardBrand), for: UIControl.State.normal)
            
        } else {
            
            paymentTypeTitleLabel.text = bookingDetails.paymentTypeText.capitalized

            cardDetailsbackView.isHidden = true
            
            cardNumberLabel.text = ""
            cardImageButton.setImage(#imageLiteral(resourceName: "cash"), for: UIControl.State.normal)
            
        }
        
    }
}
