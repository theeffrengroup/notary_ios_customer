//
//  BDCancellationReasonTableViewCell.swift
//  Notary
//
//  Created by 3Embed on 27/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

class BDCancellationReasonTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!

    @IBOutlet weak var cancellationReasonLabel: UILabel!
    
}
