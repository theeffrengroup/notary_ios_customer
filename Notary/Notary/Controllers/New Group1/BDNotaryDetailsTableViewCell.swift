//
//  BDNotaryDetailsTableViewCell.swift
//  Notary
//
//  Created by Rahul Sharma on 22/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation
import Kingfisher

class BDNotaryDetailsTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var notaryImageView: UIImageView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var notaryNameLabel: UILabel!
    
    @IBOutlet weak var ratingLabel: UILabel!
    
    @IBOutlet weak var ratingView: FloatRatingView!

    
    func showNotaryDetails(bookingDetails:BookingDetailsModel) {
        
        if bookingDetails.providerImageURL.length > 0 {
            
            activityIndicator.startAnimating()
            
            notaryImageView.kf.setImage(with: URL(string: bookingDetails.providerImageURL),
                                          placeholder:#imageLiteral(resourceName: "myevent_profile_default_image"),
                                          options: [.transition(ImageTransition.fade(1))],
                                          progressBlock: { receivedSize, totalSize in
            },
                                          completionHandler: { image, error, cacheType, imageURL in
                                            
                                            self.activityIndicator.stopAnimating()
                                            
            })
            
        } else {
            
            notaryImageView.image = #imageLiteral(resourceName: "myevent_profile_default_image")
        }
        
        notaryNameLabel.text = bookingDetails.providerName
        
        ratingView.emptyImage = #imageLiteral(resourceName: "start_unselected")
        ratingView.fullImage = #imageLiteral(resourceName: "star_selected")
        ratingView.floatRatings = false
        
        ratingLabel.text = String(format:"%.1f",Float(bookingDetails.givenRating))
        ratingView.rating = Float(bookingDetails.givenRating)
    }
}
