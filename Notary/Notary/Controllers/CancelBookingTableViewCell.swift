//
//  CancelBookingTableViewCell.swift
//  LiveM
//
//  Created by Rahul Sharma on 04/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

class CancelBookingTableViewCell:UITableViewCell {
    
    @IBOutlet var cancelReasonLabel: UILabel!
    
    @IBOutlet var cancelmageView: UIImageView!
    
}
