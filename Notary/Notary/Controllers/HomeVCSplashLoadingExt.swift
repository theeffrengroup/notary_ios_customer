//
//  HomeVCSplashLoadingExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension HomeScreenViewController {
    
    func showPrevProviderDetails() {
        
        if let savedDetails = UserDefaults.standard.object(forKey: USER_DEFAULTS.MUSICIAN_LIST.PREVIOUS) {
            
            if (savedDetails as! [Any]).count > 0 {
                
                musiciansListManager.recievedListOfMusiciansFromMQTT(listOfNewMusicians: savedDetails as! [Any], listOfCurrentMusicians: musiciansListManager.arrayOfMusicians)
            }
            
        } else {
            
            arrayOfProvidersModel = []
            
        }
        
    }
    
    @objc func closeSplashLoading(){
        
        Helper.closeSplashLoading()
    }
    
    func getUpdatedMusiciansListFromMQTTManager() {
        
//        if musiciansListManager.arrayOfMusicians.count > 0 {
        
            musiciansListManager.recievedListOfMusiciansFromMQTT(listOfNewMusicians: musiciansListManager.arrayOfMusicians, listOfCurrentMusicians: arrayOfMusicians)

//        }
        
    }
    
}
