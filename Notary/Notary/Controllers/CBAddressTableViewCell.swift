//
//  CBAddressTableViewCell.swift
//  LiveM
//
//  Created by Rahul Sharma on 18/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

class CBAddressTableViewCell:UITableViewCell {
    
    @IBOutlet var topView: UIView!
    
    @IBOutlet var addressLabel: UILabel!
    
    @IBOutlet var changeButton: UIButton!
    
}
