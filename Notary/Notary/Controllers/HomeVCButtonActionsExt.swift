//
//  HomeVCButtonActionsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension HomeScreenViewController {
    
    @IBAction func currentLocationAction(_ sender: Any) {
        
        showCurrentLocation()
    }
    
    @IBAction func menuAction(_ sender: Any) {
        
        let leftMenu = slideMenuController()
        leftMenu?.openLeft()
    }
    
    @IBAction func listAction(_ sender: Any) {
        
//        if arrayOfProvidersModel.count > 0 {
        
            savePickupLocationDetails()
            
            let listVC:HomeListViewController = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.homeListVC) as! HomeListViewController
            
            listVC.arrayOfProvidersModel = arrayOfProvidersModel
            
            TransitionAnimationWrapperClass.flipFromLeftAnimation(view: self.navigationController!.view)
            self.navigationController!.pushViewController(listVC, animated: false)
            
//        } else {
//
//            Helper.alertVC(title: ALERTS.Message, message: ALERTS.HOME_SCREEN.MusicianMissing)
//        }
        
    }
    
    @IBAction func bookButtonAction(_ sender: Any) {
        
        let checkOutVC:CheckOutViewController = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.checkOutVC) as! CheckOutViewController
        
        checkOutVC.CBModel = createConfirmBookingModel()

        TransitionAnimationWrapperClass.caTransitionAnimationType(CATransitionType.moveIn.rawValue,
                                                                  subType: CATransitionSubtype.fromTop.rawValue,
                                                                  for: (self.navigationController?.view)!,
                                                                  timeDuration: 0.3)
        
        self.navigationController?.pushViewController(checkOutVC, animated: false)
        
    }
    
    func createConfirmBookingModel() -> ConfirmBookingModel {
        
        let CBModel = ConfirmBookingModel.sharedInstance()
        
        var defaultCardArray:[Any] = []
        
        if !Utility.defaultCardDocId.isEmpty {
            
            defaultCardArray = PaymentCardManager.sharedInstance.getDefaultCardDocumentDetailsFromCouchDB(documentId: Utility.defaultCardDocId)
        }
        
        if defaultCardArray.count > 0 {
            
            CBModel.selectedCardModel = CardDetailsModel.init(selectedCardDetails: defaultCardArray[0])
        }
        
        CBModel.bookingModel = .OnDemand
        CBModel.appointmentLocationModel = AppoimtmentLocationModel.sharedInstance
        
        return CBModel
    }
    
    
    @IBAction func datePickerAction(_ sender: Any) {
        
        datePickerView.date = selectedDate
        
        self.datePickerBackBottomConstraint.constant = 0
        self.datePickerFullBackView.isHidden = false
        
        UIView.animate(withDuration: 0.75, animations: {() -> Void in
            
            self.view.layoutIfNeeded()
            
        })
        
    }
    
    
    @IBAction func datePickerSaveButtonAction(_ sender: Any) {
        
        selectedDate = datePickerView.date
        
       // self.selectedProviderId = ""
        
        appointmentLocationModel.bookingType = BookingType.Schedule
        appointmentLocationModel.scheduleDate = selectedDate
        
        Helper.showPI(_message: PROGRESS_MESSAGE.Loading)
        musiciansListManager.publish(isIinitial: false)
        
        updateSelectedBookLaterDetails()
        
        if self.selectedBookLaterBackView.isHidden == true {
            
            showSelectedBookLaterDetails()
        }
        
        self.datePickerBackBottomConstraint.constant = -600
        self.datePickerFullBackView.isHidden = true

        
        UIView.animate(withDuration: 0.75, animations: {() -> Void in
            
            self.view.layoutIfNeeded()
        })
        
    }
    
    @IBAction func datePickerCancelButtonAction(_ sender: Any) {
        
        self.datePickerBackBottomConstraint.constant = -600
        self.datePickerFullBackView.isHidden = true
        
        UIView.animate(withDuration: 0.75, animations: {() -> Void in
            
            self.view.layoutIfNeeded()
        })
        
    }
    
    @IBAction func selectedBookLaterCloseButtonAction(_ sender: Any) {
        
        selectedEventStartTag = 0
        selectedDate = scheduleDate
        
      //  self.selectedProviderId = ""
        
        appointmentLocationModel.bookingType = BookingType.Default
        appointmentLocationModel.selectedEventStartTag = selectedEventStartTag
        
        self.eventStartCollectionView.reloadData()
        
        Helper.showPI(_message: PROGRESS_MESSAGE.Loading)
        musiciansListManager.publish(isIinitial: false)
        
        hideSelectedBookLaterDetails()
    }

        
    @IBAction func searchLocationButton(_ sender: Any) {
        
        let searchLocationVC:SearchLocationViewController = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.searchLocationVC) as! SearchLocationViewController
        
        TransitionAnimationWrapperClass.caTransitionAnimationType(CATransitionType.moveIn.rawValue,
                                                                  subType: CATransitionSubtype.fromTop.rawValue,
                                                                  for: (self.navigationController?.view)!,
                                                                  timeDuration: 0.3)
        
        searchLocationVC.delegate = self
        
        self.navigationController?.pushViewController(searchLocationVC, animated: false)
    }
    
}
