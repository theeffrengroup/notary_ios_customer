//
//  MyEventVCAsynchronousUpdateMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension MyEventViewController {
    
    enum Booking_Position:Int {
        
        case None = 0
        case Pending = 1
        case Upcoming = 2
        case Past = 3
        
    }
    
    func updateBookingDetailsAsynchronously(bookingStatusModel:BookingStatusResponseModel) {
        
        var bookingCurrentPos = Booking_Position.None.rawValue
        var currentBookingModel:BookingDetailsModel!
        
        
        for bookingModel in arrayOfPendingBookings {// Checkin Booking in Pending List
            
            if bookingModel.bookingId == bookingStatusModel.bookingId {
                
                bookingCurrentPos = Booking_Position.Pending.rawValue
                currentBookingModel = bookingModel
                break
            }
        }
        
        if bookingCurrentPos == Booking_Position.None.rawValue {//Booking Not in Pending List
            
            for bookingModel in arrayOfUpcomingBookings {
                
                if bookingModel.bookingId == bookingStatusModel.bookingId {
                    
                    bookingCurrentPos = Booking_Position.Upcoming.rawValue
                    currentBookingModel = bookingModel
                    break
                }
            }
            
            if bookingCurrentPos == Booking_Position.None.rawValue {
                
                for bookingModel in arrayOfPastBookings {
                    
                    if bookingModel.bookingId == bookingStatusModel.bookingId {
                        
                        bookingCurrentPos = Booking_Position.Past.rawValue
                        currentBookingModel = bookingModel
                        break
                    }
                }
                
            }
            
        }
        
        switch bookingStatusModel.bookingStatus {
                
            case Booking_Status.Declined.rawValue,Booking_Status.CancelByProvider.rawValue,Booking_Status.Raiseinvoice.rawValue,Booking_Status.IgnoreOrExpire.rawValue:
                
                if currentBookingModel != nil {
                    
                    currentBookingModel.bookingStatus = bookingStatusModel.bookingStatus
                    currentBookingModel.bookingStatusMessage = bookingStatusModel.bookingStatusMessage
                    
                    if currentBookingModel.bookingStatus == Booking_Status.IgnoreOrExpire.rawValue && currentBookingModel.bookingModel == BookingModel.OnDemand {
                        
                        currentBookingModel.providerName = ""
                        currentBookingModel.providerImageURL = ""
                        
                    }
                    
                    if currentBookingModel.bookingStatus == Booking_Status.CancelByProvider.rawValue {
                        
                        currentBookingModel.cancellationReason = bookingStatusModel.providerCancellationReason
                    }
                    
                    if bookingStatusModel.bookingStatus != Booking_Status.Raiseinvoice.rawValue {
                        
                        currentBookingModel.bookingTotalAmount = 0.0
                    }
                    
                    switch bookingCurrentPos  {
                        
                    case Booking_Position.Pending.rawValue:
                        
                        if arrayOfPendingBookings.count > 0 {
                            
                            if let index = arrayOfPendingBookings.index(where: {$0.bookingId == currentBookingModel.bookingId}) {
                                
                                stopTimerInPendingBookings(index: index)
                                arrayOfPendingBookings.remove(at: index)
                                pendingTableView.reloadData()
                                
                            }
                            
                        }
                        
                        
                        break
                        
                    case Booking_Position.Upcoming.rawValue:
                        
                        if arrayOfUpcomingBookings.count > 0 {
                            
                            if let index = arrayOfUpcomingBookings.index(where: {$0.bookingId == currentBookingModel.bookingId}) {
                                
                                arrayOfUpcomingBookings.remove(at: index)
                                upcomingTableView.reloadData()
                                
                            }
                            
                        }
                        
                        break
                        
                    default:
                        
                        break
                        
                    }
                    
                    var bookingAlreadyInPastList = false
                    if let index = arrayOfPastBookings.index(where: {$0.bookingId == currentBookingModel.bookingId}) {
                        
                        print("\(index)")
                        bookingAlreadyInPastList = true
                    }
                    
                    if !bookingAlreadyInPastList {
                        
                        arrayOfPastBookings.append(currentBookingModel)
                        
                    }
                    
                    self.sortPastBookingDetails()
                    pastTableView.reloadData()
                    
                } else {
                    
                    getBookingsAPI()
                }
                
                //Change Tab to Past Bookings & reload Past Bookings Tableview
                if !pastButton.isSelected {
                    
                    pastAction(pastButton)
                }
                
                upcomingTableView.reloadData()
                pendingTableView.reloadData()
                pastTableView.reloadData()
                
                
                break
                
                
            case Booking_Status.Accepted.rawValue,Booking_Status.Ontheway.rawValue,Booking_Status.Arrived.rawValue,Booking_Status.Started.rawValue,Booking_Status.Completed.rawValue,Booking_Status.Raiseinvoice.rawValue:
                
                if currentBookingModel != nil {
                    
                    currentBookingModel.bookingStatus = bookingStatusModel.bookingStatus
                    currentBookingModel.bookingStatusMessage = bookingStatusModel.bookingStatusMessage
                    
                    if bookingCurrentPos != Booking_Position.Upcoming.rawValue {
                        
                        if arrayOfPendingBookings.count > 0 {
                            
                            if let index = arrayOfPendingBookings.index(where: {$0.bookingId == currentBookingModel.bookingId}) {
                                
                                stopTimerInPendingBookings(index: index)
                                arrayOfPendingBookings.remove(at: index)
                                pendingTableView.reloadData()
                            }

                            arrayOfUpcomingBookings.append(currentBookingModel)
                            self.sortAssignedBookingDetails()
                            upcomingTableView.reloadData()
                        }
                        
                    }
                    
                } else {
                    
                    getBookingsAPI()
                }
                
                //Change Tab to Upcoming Bookings & reload Upcoming Bookings Tableview
                
                if !upcomingButton.isSelected {
                    
                    upcommingAction(upcomingButton)
                }
                
                upcomingTableView.reloadData()
                pendingTableView.reloadData()
                
                break
                
                
            default:
                
                break
                
        }
        
    }
    
    func stopTimerInPendingBookings(index:Int) {
        
        if let pendingBookingCell = pendingTableView.cellForRow(at: IndexPath.init(row: index, section: 0)) as? MyEventPendingTVCell {
            
            pendingBookingCell.timer.invalidate()
        }
        
    }
    
    func passExpiredPendingEventToPastEventList(bookingDetail:BookingDetailsModel) {
        
        if arrayOfPendingBookings.count > 0 {
            
            bookingDetail.bookingStatus = Booking_Status.IgnoreOrExpire.rawValue
            bookingDetail.bookingStatusMessage = ALERTS.BOOKING_FLOW.IgnoredOrExpired
            bookingDetail.bookingTotalAmount = 0.0
            
            if bookingDetail.bookingModel == .OnDemand {
                
                bookingDetail.providerName = ""
                bookingDetail.providerImageURL = ""
            }
            
            if let index = arrayOfPendingBookings.index(where: {$0.bookingId == bookingDetail.bookingId}) {
                
                stopTimerInPendingBookings(index: index)
                arrayOfPendingBookings.remove(at: index)
                arrayOfPastBookings.append(bookingDetail)
                self.sortPastBookingDetails()
            }
            
            
            //Change Tab to Past Bookings & reload Past Bookings Tableview
            if pendingButton.isSelected {
                
                pastAction(pastButton)
            }
            
            let bookingResponseModel = BookingStatusResponseModel.init(bookingDetailsModel: bookingDetail)
            
            BookingStatusResponseManager.sharedInstance().bookingStatusMessageFromPushOrMQTT(isFromPush: false, bookingStatusModel: bookingResponseModel)
            
            pendingTableView.reloadData()
            pastTableView.reloadData()

        }
 
        
        //Change Tab to Past Bookings & reload Past Bookings Tableview
        if !pastButton.isSelected {
            
            pastAction(pastButton)
        }
        
//        self.getBookingsAPI()
        
    }

    
    func sortUnassignedBookingDetails() {
        
        self.arrayOfPendingBookings = self.arrayOfPendingBookings.sorted(by: { $0.bookingId > $1.bookingId })
    }
    
    func sortAssignedBookingDetails() {
        
        self.arrayOfUpcomingBookings = self.arrayOfUpcomingBookings.sorted(by: { $0.bookingId > $1.bookingId })
    }
    
    func sortPastBookingDetails() {
        
        self.arrayOfPastBookings = self.arrayOfPastBookings.sorted(by: { $0.bookingId > $1.bookingId })
    }
    
}
