//
//  ConfirmBookingVCTableViewMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension ConfirmBookingScreen:UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return arrayOfHeaderTitles.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
            
            case 0,1,2,3:
                
                return 1
        
            case 4:
                
                if Utility.categoryVisitFee > 0.0 {
                    
                    return self.selectedServices.count + 3//visitFee + Total + Discount
                }
                return self.selectedServices.count + 2//Total + Discount
            
            default:
                
                return 0
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
       
        let headerCell:CBTableHeaderViewCell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! CBTableHeaderViewCell
        
        headerCell.titleLabel.text = arrayOfHeaderTitles[section]
        
        return headerCell.contentView
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 0, height: 0))
        return view
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
                
            case 0:
                
                if bookNowOrLaterCell == nil {
                    
                    bookNowOrLaterCell = tableView.dequeueReusableCell(withIdentifier: "nowOrLaterCell", for: indexPath) as? CBBookNowOrLaterTableViewCell
                }
                
                bookNowOrLaterCell.bookNowButton.addTarget(self, action: #selector(bookNowButtonAction(bookNowButton:)), for: .touchUpInside)
                
                bookNowOrLaterCell.bookLaterButton.addTarget(self, action: #selector(bookLaterButtonAction(bookLaterButton:)), for: .touchUpInside)
                
                bookNowOrLaterCell.calendarButton.addTarget(self, action: #selector(calendarButtonAction(calendarButton:)), for: .touchUpInside)
                
                
                bookNowOrLaterCell.showBookNowOrLaterDetails(CBModel: CBModel)
                
                return bookNowOrLaterCell
                
            case 1:
                
                if addressCell == nil {
                    
                    addressCell = tableView.dequeueReusableCell(withIdentifier: "addressCell", for: indexPath) as? CBAddressTableViewCell
                }
                
                addressCell.changeButton.addTarget(self, action: #selector(changeAddressButtonAction), for: .touchUpInside)
                addressCell.addressLabel.text = CBModel.appointmentLocationModel.pickupAddress
                
                return addressCell
            
           
            case 2:
                
                if paymentCell == nil {
                    
                    paymentCell = tableView.dequeueReusableCell(withIdentifier: "paymentMethodCell", for: indexPath) as? CBPaymentMethodCell
                }

                paymentCell.addCardButton.addTarget(self, action: #selector(addPaymentButtonAction), for: .touchUpInside)
                
                paymentCell.cardButton.addTarget(self, action: #selector(paymentSelectedButtonAction), for: .touchUpInside)
                paymentCell.cashButton.addTarget(self, action: #selector(paymentSelectedButtonAction), for: .touchUpInside)
                
                paymentCell.showPaymentMethodDetails(CBModel: CBModel)
                
                return paymentCell
                
            case 3:
                
                if promocodeCell == nil {
                    
                    promocodeCell = tableView.dequeueReusableCell(withIdentifier: "promocodeCell", for: indexPath) as? CBPromocodeTableViewCell
                }
                
                promocodeCell.applyButton.addTarget(self, action: #selector(applyPromocodeButtonAction), for: .touchUpInside)
                promocodeCell.showPromocodeDetails(CBModel: CBModel)
                
                return promocodeCell
                
            default:
                
                var servicesCount = self.selectedServices.count
                
                if Utility.categoryVisitFee > 0.0 {
                    
                    servicesCount = self.selectedServices.count + 1
                }
                
                if indexPath.row == (servicesCount + 2) - 1 {
                    
                    let paymentTotalCell:CBTotalTableViewCell = tableView.dequeueReusableCell(withIdentifier: "paymentTotalCell", for: indexPath) as! CBTotalTableViewCell
                    
                    var totalValue = 0.0
                    
                    if Utility.categoryVisitFee > 0.0 {
                        
                        totalValue =  (CBModel.servicesTotalAmount + Utility.categoryVisitFee) - CBModel.discountValue
                        
                    } else {
                        
                        totalValue =  CBModel.servicesTotalAmount - CBModel.discountValue
                    }
                    
                    paymentTotalCell.totalValueLabel.text = Helper.getValueWithCurrencySymbol(data:String(format:"%.2f",totalValue), currencySymbol: self.currencySymbol)
                    
                    return paymentTotalCell
                    
                } else if indexPath.row == (servicesCount + 2) - 2 {
                    
                    let paymentBreakdownCell:CBPaymentBreakDownCell = tableView.dequeueReusableCell(withIdentifier: "paymentBreakdownCell", for: indexPath) as! CBPaymentBreakDownCell
                    
                    if CBModel.discountType.length > 0 {
                        
                        if CBModel.discountType == "Percent" {
                            
                            paymentBreakdownCell.titleLabel.text = "Discount(\(CBModel.percentDiscountValue)%)"
                            
                        } else {
                            
                            paymentBreakdownCell.titleLabel.text = "Discount"
                        }
                        
                        paymentBreakdownCell.amountLabel.text = Helper.getValueWithCurrencySymbol(data:String(format:"%.2f",CBModel.discountValue), currencySymbol: self.currencySymbol)
                        
                    } else {
                        
                        paymentBreakdownCell.titleLabel.text = "Discount"
                        
                        paymentBreakdownCell.amountLabel.text = Helper.getValueWithCurrencySymbol(data:"0.00", currencySymbol: self.currencySymbol)

                    }
                    
                    return paymentBreakdownCell

                } else {
                    
                    let paymentBreakdownCell:CBPaymentBreakDownCell = tableView.dequeueReusableCell(withIdentifier: "paymentBreakdownCell", for: indexPath) as! CBPaymentBreakDownCell
                    
                    var serviceModel:ServicesModel!

                    if Utility.categoryVisitFee > 0.0 {
                        
                        if indexPath.row == 0 {
                            
                            paymentBreakdownCell.titleLabel.text = "Visit Fee"
                            
                            paymentBreakdownCell.amountLabel.text = Helper.getValueWithCurrencySymbol(data:String(format:"%.2f",Utility.categoryVisitFee), currencySymbol: self.currencySymbol)
                            
                            return paymentBreakdownCell
                            
                        } else {
                            
                            serviceModel =  self.selectedServices[indexPath.row - 1]
                        }
                        
                    } else {
                        
                        serviceModel =  self.selectedServices[indexPath.row]
                    }
                    
                    if serviceModel.isForQuantity {
                        
                        paymentBreakdownCell.titleLabel.text = String(format:"%@ (x%d)", serviceModel.serviceName, serviceModel.selectedQuantity)
                        
                    } else {
                        
                        paymentBreakdownCell.titleLabel.text = serviceModel.serviceName + " (x1)"
                    }
                    
                    paymentBreakdownCell.amountLabel.text = Helper.getValueWithCurrencySymbol(data:String(format:"%.2f",serviceModel.serviceTotalAmount), currencySymbol: self.currencySymbol)
                  
                    return paymentBreakdownCell
                    
                }
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.section {
                
            case 0:
                
                if CBModel.appointmentLocationModel.bookingType == .Schedule {
                    
                    return 250
                    
                } else {
                    
                    return 160
                }
                
            case 1:
                
                return UITableView.automaticDimension
                
            case 2:
                if CBModel.paymentmethodTag == 0 {
                    
                    return 120
                    
                } else {
                    
                    return 72
                }
                
            case 3:
                return 75
                
            case 4:
                return 30
                
            default:
                return 0
                
        }
        
    }
    
}
