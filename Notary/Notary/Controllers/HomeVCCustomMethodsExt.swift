//
//  HomeCustomMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import Kingfisher

extension HomeScreenViewController {
    
    func showProfileImageView() {
        
        self.profileImageView.layer.borderWidth = 1
        profileImageView.layer.borderColor = APP_COLOR.cgColor
        self.profileImageView.layer.masksToBounds = true
        self.profileImageView.clipsToBounds = true
        
        if !Utility.profilePic.isEmpty {
            
            profileImageView.kf.setImage(with: URL(string: Utility.profilePic),
                                         placeholder:#imageLiteral(resourceName: "myevent_profile_default_image"),
                                         options: [.transition(ImageTransition.fade(1))],
                                         progressBlock: { receivedSize, totalSize in
            },
                                         completionHandler: { image, error, cacheType, imageURL in
                                            
            })
            
        } else {
            
            profileImageView.image = #imageLiteral(resourceName: "myevent_profile_default_image")
        }
    }
    
    /// Catransition Animation To View Controller
    ///kCATransitionFromTop
    /// - Parameter idntifier: Identifier
    func catransitionAnimation(idntifier: String) {
        let dstVC = self.storyboard!.instantiateViewController(withIdentifier: idntifier)
        
        TransitionAnimationWrapperClass.caTransitionAnimationType(CATransitionType.moveIn.rawValue,
                                                                  subType: CATransitionSubtype.fromTop.rawValue,
                                                                  for: (self.navigationController?.view)!,
                                                                  timeDuration: 0.3)
        
        self.navigationController?.pushViewController(dstVC, animated: false)
    }
    
    /// Flip Animation For View Controller
    ///
    /// - Parameter idntifier: Identifier
    func flipAnimation(_ idntifier: String){
        
    }
    
    func savePickupLocationDetails() {
        
        appointmentLocationModel.pickupAddress = self.addressLabel.text!
    }
        
    
    func initializeMQTTMethods(initial:Bool) {
        
        musiciansListManager.delegate = self
        
        musiciansListManager.navigation = navigationController
        musiciansListManager.viewController = self
        musiciansListManager.timeInterval = Double(ConfigManager.sharedInstance.customerPublishLocationInterval)
        
        if needToShowProgress {
            
            needToShowProgress = false
            Helper.showPI(_message: PROGRESS_MESSAGE.Loading)
            
            if appointmentLocationModel.bookingType == BookingType.Schedule {
                
                musiciansListManager.publish(isIinitial: false)
            } else {
                
                musiciansListManager.publish(isIinitial: true)
            }
            
        }
        else if initial {
            
            musiciansListManager.publish(isIinitial: true)
            
        } else {
            
            musiciansListManager.publish(isIinitial: false)
        }
        
        
        
    }
    
    func uninitializeMQTTMethods() {
        
        musiciansListManager.stopPublish()
        musiciansListManager.navigation = nil
        musiciansListManager.viewController = nil
        musiciansListManager.delegate = nil
    }
    
    func showNoArtistMessage(message:String) {
        
        if self.noMusiciansMessageBackView != nil {
            
            self.noMusiciansMessageLabel.text = message
            self.noMusiciansMessageBackView.layoutIfNeeded()
            self.showETAMessage(message: ALERTS.HOME_SCREEN.NoNotaries)
            hideBookButton(message:message)
        }
        
    }
    
    func checkLocationIsChanged(){
        
        if appointmentLocationModel.pickupAddress != self.addressLabel.text && appointmentLocationModel.pickupAddress.length > 0 {
            
            self.addressLabel.text = appointmentLocationModel.pickupAddress
            musiciansListManager.checkZoneisChanged(currentLat: appointmentLocationModel.pickupLatitude, currentLong: appointmentLocationModel.pickupLongitude)
        }
    }
    
    func showDateSelectOption() {
        
        self.topAddressDivider.isHidden = false
        self.calendarButtonWidthConstraint.constant = 40
    }
    
    func hideDateSelectOption() {
        
        self.topAddressDivider.isHidden = true
        self.calendarButtonWidthConstraint.constant = 0
    }
 
    func setTimeZonesToCalendar() {
        
        self.datePickerView.timeZone = Helper.getTimeZoneFromLoaction(latitude: appointmentLocationModel.pickupLatitude,
                                                                      longitude: appointmentLocationModel.pickupLongitude)
        
        if appointmentLocationModel.bookingType == BookingType.Schedule {
            
            self.updateSelectedBookLaterDetails()
        }
    }
}
