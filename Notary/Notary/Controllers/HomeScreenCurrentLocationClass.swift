//
//  HomeScreenCurrentLocationClass.swift
//  LiveM
//
//  Created by Apple on 24/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import CoreLocation
import GoogleMaps

extension HomeScreenViewController:LocationManagerDelegate {
    
    func didUpdateLocation(location: LocationManager) {
        
        currentLatitude = location.latitute
        currentLongitude = location.longitude
        
//        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(location.latitute),
//                                              longitude: CLLocationDegrees(location.longitude),
//                                              zoom: MAP_ZOOM_LEVEL)
//        self.mapView.animate(to: camera)
        
    }
    
    func didFailToUpdateLocation() {
        
    }
}
