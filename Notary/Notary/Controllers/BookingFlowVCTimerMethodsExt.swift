//
//  BookingFlowTimerMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension BookingFlowViewController {
    
    func showBookingTimerDetails() {
        
        bookingTimer.invalidate()
        
        let currentlyElapsedSeconds = GenericUtility.intForObj(object: bookingDetailModel.totalTimeElapsed)
        
        if bookingDetailModel.timerStatus == 1 {
            
            let startTime = Date(timeIntervalSince1970: TimeInterval(bookingDetailModel.timerStamp))
            
            elapsedSeconds = Int(Date.dateWithDifferentTimeInterval().timeIntervalSince(startTime)) + currentlyElapsedSeconds
            
            if elapsedSeconds < 0 {
                
                elapsedSeconds = elapsedSeconds * -1
            }
            
            
            showRemainingTimeDetails()
            scheduleBookingTimer()
            
            pausedLabel.isHidden = true
            
        } else {
            
//            let startTime = Date(timeIntervalSince1970: TimeInterval(bookingDetailModel.timerStamp))
            
            elapsedSeconds = currentlyElapsedSeconds //Int(Date().timeIntervalSince(startTime)) + currentlyElapsedSeconds
            
            if elapsedSeconds < 0 {
                
                elapsedSeconds = elapsedSeconds * -1
            }
            
            pausedLabel.isHidden = false
            showRemainingTimeDetails()
        }
        
        showBookingTimerDetailsViewAnimation()
        
    }
    
    
    func scheduleBookingTimer(){
        
        // Scheduling timer to Call the function "updateCounting" with the interval of 1 seconds
        bookingTimer = Timer.scheduledTimer(timeInterval: 1.0,
                                            target: self,
                                            selector: #selector(self.updateRemainingTimeDetails),
                                            userInfo: nil,
                                            repeats: true)
    }
    
    
    @objc func updateRemainingTimeDetails(){
        
        elapsedSeconds += 1
        showRemainingTimeDetails()
    }
    
    
    func showRemainingTimeDetails() {
        
        let hour:Int = (elapsedSeconds/(60 * 60))
        let minute:Int = elapsedSeconds/60
        let seconds:Int = elapsedSeconds - (minute * 60)
        
        var hourString = ""
        var minuteString = ""
        var secondsString = ""
        
        if hour < 10 {
            
            hourString = "0\(hour)"
            
        } else {
            
            hourString = "\(hour)"
        }
        
        if minute < 10 {
            
            minuteString = "0\(minute)"
            
        } else {
            
            minuteString = "\(minute)"
        }
        
        if seconds < 10 {
            
            secondsString = "0\(seconds)"
            
        } else {
            
            secondsString = "\(seconds)"
        }
        
        timerLabel.text =  hourString + ":" + minuteString + ":" + secondsString
        
    }
    
    
    func showBookingTimerDetailsViewAnimation() {
        
        timerBackViewTopConstraint.constant = 0
        
        UIView.animate(withDuration: 0.8,
                       delay: 0.2,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 3,
                       options: [],
                       animations: {
                        
                        self.timerBackView.layoutIfNeeded()
                        
        }, completion: { (completed) in
            
            
        })
        
        
    }
    
    
    func hideBookingTimerDetailsViewAnimation() {
        
        timerBackViewTopConstraint.constant = -100
        
        UIView.animate(withDuration: 0.8,
                       delay: 0.2,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 3,
                       options: [],
                       animations: {
                        
                        self.timerBackView.layoutIfNeeded()
                        
        }, completion: { (completed) in
            
            
        })
        
        
    }
    
}
