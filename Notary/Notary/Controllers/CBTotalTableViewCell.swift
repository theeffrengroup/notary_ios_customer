//
//  CBTotalTableViewCell.swift
//  LiveM
//
//  Created by Rahul Sharma on 16/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

class CBTotalTableViewCell:UITableViewCell {
    
    @IBOutlet var totalTitleLabel: UILabel!
    @IBOutlet var totalValueLabel: UILabel!
    
}
