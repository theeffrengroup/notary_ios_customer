//
//  Model.swift
//  Zendesk
//
//  Created by Vengababu Maparthi on 26/12/17.
//  Copyright © 2017 Vengababu Maparthi. All rights reserved.
//

import Foundation
class SubMod: NSObject {
    var time:Int64 = 0
    var subject = ""
    var id:Int64 = 0
}


// tickets model
class ModelClass: NSObject {
    var open = [SubMod]()
    var close = [SubMod]()
    
    func getTheParsedData(dict:[String:Any]) -> ModelClass {
        let modelData = ModelClass()
        open = [SubMod]()
        close = [SubMod]()
        for openDict in dict["open"] as! [[String:Any]] {
            let openData = SubMod()
            
            if let subject = openDict["subject"] as? String{
                openData.subject = subject
            }

            
            if let dateTime = openDict["timeStamp"] as? NSNumber{
                openData.time = Int64(truncating: dateTime)
            }
            
            if let ticketID = openDict["id"] as? NSNumber{
                openData.id = Int64(truncating: ticketID)
            }
            
            modelData.open.append(openData)
        }
        
        for closeDict in dict["close"] as! [[String:Any]] {
            let closeData = SubMod()
            
            if let subject = closeDict["subject"] as? String{
                closeData.subject = subject
            }
            
            
            if let dateTime = closeDict["timeStamp"] as? NSNumber{
                closeData.time = Int64(truncating: dateTime)
            }
            
            
            if let ticketID = closeDict["id"] as? NSNumber{
                closeData.id = Int64(truncating: ticketID)
            }
            
            modelData.close.append(closeData)
        }
        return modelData
    }
}
