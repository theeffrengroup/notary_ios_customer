//
//  FAQVCTableViewMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension SupportListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrayOfSupportList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "supportCell", for: indexPath) as! SupportTableViewCell
        
        let supportDetailsModel = arrayOfSupportList[indexPath.row]
        
        cell.tittleLabel.text = supportDetailsModel.name
        
        return cell
    }
    
}

extension SupportListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let supportDetailsModel = arrayOfSupportList[indexPath.row]
        
        if supportDetailsModel.innerSupport.count > 0 {
            
            let supportInnerListVC:SupportInnerListViewController = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.supportInnerVC) as! SupportInnerListViewController
            
            var arrayofSupportInnerList = [SupportDetailsModel]()
            
            for eachInnerSupport in supportDetailsModel.innerSupport {
                
                let supportModel = SupportDetailsModel.init(supportResponse:eachInnerSupport)
                arrayofSupportInnerList.append(supportModel)
                    
            }
            
            supportInnerListVC.arrayOfSupportList = arrayofSupportInnerList
            supportInnerListVC.tittle = supportDetailsModel.name
            
            self.navigationController!.pushViewController(supportInnerListVC, animated: true)
            
        } else {
            
            let supportWebVC:SupportWebViewController = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.supportWebVC) as! SupportWebViewController
            
            supportWebVC.webURL = supportDetailsModel.link
            supportWebVC.navTitle = supportDetailsModel.name
            
            self.navigationController!.pushViewController(supportWebVC, animated: true)
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.contentView.transform = CGAffineTransform(translationX: -500, y: 0)
        UIView.animate(withDuration: 0.6) {
            cell.contentView.transform = .identity
        }
        
    }
    
}
