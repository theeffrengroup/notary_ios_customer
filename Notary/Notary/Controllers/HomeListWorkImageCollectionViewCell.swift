//
//  HomeListWorkImageCollectionViewCell.swift
//  Notary
//
//  Created by Rahul Sharma on 12/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

class HomeListWorkImageCollectionViewCell:UICollectionViewCell {
    
    @IBOutlet weak var workImageView: UIImageView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
 
}
