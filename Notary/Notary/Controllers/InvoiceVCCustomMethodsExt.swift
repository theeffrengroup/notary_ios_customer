//
//  InvoiceVCCustomMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import Kingfisher

extension InvoiceViewController {
    
    func showBookingDetails() {
        
        ratingView.emptyImage = #imageLiteral(resourceName: "invoice_star_unsel")
        ratingView.fullImage = #imageLiteral(resourceName: "Invoice_star_selec")
        ratingView.floatRatings = false
        
        let startDate = Date(timeIntervalSince1970: TimeInterval(bookingDetailModel.bookingStartTime))
        
        let dateFormat = DateFormatter.initTimeZoneDateFormat()
        
        dateFormat.dateFormat = "d MMM yyyy"
        eventDateLabel.text = dateFormat.string(from: startDate)
        
        
        amountLabel.text = Helper.getValueWithCurrencySymbol(data: String(format:"%.2f",bookingDetailModel.bookingTotalAmount), currencySymbol: bookingDetailModel.currencySymbol)
                
        
        ratingView.rating = 5.0
        
        musicianNameLabel.text = bookingDetailModel.providerName
        
        
        if bookingDetailModel.providerImageURL.length > 0 {
            
            activityIndicator.startAnimating()
            
            musicianImageView.kf.setImage(with: URL(string: bookingDetailModel.providerImageURL),
                                          placeholder:#imageLiteral(resourceName: "myevent_profile_default_image"),
                                          options: [.transition(ImageTransition.fade(1))],
                                          progressBlock: { receivedSize, totalSize in
            },
                                          completionHandler: { image, error, cacheType, imageURL in
                                            
                                            self.activityIndicator.stopAnimating()
                                            
            })
            
        } else {
            
            musicianImageView.image = #imageLiteral(resourceName: "myevent_profile_default_image")
        }
        
        
    }
    
    
}
