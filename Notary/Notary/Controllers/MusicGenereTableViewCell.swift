//
//  MusicGenereTableViewCell.swift
//  LiveM
//
//  Created by Rahul Sharma on 05/12/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

class MusicGenereTableViewCell:UITableViewCell {
    
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var selectedImageView: UIImageView!
    @IBOutlet var divider: UIView!
    
}
