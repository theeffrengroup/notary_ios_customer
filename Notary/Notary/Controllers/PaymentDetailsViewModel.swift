//
//  PaymentDetailsViewModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift


class PaymentDetailsViewModel {
    
    let disposebag = DisposeBag()
    var cardId = ""
    
    
    func deletePaymentCardAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        let rxPaymentCardAPICall = PaymentCardAPI()
        
        if !rxPaymentCardAPICall.deletePaymentCard_Response.hasObservers {
            
            rxPaymentCardAPICall.deletePaymentCard_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
        
        rxPaymentCardAPICall.deletePaymentCardServiceAPICall(cardId: cardId)
    }
    
    func makePaymentCardDefaultAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        let rxPaymentCardAPICall = PaymentCardAPI()
        
        if !rxPaymentCardAPICall.makePaymentCardDefault_Response.hasObservers {
            
            rxPaymentCardAPICall.makePaymentCardDefault_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
        
        rxPaymentCardAPICall.makePaymentCardDefaultServiceAPICall(cardId: cardId)
    }

}
