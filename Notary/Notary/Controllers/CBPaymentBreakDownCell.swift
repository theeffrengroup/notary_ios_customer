//
//  CBPaymentBreakDownCell.swift
//  LiveM
//
//  Created by Rahul Sharma on 16/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

class CBPaymentBreakDownCell:UITableViewCell {
    
    @IBOutlet var amountLabel: UILabel!
    
    @IBOutlet var titleLabel: UILabel!
    
}
