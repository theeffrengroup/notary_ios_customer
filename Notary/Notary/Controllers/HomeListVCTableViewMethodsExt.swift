//
//  HomeListVCTableViewMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
//import youtube_ios_player_helper_swift

extension HomeListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if appointmentLocationModel.bookingType == BookingType.Schedule {
            
            return 1
        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if appointmentLocationModel.bookingType == BookingType.Schedule {
            
            return self.arrayOfAvailableNowProviders.count
            
        } else {
            
            if section == 0 {
                
                return self.arrayOfAvailableNowProviders.count
                
            } else {
                
                return self.arrayOfAvailableLaterProviders.count
            }
        }

    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if appointmentLocationModel.bookingType == BookingType.Schedule {

            let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 0, height: 0))
            return view
            
        } else {
            
            let headerCell:CBTableHeaderViewCell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! CBTableHeaderViewCell
            
            if section == 0 {
                
                headerCell.titleLabel.text = ALERTS.HOME_LIST_SCREEN.AvailableNowMessage + " (\(self.arrayOfAvailableNowProviders.count)) "
                
            } else {
                
                headerCell.titleLabel.text = ALERTS.HOME_LIST_SCREEN.AvailableLaterMessage + " (\(self.arrayOfAvailableLaterProviders.count)) "
            }
            
            return headerCell.contentView
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if appointmentLocationModel.bookingType == BookingType.Schedule {
            
            return 0.001
        }
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 200.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: HomeListTableViewCell!
        
        cell = tableView.dequeueReusableCell(withIdentifier: "listCell", for: indexPath) as! HomeListTableViewCell
 
        let musicianDetails:MusicianDetailsModel!
        
        if appointmentLocationModel.bookingType == BookingType.Schedule {
            
            musicianDetails = arrayOfAvailableNowProviders[indexPath.row]
            
        } else {
            
            if indexPath.section == 0 {
                
                musicianDetails = arrayOfAvailableNowProviders[indexPath.row]
                
            } else {
                
                musicianDetails = arrayOfAvailableLaterProviders[indexPath.row]
            }
        }
        
        cell.musicianDetailsModel = musicianDetails
        
        cell.nameLabel.text = musicianDetails.firstName.capitalized + " " + musicianDetails.lastName.capitalized
        
        cell.ratingView.emptyImage = #imageLiteral(resourceName: "start_unselected")
        cell.ratingView.fullImage = #imageLiteral(resourceName: "star_selected")
        cell.ratingView.floatRatings = false
        
        if musicianDetails.status == 0 {
            
            cell.statusLabel.text = "OFFLINE"
            cell.statusLabel.textColor = HOME_LIST_COLOR_CODE.OFFLINE
            
        } else {
            
            cell.statusLabel.text = "ONLINE"
            cell.statusLabel.textColor = HOME_LIST_COLOR_CODE.ONLINE
        }
        
        
        Helper.setShadowFor(cell.topView, andWidth: self.view.bounds.size.width - 20, andHeight: 200 - 14)
        
        cell.ratingView.rating = Float(musicianDetails.overallRating)
        
        cell.isHidden = false
        
        
//        cell.priceLabel.text = String(format:"%@ %.2f", musicianDetails.currencySymbol, musicianDetails.amount)
        
        cell.distanceLabel.text = Helper.getDistanceDependingMileageMetricFromServer(distance: musicianDetails!.distance, mileageMatric: musicianDetails!.mileageMatric)
        
        cell.collectionView.reloadData()
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let musicianDetailsVC:MusicianDetailsViewController = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.musicianDetailsVC) as! MusicianDetailsViewController
        
        if appointmentLocationModel.bookingType == BookingType.Schedule {

            musicianDetailsVC.providerDetailFromPrevController = arrayOfAvailableNowProviders[indexPath.row]
            
        } else {
            
            if indexPath.section == 0 {
                
                musicianDetailsVC.providerDetailFromPrevController = arrayOfAvailableNowProviders[indexPath.row]
                
            } else {
                
                musicianDetailsVC.providerDetailFromPrevController = arrayOfAvailableLaterProviders[indexPath.row]
                
            }
        }
        
        self.navigationController!.pushViewController(musicianDetailsVC, animated: true)
    }
        
}
