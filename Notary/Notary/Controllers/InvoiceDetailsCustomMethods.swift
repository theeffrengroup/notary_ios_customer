//
//  InvoiceDetailsCustomMethods.swift
//  LiveM
//
//  Created by Rahul Sharma on 06/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import Kingfisher

extension InvoiceDetailsPopUpScreen {
    
    /// Method to show Invoice details
    func showInvoiceDetails() {
        
//        gigNameLabel.text = String(format:"%@ %@ / Gig", GenericUtility.strForObj(object:bookingDetailModel.gigTimeDict["name"]), GenericUtility.strForObj(object:bookingDetailModel.gigTimeDict["unit"]))
        
         gigNameLabel.text = String(format:"%@ %@", GenericUtility.strForObj(object:bookingDetailModel.gigTimeDict["name"]), GenericUtility.strForObj(object:bookingDetailModel.gigTimeDict["unit"]))
        
        eventIdLabel.text = "Booking Id: \(bookingDetailModel.bookingId)"

        
        gigValueLabel.text = Helper.getValueWithCurrencySymbol(data: String(format:"%.2f",bookingDetailModel.bookingTotalAmount), currencySymbol: bookingDetailModel.currencySymbol)
        
        serviceFeeValueLabel.text = Helper.getValueWithCurrencySymbol(data: String(format:"%.2f",bookingDetailModel.bookingTotalAmount), currencySymbol: bookingDetailModel.currencySymbol)
    
        discountValueLabel.text = Helper.getValueWithCurrencySymbol(data: String(format:"%.2f",bookingDetailModel.discountValue), currencySymbol: bookingDetailModel.currencySymbol)
    
        totalValueLabel.text = Helper.getValueWithCurrencySymbol(data: String(format:"%.2f",bookingDetailModel.bookingTotalAmount), currencySymbol: bookingDetailModel.currencySymbol)
        
        
        if bookingDetailModel.signatureURL.length > 0 {
            
            activityIndicator.startAnimating()
            
            signatureImageView.kf.setImage(with: URL(string: bookingDetailModel.signatureURL),
                                          placeholder:nil,
                                          options: [.transition(ImageTransition.fade(1))],
                                          progressBlock: { receivedSize, totalSize in
            },
                                          completionHandler: { image, error, cacheType, imageURL in
                                            
                                            self.activityIndicator.stopAnimating()
                                            
            })
            
        } else {
            
            signatureImageView.image = nil
        }

        
    }
    
}
