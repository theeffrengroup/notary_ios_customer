//
//  FAQViewModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class SupportListViewModel {
    
    let disposebag = DisposeBag()
    
    func getSupportListAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        let rxSupportAPICall = SupportAPI()
        
        if !rxSupportAPICall.getSupport_Response.hasObservers {
            
            rxSupportAPICall.getSupport_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
        
        rxSupportAPICall.getSupportServiceAPICall()
    }
}
