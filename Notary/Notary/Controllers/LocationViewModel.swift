//
//  LocationViewModel.swift
//  Notary
//
//  Created by 3Embed on 10/04/20.
//  Copyright © 2020 Rahul Sharma. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import CoreLocation
import GoogleMaps

/// Login View Model Class to maintain Login View Data
class LocationViewModel{
    
    var loginModel:LoginModel!//Login Model used to bind login data(We can use this model in any class)
    
    var locationCoordinates = CLLocationCoordinate2DMake(0.0,0.0)
    
    let disposebag = DisposeBag()
    let rxLocationAPICall = GoogleLocationAPI()
    
    func locationAPICall(completion:@escaping ([String:Any]) -> ()) {
        
        rxLocationAPICall.locationServiceAPICall(locationCoordinates: locationCoordinates)
        
        if !rxLocationAPICall.location_Response.hasObservers {
            
            rxLocationAPICall.location_Response
            .subscribe(onNext: {response in
                
//                if (response.data[SERVICE_RESPONSE.Error] != nil) {
//
//                    Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
//                    return
//                }
                
                completion(response.dataResponse)
                
                
            }, onError: {error in
                
            }).disposed(by: disposebag)
        }
        

    }
}
