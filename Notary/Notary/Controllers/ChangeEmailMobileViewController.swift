//
//  ChangeEmailMobileViewController.swift
//  LiveM
//
//  Created by Rahul Sharma on 18/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol changeEmailMobileDelegate {
    func recallApi(sucess: Bool)
}

class ChangeEmailMobileViewController: UIViewController, AccessTokeDelegate {
    
    
    @IBOutlet weak var mobileEmailTF: UITextField!
    
    @IBOutlet weak var eraseTFButton: UIButton!
    
    @IBOutlet weak var discriptionLabel: UILabel!
    
    @IBOutlet weak var loadingWidth: NSLayoutConstraint!
    
    @IBOutlet weak var phoneNumberView: UIView!
    
    @IBOutlet weak var continueBottomConstrain: NSLayoutConstraint!
    
    @IBOutlet weak var phoneNumberTF: UITextField!
    @IBOutlet weak var countryImage: UIImageView!
    @IBOutlet weak var countryCodeLabel: UILabel!
    @IBOutlet weak var continueButton: UIButtonCustom!
    
    var delegate: changeEmailMobileDelegate?
    
    var cImage: String = ""
    var isEmail: Bool = false
    var appDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
    var selectedCountry: Country!
    var apiTag: Int!
    let acessClass = AccessTokenRefresh()
    
    var currentEmail = ""
    var currentPhoneNumber = ""
    var currentCountryCode = ""
    var currentCountryImage:UIImage!
    
    var changeEmailMobileViewModel = ChangeEmailMobileViewModel()
    let disposeBag = DisposeBag()
    var countryCodeSymbol = ""
    var currentCountryCodeSymbol = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
        initiallSetUp()
        
        mobileEmailTF.text = currentEmail
        phoneNumberTF.text = currentPhoneNumber
        countryCodeLabel.text = currentCountryCode
        
        if currentCountryImage != nil {
            
            countryImage.image = currentCountryImage
        }
        
        changeEmailMobileViewModel.emailText.value = mobileEmailTF.text!
        changeEmailMobileViewModel.phoneNumberText.value = phoneNumberTF.text!
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if (self.navigationController?.navigationBar.isTranslucent)! {
            
            self.navigationController?.navigationBar.isTranslucent = false
        }
        
        appDelegate?.keyboardDelegate = self
        appDelegate?.accessTokenDelegate = self
        
        addObserveToVariables()
        
        if isEmail {
            
            discriptionLabel.text = CHANGE_ME.emaileText
            phoneNumberView.isHidden = true
            mobileEmailTF.becomeFirstResponder()
            self.title = CHANGE_ME.emailTittle
            
            validateFields(textFieldText: mobileEmailTF.text!)
            
        }else {
            
            discriptionLabel.text = CHANGE_ME.phoneText
            phoneNumberView.isHidden = false
            phoneNumberTF.becomeFirstResponder()
            self.title = CHANGE_ME.phoneTittle
            validateFields(textFieldText: phoneNumberTF.text!)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func eraseAction(_ sender: Any) {
        phoneNumberTF.text = ""
        changeEmailMobileViewModel.emailText.value = ""
        mobileEmailTF.text = ""
        changeEmailMobileViewModel.phoneNumberText.value = ""
    }
    
    @IBAction func continueAction(_ sender: Any) {
        
        if isEmail {
            
            if (mobileEmailTF.text?.length)! > 0 {
                
                if Helper.isValidEmail(testStr: mobileEmailTF.text!) {
                    
                    sendRequestToValidateEmailAddress()
                    
                } else {
                    
                     Helper.alertVC(title: ALERTS.Message, message: ALERTS.EmailMissing)
                }
                
            } else {
                
                Helper.alertVC(title: ALERTS.Message, message: ALERTS.EmailMissing)
            }
            
        }else {
            
            if (phoneNumberTF.text?.length)! > 5 {
                
                sendRequestToValidatePhoneNumber()
                
            } else {
                
                Helper.alertVC(title: ALERTS.Message, message: ALERTS.PhoneNumberMissing)
            }
            
        }
    }
    
    @IBAction func countryPickerAction(_ sender: Any) {
        catransitionAnimation(idntifier: VCIdentifier.countryNameVC)
    }
    
    func recallApi() {
        switch apiTag {
        case RequestType.verifyEmail.rawValue:
            sendRequestToValidateEmailAddress()
            
        case RequestType.updateMobile.rawValue:
            sendRequestToChangeMobile()
            
        case RequestType.updateEmail.rawValue:
            sendRequestToChangeEmail()
            
        case RequestType.verifyMobileNumber.rawValue:
            sendRequestToValidatePhoneNumber()
            
        default:
            break
        }
    }
    
    func initiallSetUp()  {
        
        if countryCodeLabel.text?.length == 0 {
            
            if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
                DDLogDebug(countryCode)
                let picker = CountryNameViewController()
                picker.initialSetUp()
                let contry: Country
                contry = picker.localCountry(countryCode)
                countryCodeLabel.text = contry.dial_code
                let imagestring = contry.country_code
                let imagePath = "CountryPicker.bundle/\(imagestring).png"
                countryImage.image = UIImage(named: imagePath)
                currentCountryCodeSymbol = countryCode
            }

        }
        
    }
    
    /// Catransition Animation To View Controller
    ///
    /// - Parameter idntifier: Identifier
    func catransitionAnimation(idntifier: String){
        
        let dstVC = self.storyboard!.instantiateViewController(withIdentifier: idntifier) as! CountryNameViewController
        
        TransitionAnimationWrapperClass.caTransitionAnimationType(CATransitionType.moveIn.rawValue,
                                                                  subType: CATransitionSubtype.fromTop.rawValue,
                                                                  for: (self.navigationController?.view)!,
                                                                  timeDuration: 0.3)
        
        dstVC.countryDelegate = self
        
        self.navigationController?.pushViewController(dstVC, animated: false)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SEgueIdetifiers.changeEMToVerify {
            if let dstVC = segue.destination as? VerifyMobileViewController {
                dstVC.isCommingFrom = "ChangeEmailPhone"
                dstVC.dataFromForgetPasswordVC = sender as! [String : Any]
                dstVC.delegate = self
            }
        }
    }
    
    func addObserveToVariables() {
        
        mobileEmailTF.rx.text
            .orEmpty
            .bind(to: changeEmailMobileViewModel.emailText)
            .disposed(by: disposeBag)
        
        phoneNumberTF.rx.text
            .orEmpty
            .bind(to: changeEmailMobileViewModel.phoneNumberText)
            .disposed(by: disposeBag)
        
    }

}


extension ChangeEmailMobileViewController: KeyboardDelegate {
    
    // MARK: - Keyboard Methods -
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillShow(notification: NSNotification) {
        let inputViewFrame: CGRect? = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect
        continueBottomConstrain.constant = (inputViewFrame?.size.height)!
        
    }
    
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillHide(notification: NSNotification) {
        //Once keyboard disappears, restore original positions
        
        
        
    }
    
}

// MARK: - CountrySelectedDelegate -
extension ChangeEmailMobileViewController: CountrySelectedDelegate {
    
    /// getting data from CountryNameViewController
    ///
    /// - Parameter country: country description
    func countryNameSelected(countrySelected country: Country) {
        self.selectedCountry = country
        DDLogVerbose("country selected  code \(selectedCountry.country_code), country name \(self.selectedCountry.country_name), dial code \(self.selectedCountry.dial_code)")
        
        let imagestring = selectedCountry.country_code
        let imagePath = "CountryPicker.bundle/\(imagestring).png"
        countryImage.image = UIImage(named: imagePath)
        
        countryCodeLabel.text =  self.selectedCountry.dial_code
        /*
         country code is not setting to correct country symbol.
         Sending the selected country code symbole name leke "IN" or "US" to back end during registration.
         We will get again the symbol name fromt the backend where ever we need to dispay the country flag
         Owner : Vani Chikaraddi
         Date : 19/12/2019
         */
        countryCodeSymbol = country.country_code
        currentCountryCodeSymbol = country.country_code
    }
    
    /// Filter Country Name
    ///
    /// - Parameter searchText: To search text in country name
    func filtercountry(_ searchText: String) {
        let picker = CountryNameViewController()
        picker.initialSetUp()
        let contry: Country
        contry = picker.localCountryName(searchText)
        let imagestring = contry.country_code
        let imagePath = "CountryPicker.bundle/\(imagestring).png"
        countryImage.image = UIImage(named: imagePath)
    }
    
}

extension ChangeEmailMobileViewController: VerifyMobileViewControllerDelegate {
    func didUpdateMobileNumber() {
        delegate?.recallApi(sucess: true)
    }
}

