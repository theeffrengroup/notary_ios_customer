//
//  ForgetPasswordViewController.swift
//  DayRunner
//
//  Created by Rahul Sharma on 28/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


class ForgetPasswordViewController: UIViewController {
    
    // MARK: - Outlets -
    //Layout Constrains
    @IBOutlet weak var loadingViewHeight: NSLayoutConstraint!
    @IBOutlet weak var codeWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var emailViewHeight: NSLayoutConstraint!
    
    //Image View
    @IBOutlet weak var countryImagee: UIImageView!
    
    // Label
    @IBOutlet weak var countryCode: UILabel!
    @IBOutlet weak var discriptionLabel: UILabel!
    
    // text Field
    @IBOutlet weak var phonenumberTF: UITextField!
    @IBOutlet weak var emailTF: FloatLabelTextField!
    
    // View
    @IBOutlet weak var phoneNumView: UIView!
    @IBOutlet weak var emailBottomView: UIView!
    @IBOutlet weak var nextBottomView: UIViewCustom!
    @IBOutlet weak var phoneButtonBottomView: UIView!
    @IBOutlet weak var emailButtonBottomView: UIView!
    
    // Button
    @IBOutlet weak var nextButtonOutlet: UIButtonCustom!
    @IBOutlet weak var phoneButton: UIButton!
    @IBOutlet weak var emailButton: UIButton!
    
    // MARK: - Variable Decleration -
    let catransitionAnimationClass = CatransitionAnimationClass()
    var selectedCountry: Country!
    
    var forgotPasswordViewModel = ForgotPasswordViewModel()
    let disposeBag = DisposeBag()
    var forgotPasswordWithEmail:Bool = false
     var countryCodeSymbol = ""
    // MARK: - Default Class Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        initiallSetup()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        animateBottomView()
        initiallAnimation()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        if emailViewHeight.constant == 0 {
            
            phonenumberTF.becomeFirstResponder()
            
        } else {
            
            emailTF.becomeFirstResponder()
        }
        
        addObserveToVariables()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func addObserveToVariables() {
        
        emailTF.rx.text
            .orEmpty
            .bind(to: forgotPasswordViewModel.emailText)
            .disposed(by: disposeBag)
        
        phonenumberTF.rx.text
            .orEmpty
            .bind(to: forgotPasswordViewModel.phoneNumberText)
            .disposed(by: disposeBag)
        
    }
    
}

// MARK: - UINavigationControllerDelegate
extension ForgetPasswordViewController: UINavigationControllerDelegate{
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

// MARK: - CountrySelectedDelegate -
extension ForgetPasswordViewController: CountrySelectedDelegate {
    
    /// getting data from CountryNameViewController
    ///
    /// - Parameter country: country description
    func countryNameSelected(countrySelected country: Country) {
        self.selectedCountry = country
        DDLogVerbose("country selected  code \(selectedCountry.country_code), country name \(self.selectedCountry.country_name), dial code \(self.selectedCountry.dial_code)")
        
        let imagestring = selectedCountry.country_code
        let imagePath = "CountryPicker.bundle/\(imagestring).png"
        countryImagee.image = UIImage(named: imagePath)
        
        countryCode.text =  self.selectedCountry.dial_code
        /*
         country code is not setting to correct country symbol.
         Sending the selected country code symbole name leke "IN" or "US" to back end during registration.
         We will get again the symbol name fromt the backend where ever we need to dispay the country flag
         Owner : Vani Chikaraddi
         Date : 19/12/2019
         */
        countryCodeSymbol = country.country_code
    }
    
    /// Filter Country Name
    ///
    /// - Parameter searchText: To search text in country name
    func filtercountry(_ countryCode: String) {
        let country = ContryNameModelClass.sharedInstance.localCountry(countryCode)
        let imagestring = country.country_code
        let imagePath = "CountryPicker.bundle/\(imagestring).png"
        countryImagee.image = UIImage(named: imagePath)
        countryCodeSymbol = countryCode
    }
    
}
