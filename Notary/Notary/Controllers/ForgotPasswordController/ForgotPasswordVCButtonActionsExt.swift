//
//  ForgotPasswordButtonActionsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension ForgetPasswordViewController {
    
    // MARK: - Custom Action Methods -
    @IBAction func tapGestureAction(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    @IBAction func nextButtonAction(_ sender: AnyObject) {
        if (phonenumberTF.text?.length)! > 0 || (emailTF.text?.length)! > 0 {
            
            if forgotPasswordWithEmail {
                
                validateAllFields()
                
            }else {
                sendRequestToForgotPasswordUsingEmail()
            }
        }
    }
    
    @IBAction func backButtonAction(_ sender: AnyObject) {
        backButtonMethod()
    }
    
    @IBAction func CountryCodeSelected(_ sender: AnyObject) {
        catransitionAnimation(idntifier: VCIdentifier.countryNameVC)
    }
    
    @IBAction func emailAction(_ sender: Any) {
        self.view.layoutIfNeeded()
        emailViewHeight.constant = nextBottomView.frame.size.width
        UIView.animate(withDuration: 1.4, animations: {
            self.view.layoutIfNeeded()
        })
        phonenumberTF.text = ""
        forgotPasswordViewModel.phoneNumberText.value = ""
//        self.view.endEditing(true)
        forgotPasswordWithEmail = true
        emailButton.setTitleColor(#colorLiteral(red: 0.3882352941, green: 0.4235294118, blue: 0.7725490196, alpha: 1).withAlphaComponent(1), for: .normal)
        phoneButton.setTitleColor(#colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1).withAlphaComponent(0.5), for: .normal)
        emailButtonBottomView.backgroundColor = #colorLiteral(red: 0.3882352941, green: 0.4235294118, blue: 0.7725490196, alpha: 1)
        phoneButtonBottomView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1)
        nextButtonOutlet.setTitle("CONFIRM", for: .normal)
        
        emailTF.becomeFirstResponder()
    }
    
    @IBAction func phoneAction(_ sender: Any) {
        self.view.layoutIfNeeded()
        emailViewHeight.constant = 0
        UIView.animate(withDuration: 1.4, animations: {
            self.view.layoutIfNeeded()
        })
        emailTF.text = ""
        forgotPasswordViewModel.emailText.value = ""
//        self.view.endEditing(true)
        forgotPasswordWithEmail = false
        phoneButton.setTitleColor(#colorLiteral(red: 0.3882352941, green: 0.4235294118, blue: 0.7725490196, alpha: 1).withAlphaComponent(1), for: .normal)
        emailButton.setTitleColor(#colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1).withAlphaComponent(0.5), for: .normal)
        phoneButtonBottomView.backgroundColor = #colorLiteral(red: 0.3882352941, green: 0.4235294118, blue: 0.7725490196, alpha: 1)
        emailButtonBottomView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1)
        nextButtonOutlet.setTitle("NEXT", for: .normal)
        
        phonenumberTF.becomeFirstResponder()
    }
    
}
