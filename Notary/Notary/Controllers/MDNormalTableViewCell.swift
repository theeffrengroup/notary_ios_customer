//
//  MDNormalTableViewCell.swift
//  LiveM
//
//  Created by Apple on 08/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

class MDNormalTableViewCell:UITableViewCell {
    
    @IBOutlet weak var readMoreLabel: UILabel!
    @IBOutlet weak var readMoreButton: UIButton!
    
    @IBOutlet weak var readMoreButtonHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var divider: UIView!
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
       
    }
}
