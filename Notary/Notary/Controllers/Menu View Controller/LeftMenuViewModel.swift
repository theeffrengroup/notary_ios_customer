//
//  LeftMenuModelClass.swift
//  Iserve
//
//  Created by Rahul Sharma on 12/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class LeftMenuViewModel: NSObject {
    
    //Left menu titles
    var menus = ["Book notary",
                 "My Bookings",
                 "Payment method",
//                 "Wallet",
                 "Your addresses",
                 "Support",
                 "Share",
                 "Help center",
                 "Notary-90210"]
    
    //Left menu each title image names
    var menusImages = ["search",
                       "bookings",
                       "paymentMethod",
//                       "wallet",
                       "address",
                       "support",
                       "share",
                       "help",
                       "notary"]
    
    var count: Int {
        return menus.count
    }
}
