//
//  MDHeaderTableViewCell.swift
//  LiveM
//
//  Created by Apple on 08/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

class MDHeaderTableViewCell:UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
}
