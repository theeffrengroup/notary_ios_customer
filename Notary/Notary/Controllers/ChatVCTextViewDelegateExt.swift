//
//  ChatVCTextViewDelegateExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 12/01/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension ChatViewController:UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Type message here.."{
            textView.text = nil
            textView.textColor = UIColor.black
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                self.widthMore.constant = 0
            })
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty {
            textView.text = "Type message here.."
            textView.textColor = Helper.UIColorFromRGB(rgbValue: 0x999999)
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                self.widthMore.constant = 35
            })
        }
        adjustFrames()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            if let text = self.inputText.text {
                if text.length > 0 {
                    self.composeMessage(type: .text, content: self.inputText.text!)
                    textView.text = "Type message here.."
                    textView.textColor = Helper.UIColorFromRGB(rgbValue: 0x999999)
                    UIView.animate(withDuration: 0.5, animations: { () -> Void in
                        self.widthMore.constant = 35
                    })
                }
            }
            textView.resignFirstResponder()
            return false
        }
        adjustFrames()
        return true;
    }
}
