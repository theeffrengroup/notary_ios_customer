//
//  AddressPickupViewController.swift
//  LiveM
//
//  Created by Rahul Sharma on 15/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import GoogleMaps

class AddNewAddressViewController:UIViewController {

    
    @IBOutlet var navigationLeftButton: UIButton!
    @IBOutlet var saveButton: UIButton!
    
    @IBOutlet var mapView: GMSMapView!
    
    @IBOutlet var topAddressView: UIViewCustom!
    
    @IBOutlet var homeButton: UIButton!
    
    @IBOutlet var workButton: UIButton!
    
    @IBOutlet var otherButton: UIButton!
    
    @IBOutlet var otherTextFieldBackView: UIView!

    @IBOutlet var otherTextField: UITextField!
    
    @IBOutlet var addressLabel: UILabel!
    
    @IBOutlet var mapPinBackView: UIView!
    
    @IBOutlet var mapPin: UIImageView!
    
    
    @IBOutlet var topAddressViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var otherTextfieldBackViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var mapPinBackViewCenterYConstraint: NSLayoutConstraint!
    
    
    var currentLatitude = 0.0
    var currentLongitude = 0.0
    
    var pickupLatitude = 0.0
    var pickupLongitude = 0.0
    
    var selectedAddresstype:Int! = 1
    
    var isForEditing:Bool! = false
    
    var isAddressForEditing = false

    let locationObj = LocationManager.sharedInstance()
    
    var addressDetails:AddressModel! = nil
    
    let acessClass = AccessTokenRefresh.sharedInstance()
    
    var apiTag:Int!
    
    var isFromConfirmBookingVC:Bool = false
    
    var addNewAddressViewModel = AddNewAddressViewModel()
    
    var isAddressPickedManually = false
    
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!

    
    // MARK: - Default calss Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setInitialMapViewProperties()
        setInitialAddressviewProperties()
        
        showInitialSpringAnimation()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        acessClass.acessDelegate = self
        setupGestureRecognizer()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        acessClass.acessDelegate = nil
    }


    
    // MARK: - Button Actions -
    
    @IBAction func addressTypeButtonAction(_ sender: Any) {
        
        let button = sender as? UIButton
        
        switch (button?.tag)! {
            
            case 1:
                
                changeButtonState(homeButton)
                selectedAddresstype = homeButton.tag
                showNormalAddressTagFrame()

                break
            
            case 2:
            
                changeButtonState(workButton)
                selectedAddresstype = workButton.tag
                showNormalAddressTagFrame()
                
                break
            
            case 3:
            
                changeButtonState(otherButton)
                selectedAddresstype = otherButton.tag
                otherTextField.text? = ""
                showOtherAddressTagFrame()
            
            break

            
            default:
                break
        }
    }
    
    @IBAction func saveButtonAction(_ sender: Any) {
        
        if selectedAddresstype == 3 {
            
            if (otherTextField.text?.length)!  > 0 {
                
                 self.saveAddressAPI()
                
            } else {
                
                Helper.showAlert(head: ALERTS.Message, message: ALERTS.SelectAddressTag)
            }
            
        } else {
            
            saveAddressAPI()
        }
        
    }
    
    @IBAction func navigationLeftButtonAction(_ sender: Any) {
        
        TransitionAnimationWrapperClass.caTransitionAnimationType(CATransitionType.reveal.rawValue,
                                                                  subType: CATransitionSubtype.fromBottom.rawValue,
                                                                  for: (self.navigationController?.view)!,
                                                                  timeDuration: 0.3)
        self.navigationController?.popViewController(animated: false)
    }
    
    
    @IBAction func searchAddressButtonAction(_ sender: Any) {
        
        let searchLocationVC:SearchLocationViewController = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.searchLocationVC) as! SearchLocationViewController
        
        TransitionAnimationWrapperClass.caTransitionAnimationType(CATransitionType.moveIn.rawValue,
                                                                  subType: CATransitionSubtype.fromTop.rawValue,
                                                                  for: (self.navigationController?.view)!,
                                                                  timeDuration: 0.3)
        
        searchLocationVC.delegate = self
        
        self.navigationController?.pushViewController(searchLocationVC, animated: false)
        
    }
    
    
}

extension AddNewAddressViewController:SearchAddressDelegate {
    
    func searchAddressDelegateMethod(_ addressModel:AddressModel) {
        
        isAddressPickedManually = true
        
        self.addressLabel.text = addressModel.fullAddress
        
        pickupLatitude = Double(addressModel.latitude)!
        pickupLongitude = Double(addressModel.longitude)!
        
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(pickupLatitude),
                                              longitude: CLLocationDegrees(pickupLongitude),
                                              zoom: MAP_ZOOM_LEVEL)
        self.mapView.animate(to: camera)
        
    }
    
    func searchAddressCurrentLocationButtonClicked() {
        
        showCurrentLocation()
    }
    
    /// Method to show current location in MapView
    func showCurrentLocation() {
        
        let location = self.mapView.myLocation
        
        if (location != nil) {
            
            let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees((location?.coordinate.latitude)!),
                                                  longitude: CLLocationDegrees((location?.coordinate.longitude)!),
                                                  zoom: MAP_ZOOM_LEVEL)
            self.mapView.animate(to: camera)
            
        }
        
    }
    
}

extension AddNewAddressViewController:AccessTokeDelegate {
    
    func recallApi() {
        
        switch apiTag {
            
            case RequestType.addAddress.rawValue,RequestType.UpdateAddress.rawValue:
                
                saveAddressAPI()
                
                break
                
            default:
                break
        }
    }
    
}

extension AddNewAddressViewController:UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
}

extension AddNewAddressViewController: UINavigationControllerDelegate {
    
    internal func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}

extension AddNewAddressViewController {
    
    func setupGestureRecognizer() {
        
        guard (navigationController?.viewControllers.count)! > 1 else {
            
            return
        }
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
        
    }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
            
        case .began:
            
            self.navigationController?.delegate = self
            _ = navigationController?.popViewController(animated: true)
            
        case .changed:
            
            if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                percentDrivenInteractiveTransition.finish()
            } else {
                percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
            percentDrivenInteractiveTransition.cancel()
            
        default:
            break
        }
    }
    
    
}
