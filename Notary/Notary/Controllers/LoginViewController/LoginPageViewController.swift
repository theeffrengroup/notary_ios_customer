//
//  LoginPageViewController.swift
//  DayRunner
//
//  Created by NABEEL on 10/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import GoogleSignIn
import Firebase

class LoginPageViewController: UIViewController {
    
    var activeTextField = UITextField()
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var userNameTF: HoshiTextField!
    @IBOutlet weak var passwordTF: HoshiTextField!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var signupOutlet: UIButton!
    @IBOutlet weak var emailCheckMark: UIImageView!
    @IBOutlet weak var passwordCheckMark: UIImageView!
    @IBOutlet weak var bottomView: UIView!
    let fbHandler:FBLoginHandler = FBLoginHandler.sharedInstance()
    let defaults = UserDefaults.standard
    let googleHandler:GmailLoginHandler = GmailLoginHandler.sharedInstance()
    var selectedLoginType : SignInLoginType? = .normal
    var fbData = UserDataModel()
    var socialMediaEmail = ""
    var socialMediaPassword = ""
    var socialMediaId = ""
    var location = LocationManager()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        location = LocationManager.shared
        location.start()
        self.updateUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.setHidesBackButton(true, animated:true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        if Helper.userDataAlreadyExist(kUsernameKey: USER_DEFAULTS.USER.USER_NAME) {
            userNameTF.text = UserDefaults.standard.string(forKey: USER_DEFAULTS.USER.USER_NAME)
        }
        if Helper.userDataAlreadyExist(kUsernameKey: USER_DEFAULTS.USER.USER_PASSWORD) {
            passwordTF.text = UserDefaults.standard.string(forKey: USER_DEFAULTS.USER.USER_PASSWORD)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func updateUI() {
        emailCheckMark.isHidden = true
        passwordCheckMark.isHidden = true
        Helper.shadowView(sender: self.bottomView, width:UIScreen.main.bounds.width, height: 55)
        self.textFieldDidChange(self.passwordTF)
        self.textFieldDidChange(self.userNameTF)
        self.passwordTF.addTarget(self, action: #selector(LoginPageViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        self.userNameTF.addTarget(self, action: #selector(LoginPageViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
    }
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    /*****************************************************************/
    //MARK: - Keyboard Methods
    /*****************************************************************/
    
    func keyboardWillAppear(notification: NSNotification){
        // Do something here
        
        let info = notification.userInfo!
        let inputViewFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let screenSize: CGRect = UIScreen.main.bounds
        var frame = self.view.frame
        frame.size.height = screenSize.height - inputViewFrame.size.height + 55
        self.view.frame = frame
    }
    
    func keyboardWillDisappear(notification: NSNotification){
        // Do something here
        UIView.animate(withDuration: 0.2, animations: {() -> Void in
            let screenSize: CGRect = UIScreen.main.bounds
            var frame = self.view.frame
            frame.size.height = screenSize.height
            self.view.frame = frame
        })
    }
    
    /*****************************************************************/
    //MARK: - UIButton Actions
    /*****************************************************************/
    
    @IBAction func SigninButtonAction(_ sender: AnyObject) {
        dismissKeyboard()
        selectedLoginType = .normal
        loginMethod()
    }
    
    @IBAction func forgetPasswordAction(_ sender: AnyObject) {
        performSegue(withIdentifier: "ForgetPasswordSegue", sender: self)
    }
    
    @IBAction func tapGesture(_ sender: AnyObject) {
        self.view.endEditing(true)
    }
    
    @IBAction func faceBookLoginAction(_ sender: AnyObject) {
        fbHandler.delegate = self
        fbHandler.login(withFacebook: self)
    }
    
    @IBAction func googleLoginAction(_ sender: AnyObject) {
        selectedLoginType = .google
        googleHandler.delegate = self
        googleHandler.login(withGmail: self)
    }
    
    @IBAction func signupAction(_ sender: AnyObject) {
        
        performSegue(withIdentifier: "signupSegue", sender: self)
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ForgetPasswordSegue" {
            
            let nav = segue.destination as! UINavigationController
            if let forgetPass: ForgetPasswordViewController = nav.viewControllers.first as! ForgetPasswordViewController?{
                forgetPass.delegate = self
            }
        }else if segue.identifier == "signupSegue" {
            let nav = segue.destination as! UINavigationController
            let controller : SignUpViewController = nav.viewControllers.first as! SignUpViewController
            if self.selectedLoginType != .normal {
                controller.userData = fbData
            }
            controller.selectedLoginType = (selectedLoginType?.rawValue).map { SignUpLoginType(rawValue: $0) }!
            controller.socialMediaId = socialMediaId
            selectedLoginType = .normal
        }
    }
    
    /*****************************************************************/
    //MARK: - Validations
    /*****************************************************************/
    
    func loginMethod() {
        userNameTF.resignFirstResponder()
        passwordTF.resignFirstResponder()
        if  (userNameTF.text?.isEmpty)! {
            self.present(Helper.alertVC(title: ALERTS.Missing as NSString , message:ALERTS.Email_Miss as NSString), animated: true, completion: nil)
            return
        } else if(passwordTF.text?.isEmpty)! {
            self.present(Helper.alertVC(title: ALERTS.Missing as NSString , message:ALERTS.Pasword as NSString), animated: true, completion: nil)
            return
        } else if !Helper.isValidEmail(emailText: userNameTF.text!){
            if !Helper.validatePhone(value: userNameTF.text!){
                self.present(Helper.alertVC(title: ALERTS.Wrong as NSString , message:ALERTS.mail as NSString), animated: true, completion: nil)
                return
            }
        }
        self.sendRequestForLogin()
    }
    
    func sendRequestForLogin() {
        var emailString = ""
        var passwordString = ""
        if selectedLoginType == .normal {
            emailString = userNameTF.text!
            passwordString = passwordTF.text!
        }
        else {
            emailString = socialMediaEmail
            passwordString = socialMediaPassword
        }
        let params : [String : Any] =   [APIParameterConstants.login.Email_Phone   : emailString,
                                         APIParameterConstants.login.Password      : passwordString,
                                         APIParameterConstants.login.DeviceID      : DEVICE_TOKEN,
                                         APIParameterConstants.login.PushToken     : Utility.pushtoken,
                                         APIParameterConstants.login.Version       : Utility.appVersion,
                                         APIParameterConstants.login.DeviceMake    : "Apple",
                                         APIParameterConstants.login.DeviceModel   : Utility.deviceName,
                                         APIParameterConstants.login.DeviceType    : Utility.userType,
                                         APIParameterConstants.login.DeviceTime    : Helper.currentDateTime,
                                         APIParameterConstants.login.LoginType     : selectedLoginType!.rawValue,
                                         APIParameterConstants.login.SocialID      : socialMediaId,
                                         APIParameterConstants.login.Name          : fbData.firstName + " " + fbData.lastName,
                                         APIParameterConstants.login.Mobile        : "",
                                         APIParameterConstants.login.Country       : "",
                                         APIParameterConstants.login.ProfilePic    : fbData.imageUrlString,
                                         APIParameterConstants.login.ZipCode       : location.zipcode,
                                         APIParameterConstants.login.Lat           : location.latitute,
                                         APIParameterConstants.login.Long          : location.longitude,
                                         APIParameterConstants.login.AccountType   : 1]
        let login = LoginModel()
        login.LoginDelegate = self
        login.sendRequestForLogin(data: params)
    }
}

// MARK: - UITextFieldDelegate

extension LoginPageViewController : UITextFieldDelegate {
    
    //MARK: - Custom Methods -
    func textFieldDidChange(_ textField: UITextField) {
        if !(userNameTF.text?.isValidEmail)!{
            if !Helper.validatePhone(value: userNameTF.text!) {
                emailCheckMark.isHidden = true
            }else{
                emailCheckMark.isHidden = false
            }
        }else {
            emailCheckMark.isHidden = false
        }
        if passwordTF.text?.length == 0 {
            passwordCheckMark.isHidden = true
        }else {
            passwordCheckMark.isHidden = false
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeTextField = textField
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range:NSRange, replacementString string: String) -> Bool {
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //delegate method
        
        if textField .isEqual(userNameTF) {
            passwordTF.becomeFirstResponder()
        } else {
            passwordTF.resignFirstResponder()
        }
        return true
    }
}
extension LoginPageViewController : facebookLoginDelegate {
    
    func didFacebookUserLogin(withDetails userInfo:NSDictionary) {
        let modalData = UserDataModel()
        fbData = modalData.initArray(str: userInfo as! [String : AnyObject])
        print("FBDetails =",userInfo)
        socialMediaPassword = ((userInfo["id"]! as AnyObject) as? String)!
        socialMediaId = ((userInfo["id"]! as AnyObject) as? String)!
        if let temp = userInfo["email"] as? String {
            socialMediaEmail = temp
        }else{
            socialMediaEmail = fbData.mailId
        }
        socialMediaPassword = ((userInfo["id"]! as AnyObject) as? String)!
        socialMediaId = ((userInfo["id"]! as AnyObject) as? String)!
        selectedLoginType = .facebook
        sendRequestForLogin()
    }
    func didFailWithError(_ error: Error?) {
        
    }
    func didUserCancelLogin() {
        
    }
}


extension LoginPageViewController : GmailLoginHandlerDelegate {
    
    func didGetDetailsFromGoogle(email: String, name: String, token: String, userId: String, profile: GIDProfileData) {
        let modalData = UserDataModel()
        fbData = modalData.initArrayGoogle(str: profile, userIdReached: userId)
        socialMediaEmail = email
        socialMediaPassword = userId
        socialMediaId = userId
        selectedLoginType = .google
        self.sendRequestForLogin()
    }
}
extension LoginPageViewController : LoginDelegate {
    func LoginDelegate(success: Bool, message: String) {
        if success {
            let defaults = UserDefaults.standard
            defaults.set(self.selectedLoginType?.rawValue, forKey: USER_DEFAULTS.USER.LoginType)
            defaults.set(self.userNameTF.text!, forKey: USER_DEFAULTS.USER.USER_NAME)
            defaults.set(self.passwordTF.text!, forKey: USER_DEFAULTS.USER.USER_PASSWORD)
            defaults.synchronize()
            let modalForProfile = ModalForProfile()
            modalForProfile.sendRequestForProfileType()
            DispatchQueue.main.async(execute: {
                let mainVcIntial = kConstantObj.SetIntialMainViewController(Storyboard_Identifier.HOME_VC)
                let window = UIApplication.shared.keyWindow
                window?.rootViewController = mainVcIntial
            })
        }else{
            if self.selectedLoginType != .normal && message.characters.count == 0 {
                self.signupAction(UIButton())
                return
            }else {
                self.present(Helper.alertVC(title: ALERTS.Message, message:message as NSString), animated: true, completion: nil)
            }
        }
    }
}
extension LoginPageViewController : ForgetPasswordViewControllerDelegate {
    func changedPassword() {
        
    }
    
}
