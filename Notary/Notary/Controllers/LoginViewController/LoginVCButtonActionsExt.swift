//
//  LoginVCButtonActionsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import FacebookLogin

extension LoginViewController {
    
    // MARK: - Action Methods -
    
    /// Fb login button action
    ///
    /// - Parameter sender: Action
    @IBAction func fbLoginAction(_ sender: Any) {
        
        let fbLoginManager:LoginManager = LoginManager.init()
        fbLoginManager.logOut()
        
        let fbLoginHandler = FBLoginHandler.sharedInstance()
        fbLoginHandler.delegate = self
        fbLoginHandler.login(withFacebook: self)
        
    }
    
    /// Login Button
    ///
    /// - Parameter sender: Action
    @IBAction func loginAction(_ sender: Any) {
        
        if loginButton.frame.size.width == loadingView.frame.size.width {
            self.view.endEditing(true)
            validateAllFields()
        }else {
            checkFortheField()
        }
    }
    
    
    /// move controller to last controller
    ///
    /// - Parameter sender: Action
    @IBAction func backButtonAction(_ sender: Any) {
        backButton()
        
    }
    
    /// Forget Password action Button
    ///
    /// - Parameter sender: action
    @IBAction func forgetPasswordAction(_ sender: Any) {
        self.performSegue(withIdentifier: SEgueIdetifiers.LoginToForgetPassordVC, sender: nil)
        //        showForgotPasswordAlertActionSheet()
    }
    
    @IBAction func registerAction(_ sender: Any) {
        catransitionAnimation(idntifier: VCIdentifier.signinVC)
    }
    
    @IBAction func tapGestureAction(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    
}
