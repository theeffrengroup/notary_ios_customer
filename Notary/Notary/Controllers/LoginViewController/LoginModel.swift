//
//  LoginModel.swift
//  DayRunner
//
//  Created by Rahul Sharma on 04/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
protocol LoginDelegate {
    func LoginDelegate(success:Bool,message:String)
}
enum SignInLoginType : Int {
    
    case normal = 1
    case facebook = 2
    case google = 3
}

class LoginModel: NSObject {
    var LoginDelegate:LoginDelegate! = nil
    
    func sendRequestForLogin(data : [String : Any]) {
        Helper.showPI(_message: ALERTS.loggingin)
        NetworkHelper.requestPOST(serviceName:API.METHOD.LOGIN,
                                  params: data,
                                  success: { (response : [String : Any]) in
                                    print("login response : ",response)
                                    Helper.hidePI()
                                    if response.isEmpty == false {
                                        
                                        if let errFlag: Int = response["errFlag"] as? Int {
                                            
                                            if errFlag == 0 {
                                                
                                                print("Login successful")
                                                
                                                if let dict = response["data"] as? [String:AnyObject] {
                                                    self.parseData(dict: dict)
                                                }
                                            }
                                            else if errFlag == 1 {
                                                if self.LoginDelegate != nil {
                                                    self.LoginDelegate.LoginDelegate(success: false, message: response["errMsg"] as! String)
                                                }
                                            }
                                        } else {
                                            
                                            if let statusCode: ERRORS = ERRORS(rawValue: (response["statusCode"] as? Int)!) {
                                                
                                                if statusCode == .MISSING {
                                                    if self.LoginDelegate != nil {
                                                        self.LoginDelegate.LoginDelegate(success: false, message: response["message"] as! String)
                                                    }
                                                }
                                            }
                                        }
                                    }
        }) { (Error) in
            if self.LoginDelegate != nil {
                self.LoginDelegate.LoginDelegate(success: false, message: Error.localizedDescription)
            }
        }
    }
    
    func parseData(dict:[String:AnyObject]) {
        let defaults = UserDefaults.standard
        defaults.set(dict["token"] as! String, forKey: USER_DEFAULTS.TOKEN.SESSION)
        defaults.set(dict["presence_chn"] as! String, forKey: USER_DEFAULTS.CHANNEL.PRESENCE_CHANNEL)
        defaults.set(dict["pub_key"] as! String, forKey: USER_DEFAULTS.CHANNEL.PUBLISH_CHANNEL)
        defaults.set(dict["Name"] as! String, forKey: USER_DEFAULTS.USER.NAME)
        defaults.set(dict["mobile"] as! String, forKey: Utility.phone)
        defaults.set(dict["sub_key"] as! String, forKey: USER_DEFAULTS.CHANNEL.SUBSCRIBE_CHANNEL)
        defaults.set(dict["chn"] as! String, forKey: USER_DEFAULTS.CHANNEL.MY_CHANNEL)
        defaults.set(dict["server_chn"] as! String, forKey: USER_DEFAULTS.CHANNEL.SERVER_CHANNEL)
        defaults.set(dict["referralcode"] as! String, forKey: USER_DEFAULTS.USER.REFERAL)
        defaults.set(dict["sid"] as! String, forKey:Utility.sid)
        defaults.synchronize()
        
        if Utility.myChannel.isEmpty == false {
            (VNHPubNubWrapper.sharedInstance() as! VNHPubNubWrapper).subscribeToMyChannel()
        }
        if LoginDelegate != nil {
            LoginDelegate.LoginDelegate(success: true, message: "")
        }
    }
}
