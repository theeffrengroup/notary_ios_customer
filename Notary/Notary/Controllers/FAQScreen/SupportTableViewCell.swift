//
//  FAQTableViewCell.swift
//  LiveM
//
//  Created by Rahul Sharma on 11/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class SupportTableViewCell: UITableViewCell {
    
    // MARK: - Action methods -
    // Label
    @IBOutlet weak var tittleLabel: UILabel!
    @IBOutlet weak var discriptionLabel: UILabel!
    
    @IBOutlet weak var heightOfDisLabel: NSLayoutConstraint!
    // Button
    @IBOutlet weak var buttonOutlet: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
