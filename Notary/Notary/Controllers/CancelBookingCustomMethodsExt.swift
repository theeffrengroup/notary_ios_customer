//
//  CancelBookingCustomMethods.swift
//  LiveM
//
//  Created by Rahul Sharma on 04/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension CancelBookingScreen {
    
    
    /// Method to show cancellation reasons list
    ///
    /// - Parameter cancelReasonResponse: list of cancellation resons
    func showCancelReasons(cancelReasonResponse:[Any]) {
        
        arrayOfCancelReasons = []
        
        for cancelReason in cancelReasonResponse {
            
            let cancelReasonModel = CancelReasonModel.init(cancelReasonDetails:cancelReason)
            arrayOfCancelReasons.append(cancelReasonModel)
        }
        
        self.tableView.reloadData()

    }
    
}
