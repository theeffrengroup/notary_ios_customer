//
//  HomeListVCMQTTDelegatesExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension HomeListViewController:MusiciansListManagerDelegate {
    
    func emptyMusicianList(errMessage:String) {
        
        if arrayOfProvidersModel.count > 0 {
        
            arrayOfProvidersModel = []
            arrayOfAvailableNowProviders = []
            arrayOfAvailableLaterProviders = []
//            updateScrollViewConstraint()
        
            //Reload TableView
            self.tableView.reloadData()
//            updateScrollViewConstraint()
            
        } else {
            
            self.messageLabel.text = musiciansListManager.errMessageString
        }
        
    }
    
    func showInitialMusicianList(arrayOfUpdatedMusiciansModel: [MusicianDetailsModel],arrayOfMusiciansList:[Any]) {
        
        // Load First time
        arrayOfProvidersModel = arrayOfUpdatedMusiciansModel
        arrayOfMusicians = arrayOfMusiciansList
        
        self.parseBookNowAndLaterNotaries()
        
        //Reload TableView
        self.tableView.reloadData()
        
    }
    
    func updateMusicianList(arrayOfUpdatedMusicians: [MusicianDetailsModel], arrayOfRowsToAdd: [Int], arrayOfRowsToRemove: [Int], arrayOfMusicianIdToRemove:[String], arrayOfMusiciansList:[Any]) {
        
        if arrayOfProvidersModel.isEmpty {
            
            showInitialMusicianList(arrayOfUpdatedMusiciansModel: arrayOfUpdatedMusicians, arrayOfMusiciansList: arrayOfMusiciansList)
            
        } else {
            
            var needToReloadTableView = false
            if arrayOfMusiciansList.count == arrayOfMusicians.count {
                
                let previous: NSArray = arrayOfMusicians as NSArray
                let updated: NSArray = arrayOfMusiciansList as NSArray
                
                if previous.isEqual(to: updated as! [Any]) {
                    // Both are Equal
                    // Do not do anything
                    DDLogDebug("Notaries List is Same")
                    
                }else {
                    
                    // Both are not equal
                    DDLogDebug("Updated Notaries List is not equal")
                    needToReloadTableView = true
                }
                
            }
                
            arrayOfMusicians = arrayOfMusiciansList
            arrayOfProvidersModel = arrayOfUpdatedMusicians
            
            if arrayOfRowsToAdd.count > 0 || arrayOfRowsToRemove.count > 0 || needToReloadTableView {
                
                self.parseBookNowAndLaterNotaries()
//                updateScrollViewConstraint()
                
                //Reload TableView
                self.tableView.reloadData()
                
//                updateScrollViewConstraint()
                
            } else {
                
//                let when = DispatchTime.now() + 0.4
//                DispatchQueue.main.asyncAfter(deadline: when){
//
//                    self.updateScrollViewConstraint()
//                }
            }

           
        }
        
    }
    
    func parseBookNowAndLaterNotaries() {
        
        arrayOfAvailableNowProviders = []
        arrayOfAvailableLaterProviders = []
        
        for i in 0..<arrayOfProvidersModel.count {
            
            let musicianDetails = arrayOfProvidersModel[i]
            
            if appointmentLocationModel.bookingType == .Schedule {
                
                arrayOfAvailableNowProviders.append(musicianDetails)
                
            } else {
                
                if musicianDetails.status == 0 {
                    
                    arrayOfAvailableLaterProviders.append(musicianDetails)
                    
                } else {
                    
                    arrayOfAvailableNowProviders.append(musicianDetails)
                }
            }
            
        }
    }
    
    /*func removeRowFromTableView(indexPath:IndexPath) {
        
        if let musicianCell = self.tableView.cellForRow(at: indexPath) as? HomeListTableViewCell {
            
            DDLogVerbose("\(musicianCell)")
            
            if #available(iOS 11.0, *) {
                
                self.tableView.performBatchUpdates({
                    self.tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.none)
                }, completion:nil)
                
            } else {
                
                // Fallback on earlier versions
                self.tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.none)
                self.tableView.endUpdates()
            }
        }
        
    }
    
    func insertRowFromTableView(indexPath:IndexPath) {
        
        if #available(iOS 11.0, *) {
            
            self.tableView.performBatchUpdates({
                self.tableView.insertRows(at: [indexPath], with: UITableViewRowAnimation.none)
            }, completion:nil)
            
        } else {
            
            // Fallback on earlier versions
            self.tableView.insertRows(at: [indexPath], with: UITableViewRowAnimation.none)
            self.tableView.endUpdates()
        }
        
    }*/
    
    
    
    
    /*func updateProviderTableViewData() {
        
        for i in 0..<arrayOfProvidersModel.count {
            
            if let musicianCell = self.tableView.cellForRow(at: IndexPath.init(row: i, section: 0)) as? HomeListTableViewCell {
                
                let musicianDetails = arrayOfProvidersModel[i]
                
                musicianCell.nameLabel.text = musicianDetails.firstName.capitalized + " " + musicianDetails.lastName.capitalized
                musicianCell.distanceLabel.text = Helper.getDistanceDependingMileageMetricFromServer(distance: musicianDetails.distance, mileageMatric: musicianDetails.mileageMatric)
                
                if musicianDetails.status == 0 { 
                    
                    musicianCell.statusLabel.text = "OFFLINE"
                    musicianCell.statusLabel.textColor = HOME_LIST_COLOR_CODE.OFFLINE
                    
                } else {
                    
                    musicianCell.statusLabel.text = "ONLINE"
                    musicianCell.statusLabel.textColor = HOME_LIST_COLOR_CODE.ONLINE
                }
                
                musicianCell.ratingView.rating = Float(musicianDetails.overallRating)
                
//                musicianCell.priceLabel.text = String(format:"%@ %.2f", musicianDetails.currencySymbol, musicianDetails.amount)
                
                if musicianCell.musicianDetailsModel.providerId != musicianDetails.providerId {
                    
                    musicianCell.musicianDetailsModel = musicianDetails
                    musicianCell.collectionView.reloadData()
                    
                } else {
                    
                    let previous: NSArray = musicianCell.musicianDetailsModel.workImagesArray as NSArray
                    let updated: NSArray = musicianDetails.workImagesArray as NSArray
                    
                    if !previous.isEqual(to: updated as! [Any]) {
                        
                        musicianCell.musicianDetailsModel = musicianDetails
                        musicianCell.collectionView.reloadData()
                    }
                }
            }
        }
    }*/
    
    
}
