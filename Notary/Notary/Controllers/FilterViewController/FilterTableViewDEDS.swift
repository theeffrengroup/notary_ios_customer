//
//  FilterTableViewDEDS.swift
//  DayRunner
//
//  Created by Rahul Sharma on 04/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import UIKit

extension FilterViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let cellType = cellType(rawValue: indexPath.row){
            switch cellType {
            case .gigTime :
                return 77
               
            case .eventType:
                return  300
                
            case .priceRange:
                return 120
                
            case .OperationRadius:
                return 100
                
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
    }
    
}

extension FilterViewController: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cellType = cellType(rawValue: indexPath.row){
            switch cellType {
            case .gigTime :
                let cell: FilterGigTimeTVCell = tableView.dequeueReusableCell(withIdentifier: "GigTime", for: indexPath) as! FilterGigTimeTVCell
                return cell
                
            case .eventType:
                let cell: FilterEventTypeTVCell = tableView.dequeueReusableCell(withIdentifier: "EventType", for: indexPath) as! FilterEventTypeTVCell
                return cell
                
            case .priceRange:
                let cell: FilterPriceRangeTVCell = tableView.dequeueReusableCell(withIdentifier: "PriceRange", for: indexPath) as! FilterPriceRangeTVCell
                return cell
            
                
            default:
                let cell: FilterRadiusOperationTVCell = tableView.dequeueReusableCell(withIdentifier: "RadiusOperation", for: indexPath) as! FilterRadiusOperationTVCell
                return cell

                break
                
            }
        } else {
            let cell: FilterRadiusOperationTVCell = tableView.dequeueReusableCell(withIdentifier: "RadiusOperation", for: indexPath) as! FilterRadiusOperationTVCell
            return cell
        }
        
    }
    
}



