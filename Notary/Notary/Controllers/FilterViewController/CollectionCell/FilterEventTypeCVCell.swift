//
//  FilterEventTypeCVCell.swift
//  DayRunner
//
//  Created by Rahul Sharma on 04/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class FilterEventTypeCVCell: UICollectionViewCell {
    
    // MARK: - Outlets -
    // Image View
    @IBOutlet weak var ItemImage: UIImageView!
    
    // Label
    @IBOutlet weak var titleLabel: UILabel!
    
}
