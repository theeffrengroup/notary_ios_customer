//
//  FilterGigTimeTVCell.swift
//  DayRunner
//
//  Created by Rahul Sharma on 04/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class FilterGigTimeTVCell: UITableViewCell {

    @IBOutlet weak var gigTimeCollectionView: UICollectionView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        gigTimeCollectionView.dataSource = self
        gigTimeCollectionView.delegate = self 
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

