//
//  ReceiptVCAddressDetailsCell.swift
//  Notary
//
//  Created by 3Embed on 26/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

class ReceiptVCAddressDetailsCell: UITableViewCell {
    
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBOutlet weak var topView: UIView!

}
