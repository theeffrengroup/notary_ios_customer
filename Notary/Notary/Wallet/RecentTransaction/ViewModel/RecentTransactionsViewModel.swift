//
//  RecentTransactionsViewModel.swift
//  Notary
//
//  Created by 3Embed on 19/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class RecentTransactionsViewModel {
    
    let disposebag = DisposeBag()
    
    func getWalletTransactionsAPICall(pageIndex:Int,
                                        completion:@escaping (Int,String?,Any?) -> ()) {
        
        let rxWalletAPI = WalletAPI()
        rxWalletAPI.getWalletTransactionsServiceAPICall(pageIndex: pageIndex)
        
        if !rxWalletAPI.getWalletTransactions_Response.hasObservers {
            
            rxWalletAPI.getWalletTransactions_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
        
    }
}
