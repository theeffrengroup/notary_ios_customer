//
//  RTModel.swift
//  DayRunner
//
//  Created by apple on 9/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class RTModel: NSObject {
    
    static let sharedInstance = RTModel()
    
    let recentTransactionViewModel = RecentTransactionsViewModel()
    
        
    ///Get Recent Transactions Data
    ///
    /// - Parameters:
    /// - index: Index number
    /// - success: return an array of RTModel
    func getWalletTransactionsAPI(index: Int,
                                   success:@escaping (RTDataType?) -> Void) {
        
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        
        recentTransactionViewModel.getWalletTransactionsAPICall(pageIndex: index){ (statCode, errMsg, dataResp) in
            
            if statCode == HTTPSResponseCodes.SuccessResponse.rawValue {
                
                if let data = dataResp as? [String:Any] {
                    success(RTDataType(data: data))
                }
            }
        }
 
    }
}
