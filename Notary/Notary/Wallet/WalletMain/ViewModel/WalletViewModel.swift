//
//  WalletViewModel.swift
//  Notary
//
//  Created by 3Embed on 19/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class WalletViewModel {
    
    let disposebag = DisposeBag()
    
    
    func getTheUpdatedWalletDataAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        let rxWalletAPI = WalletAPI()
        rxWalletAPI.getWalletDetailsServiceAPICall()
        
        if !rxWalletAPI.getWalletDetails_Response.hasObservers {
            
            rxWalletAPI.getWalletDetails_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
    }
    
    func rechargeWalletAPICall(cardId:String,
                               amount:String,
                               completion:@escaping (Int,String?,Any?) -> ()) {
        
        let rxWalletAPI = WalletAPI()
        rxWalletAPI.rechargeWalletServiceAPICall(cardId: cardId,
                                                 amount: amount)
        
        if !rxWalletAPI.rechargeWallet_Response.hasObservers {
            
            rxWalletAPI.rechargeWallet_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
    }
}
