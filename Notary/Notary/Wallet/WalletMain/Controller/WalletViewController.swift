//
//  WalletViewController.swift
//  DayRunner
//
//  Created by apple on 9/6/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

enum WalletSectionType: Int {
    case currentCredit = 0
    case cardDetails = 1
}

enum WalletRowType: Int {
    case seperator = 0
    case Default = 1
}

class WalletViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var activeTextField = UITextField()
    //    fileprivate let pubnubObj = VNHPubNubWrapper.sharedInstance() as! VNHPubNubWrapper
    var enterAmt = ""
//    var cardID = ""
//    var cardBrand = ""
//    var last4 = ""
    
    var selectedCardModel:CardDetailsModel!

    
    @IBOutlet weak var walletMsg: UILabel!
    
    // MARK: - Variable Decleratons -
    weak var delegate: LeftMenuProtocol?
    
    let walletViewModel = WalletViewModel()
    var apiTag:Int!
    let acessClass = AccessTokenRefresh.sharedInstance()
    var appDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
    var isFromAPI = false
    
    // Shared instance object for gettting the singleton object
    static var obj:WalletViewController? = nil
    
    class func sharedInstance() -> WalletViewController {
        
        return obj!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        WalletViewController.obj = self
        getDefaultCardDetails()
        showWalletBalanceMessage()
        
        isFromAPI = false
        self.getTheUpdatedWalletDataAPI()
        
        // Do any additional setup after loading the view.
    }
    
    func getDefaultCardDetails() {
        
        var defaultCardArray:[Any] = []
        
        if !Utility.defaultCardDocId.isEmpty {
            
            defaultCardArray = PaymentCardManager.sharedInstance.getDefaultCardDocumentDetailsFromCouchDB(documentId: Utility.defaultCardDocId)
        }
        
        if defaultCardArray.count > 0 {
            
            self.selectedCardModel = CardDetailsModel.init(selectedCardDetails: defaultCardArray[0])
        }
    }
    
    func showWalletBalanceMessage() {
        
        if Utility.walletTotalAmount != "0" && Utility.walletTotalAmount != "0.0" && Utility.walletTotalAmount != "0.00" {
            
            walletMsg.text = "\(APP_NAME) app owes you " + Utility.currencySymbol + Utility.walletTotalAmount + "Amount"
            
        }else{
            
            walletMsg.text = "You owe the \(APP_NAME) app " + Utility.currencySymbol + Utility.walletTotalAmount + "Amount"
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        acessClass.acessDelegate = self
        appDelegate?.keyboardDelegate = self
        self.tableView.reloadSections([1], with: .none)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        acessClass.acessDelegate = nil
        appDelegate?.keyboardDelegate = nil
    }
    
    
    @IBAction func btnSideMenuPressed(_ sender: UIButton) {
//        if Utility.getLanguage == "2" {
//            slideMenuController()?.toggleRight()
//        } else {
//            slideMenuController()?.toggleLeft()
//        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapGesture(_ sender: Any) {
        
        self.view.endEditing(true)
    }
    
    @IBAction func confirmAndPayAction(_ sender: Any) {
        let indexPath = IndexPath(row: WalletRowType.Default.rawValue, section: WalletSectionType.cardDetails.rawValue)
        if let cell = self.tableView.cellForRow(at: indexPath) as? WalletCardCell{
            
            if cell.amountTF.text?.length == 0 {
                
                Helper.alertVC(title: ALERTS.Message , message: ALERTS.WALLET.MissingAmount)
            }
            else {
                
                self.sendRequestForPayment()
            }
        }
    }
    
    func sendRequestForPayment() {
        
        if self.selectedCardModel != nil {
            
            showWarning()
            
        } else {
            
            Helper.alertVC(title: ALERTS.Message, message: ALERTS.WALLET.SelectCard)
        }
        
    }

    
//    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        // Get the new view controller using segue.destinationViewController.
//        // Pass the selected object to the new view controller.
//        switch segue.identifier! as String  {
//        case "cardSegue":
//            if let cardVC = segue.destination as? CardViewController {
//                cardVC.delegate = self
//            }
//        default:
//            break
//        }
//    }
    
    @objc func cardButtonClicked() {
//        self.performSegue(withIdentifier: "cardSegue", sender: self)
        
        self.view.endEditing(true)
        
        let paymentVC:PaymentViewController = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.paymentVC) as! PaymentViewController
        
        paymentVC.isFromConfirmBookingVC = false
        paymentVC.isFromWalletVC = true
        
        self.navigationController!.pushViewController(paymentVC, animated: true)
    }
    
    @objc func recentTransactionButton() {
        self.performSegue(withIdentifier:"recentTransactionSegue", sender: self)
    }
    
    /*/// Move View Down / Normal Position When keyboard disappears
    func moveViewDown() {
        
        self.tableView.contentOffset = CGPoint(x: 0, y: 0)
    }
    /*****************************************************************/
    //MARK: - Keyboard Methods
    /*****************************************************************/
    
    func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self .moveViewUp(activeView: activeTextField, keyboardHeight: keyboardSize.height)
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            self.moveViewDown()
        }
    }
    
    /// Move View Up When keyboard Appears
    ///
    /// - Parameters:
    ///   - activeView: View that has Became First Responder
    ///   - keyboardHeight: Keyboard Height
    
    func moveViewUp(activeView: UIView, keyboardHeight: CGFloat) {
        
        // Get Max Y of Active View with respect their Super View
        var viewMAX_Y = activeView.frame.maxY
        
        // Check if SuperView of ActiveView lies in Nexted View context
        // Get Max for every Super of ActiveTextField with respect to Views SuperView
        if var view: UIView = activeView.superview {
            
            // Check if Super view of View in Nested Context is View of ViewController
            while (view != self.view.superview) {
                viewMAX_Y += view.frame.minY
                view = view.superview!
            }
        }
        
        // Calculate the Remainder
        let remainder = self.view.frame.height - (viewMAX_Y + keyboardHeight)
        
        // If Remainder is Greater than 0 does mean, Active view is above the and enough to see
        if (remainder >= 0) {
            // Do nothing as Active View is above Keyboard
        }
        else {
            // As Active view is behind keybard
            // ScrollUp View calculated Upset
            UIView.animate(withDuration: 0.4,
                           animations: { () -> Void in
                            self.tableView.contentOffset = CGPoint(x: 0, y: -remainder)
                            print("remainder.........", remainder)
            })
        }
        // Set ContentSize of ScrollView
        //        var contentSizeOfContent: CGSize = self.contentScrollView.frame.size
        //        contentSizeOfContent.height += keyboardHeight
        //        self.mainScrollView.contentSize = contentSizeOfContent
    }*/
    
    
    
    /// Show Default Alert message
    private func showWarning() {
        alertForConfirmation()
    }
    
    func alertForConfirmation(){
        let alert = UIAlertController(title: "Confirm", message: "Are you sure you want to recharge your wallet with " + Utility.currencySymbol + self.enterAmt + "?", preferredStyle: .alert)
        let yesButton = UIAlertAction(title: "Yes", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            self.rechargeWalletAPI()
        })
        let noButton = UIAlertAction(title: "No", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            
        })
        alert.addAction(noButton)
        alert.addAction(yesButton)
        self.present(alert, animated: true, completion: nil)
    }
}

extension WalletViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension WalletViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let sectionCell = tableView.dequeueReusableCell(withIdentifier: "WalletSectionCell") as! WalletSectionCell
        
        switch WalletSectionType(rawValue: indexPath.section)! {
        case .currentCredit:
            
            let cell = tableView.dequeueReusableCell(withIdentifier:"WalletCurrentCreditCell") as! WalletCurrentCreditCell
            cell.recentTrasactionBtn.addTarget(self,
                                               action: #selector(recentTransactionButton), for: UIControl.Event.touchUpInside)
            cell.hardLimitLbl.text = Utility.currencySymbol + Utility.walletHardLimit
            cell.softLimitLbl.text = Utility.currencySymbol + Utility.walletSoftLimit
            cell.currentCreditLbl.text = Utility.currencySymbol + Utility.walletTotalAmount + "*"
            return cell
            
        case.cardDetails:
            
            switch WalletRowType(rawValue: indexPath.row)! {
            case .seperator:
                
                return sectionCell
                
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier:"WalletCardCell") as! WalletCardCell
                cell.selectCard.addTarget(self,
                                          action: #selector(cardButtonClicked),
                                          for: UIControl.Event.touchUpInside)
                
                cell.currencySymbol.text = Utility.currencySymbol
                
                if selectedCardModel != nil {
                    
                    cell.cardNo.text = "Card Ending with " + selectedCardModel.last4Digits
                    //"**** **** ****" + Utility.defaultCardLast4Digit
                    cell.cardImageView.image = Helper.cardImage(with: selectedCardModel.brand)
                    
                } else {
                    
                    cell.cardNo.text = "PLEASE SELECT CARD"
                    cell.cardImageView.image = Helper.cardImage(with: "")
                }
                return cell
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch WalletSectionType(rawValue: indexPath.section)! {
        case .currentCredit:
            return 148
            
        case .cardDetails:
            
            switch WalletRowType(rawValue: indexPath.row)! {
            case .seperator:
                return 20
            default:
                return 200
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch WalletSectionType(rawValue: section)! {
        case .currentCredit:
            
            return 1
            
        case .cardDetails:
            
            return 2
        }
    }
}

extension WalletViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.activeTextField = textField
//        self.moveViewUp(activeView: activeTextField, keyboardHeight: 250)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.enterAmt = textField.text!
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        self.enterAmt = textField.text!
//        self.moveViewDown()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var textFieldText = String()
        
        if range.length == 0 {//Entered New Character
            
            textFieldText = textField.text! + string
        }
        else {//Entered BackSpace
            
            textFieldText = String(textField.text!.dropLast())
        }
        self.enterAmt = textFieldText
        
        return true
    }
}

extension WalletViewController:KeyboardDelegate {
    
    // MARK: - Keyboard Methods -
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillShow(notification: NSNotification) {
        
        //Need to calculate keyboard exact size due to Apple suggestions
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize!.height, right: 0.0)
        
        self.tableView.contentInset = contentInsets
        self.tableView.scrollIndicatorInsets = contentInsets
        
        self.tableView.scrollToRow(at: IndexPath.init(row: 0, section: 1), at: UITableView.ScrollPosition.middle, animated: true)
        
    }
    
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillHide(notification: NSNotification) {
        
        //Once keyboard disappears, restore original positions
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0,bottom: 0.0,right: 0.0)
        
        UIView.animate(withDuration: 0.2) {
            
            self.tableView.contentInset = contentInsets
            self.tableView.scrollIndicatorInsets = contentInsets
        }
        
    }
}

extension WalletViewController:AccessTokeDelegate {
    
    func recallApi() {
        
        switch apiTag {
            
            case RequestType.GetWalletDetails.rawValue:
                
                self.getTheUpdatedWalletDataAPI()
                
                break
            
            case RequestType.RechargeWallet.rawValue:
                
                rechargeWalletAPI()
                
                break
            
            default:
                break
        }
    }
    
}



