//
//  ReceiptVCSignatureTableCell.swift
//  Notary
//
//  Created by Rahul Sharma on 30/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation
import Kingfisher

class ReceiptVCSignatureTableCell: UITableViewCell {
    
    @IBOutlet weak var signatureImageView: UIImageView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    func showSignatureDetails(bookingDetails:BookingDetailsModel) {
        
        if bookingDetails.signatureURL.length > 0 {
            
            activityIndicator.startAnimating()
            
            signatureImageView.kf.setImage(with: URL(string: bookingDetails.signatureURL),
                                           placeholder:nil,
                                           options: [.transition(ImageTransition.fade(1))],
                                           progressBlock: { receivedSize, totalSize in
            },
                                           completionHandler: { image, error, cacheType, imageURL in
                                            
                                            self.activityIndicator.stopAnimating()
                                            
            })
            
        } else {
            
            signatureImageView.image = nil
        }
    }
}
