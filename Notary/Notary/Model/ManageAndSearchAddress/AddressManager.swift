//
//  AddressManager.swift
//  LiveM
//
//  Created by Rahul Sharma on 11/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import RxCocoa
import RxSwift

class AddressManager {
    
    static let sharedInstance = AddressManager()
    var addressArrayFromCouchDB:[Any] = []
    
    let rxAddressAPICall = AddressAPI()
    let disposebag = DisposeBag()
    
    /// Method to call get list of saved address service API
    func getAddress() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            return
        }
        
        if !rxAddressAPICall.getAddressList_Response.hasObservers {
            
            rxAddressAPICall.getAddressList_Response
                .subscribe(onNext: {response in
                    
                    self.WebServiceResponse(response: response, requestType: RequestType.GetAddress)
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
        
        rxAddressAPICall.getListOfAddressesServiceAPICall()
        
    }
    
    
    
    /// Method to update manage address details in couch DB
    func updateDocumentDetailsToCouchDB() {
        
        AddressCouchDBManager.sharedInstance.updateManageAddressDetailsToCouchDBDocument(manageAddressArray: addressArrayFromCouchDB)
        
    }

    
    
    //MARK - WebService Response -
    func WebServiceResponse(response:APIResponseModel, requestType:RequestType)
    {
        if (response.data[SERVICE_RESPONSE.Error] != nil) {
            
            Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
            return
        }
       
        switch response.httpStatusCode
        {
            case HTTPSResponseCodes.SuccessResponse.rawValue:
            
                switch requestType
                {
                    case RequestType.GetAddress:
                
                        if let dataResponse:[Any] = response.data[SERVICE_RESPONSE.DataResponse] as? [Any] {
                    
                            addressArrayFromCouchDB = dataResponse
                            self.updateDocumentDetailsToCouchDB()
                        }

                    default:
                        break
                }
                break
            
            default:
                
                break
        }
        
    }
    
}
