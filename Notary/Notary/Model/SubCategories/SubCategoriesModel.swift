//
//  SubCategoriesModel.swift
//  Notary
//
//  Created by Rahul Sharma on 14/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

class SubCategoriesModel {
    
    var subCategoryId = ""
    var subCategoryName = ""
    var subCategoryDescription = ""
    var arrayOfServices = [ServicesModel]()
    
    init(subCategoriesResponse:[String:Any]) {
        
        subCategoryId = GenericUtility.strForObj(object: subCategoriesResponse["sub_cat_id"])
        subCategoryName = GenericUtility.strForObj(object: subCategoriesResponse["sub_cat_name"])
        subCategoryDescription = GenericUtility.strForObj(object: subCategoriesResponse["sub_cat_desc"])
        
        if let servicesArray = subCategoriesResponse["service"] as? [[String:Any]] {
            
            for eachService in servicesArray {
                
                let serviceModel = ServicesModel.init(serviceDetails: eachService)
                arrayOfServices.append(serviceModel)
            }
        }

    }

}
