//
//  SelectCardManager.swift
//  DayRunner
//
//  Created by Rahul Sharma on 01/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit


/// Payment Card details model
struct CardDetailsModel {
    
    var expiryYear = ""
    var last4Digits = ""
    var id = ""
    var brand = ""
    var expiryMonth = ""
    var defaultCard = false
    
    init(cardDetail:Any) {
        
        if let eachCardDetail = cardDetail as? [String:Any] {//Each card details recieved from server
            
            self.expiryYear = GenericUtility.strForObj(object:eachCardDetail["expYear"])
            self.last4Digits = GenericUtility.strForObj(object: eachCardDetail["last4"])
            self.id = GenericUtility.strForObj(object: eachCardDetail["id"])
            self.brand = GenericUtility.strForObj(object: eachCardDetail["brand"])
            self.expiryMonth = GenericUtility.strForObj(object: eachCardDetail["expMonth"])
            
            
            if let defaultCardValue = eachCardDetail["isDefault"] as? Bool  {
                
                if defaultCardValue == true {
                    
                    self.defaultCard = true
                    
                    PaymentCardManager.sharedInstance.updateDefaultCardDocumentDetailsToCouchDB(documentId: Utility.defaultCardDocId, data: eachCardDetail)
                    
                }
                
            }
        }
        
    }
    
    init(selectedCardDetails:Any) {//selected card details model from card details stored in Couch DB
        
        if let eachCardDetail = selectedCardDetails as? [String:Any] {
            
            self.expiryYear = GenericUtility.strForObj(object:eachCardDetail["expYear"])
            self.last4Digits = GenericUtility.strForObj(object: eachCardDetail["last4"])
            self.id = GenericUtility.strForObj(object: eachCardDetail["id"])
            self.brand = GenericUtility.strForObj(object: eachCardDetail["brand"])
            self.expiryMonth = GenericUtility.strForObj(object: eachCardDetail["expMonth"])
            
            if let defaultCardValue = eachCardDetail["isDefault"] as? Bool  {
                
                if defaultCardValue == true {
                    
                    self.defaultCard = true
                }
            }
        }
    }
    
}




