//
//  ChatMessageSendModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 19/12/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation


/// Chat Message Send model
class ChatMessageSendModel {
    
    var messageType = MessageType.text
    var messageTimestamp:Int64 = 0
    var messageContent = ""
    var bookingId = ""
    var musicianId = ""
    
}
