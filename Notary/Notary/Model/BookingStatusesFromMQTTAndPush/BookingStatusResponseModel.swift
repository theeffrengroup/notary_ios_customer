//
//  BookingStatusResponseModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 10/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation


/// Booking Flow Status Response Model
class BookingStatusResponseModel {
    
    var bookingId:Int64 = 0
    var bookingStatus = 0
    var bookingStatusMessage = ""
    var statusUpdatedTime = 0
    var providerCancellationReason = ""

    var musicianImageURL = ""
    
    var bookingTimerDetails:[String:Any] = [:]

    
    init(bookingStatusUpdateMessage:Any) {//MQTT booking stastus message
        
        if let bookingStatusResponse = bookingStatusUpdateMessage as? [String:Any] {
            
//            if let bId = bookingStatusResponse["bookingId"] as? String {
            
                bookingId = Int64(GenericUtility.strForObj(object:bookingStatusResponse["bookingId"]))!
//            }
            bookingStatus = Int(GenericUtility.strForObj(object:bookingStatusResponse["status"]))!
            
            statusUpdatedTime = Int(GenericUtility.strForObj(object:bookingStatusResponse["statusUpdateTime"]))!
            bookingStatusMessage = GenericUtility.strForObj(object:bookingStatusResponse["statusMsg"])
            
            musicianImageURL = GenericUtility.strForObj(object:bookingStatusResponse["proProfilePic"])
            
            if let bookingTimer = bookingStatusResponse["bookingTimer"] as? [String:Any] {
                
                bookingTimerDetails = bookingTimer
            }
            
            if let cancellationReason = bookingStatusResponse["cancellationReason"] as? String {
                
                providerCancellationReason = cancellationReason
            }
            
        }
    }
    
    init(pushbookingStatusMessage:Any) {//Push booking stastus message
        
        if let pushbookingStatusResponse = pushbookingStatusMessage as? [String:Any] {
            
            bookingId = Int64(GenericUtility.strForObj(object:pushbookingStatusResponse["bookingId"]))!
            bookingStatus = Int(GenericUtility.strForObj(object:pushbookingStatusResponse["status"]))!
            
            statusUpdatedTime = Int(GenericUtility.strForObj(object:pushbookingStatusResponse["statusUpdateTime"]))!
            bookingStatusMessage = GenericUtility.strForObj(object:pushbookingStatusResponse["statusMsg"])
            
            musicianImageURL = GenericUtility.strForObj(object:pushbookingStatusResponse["proProfilePic"])
            
            if let bookingTimer = pushbookingStatusResponse["bookingTimer"] as? [String:Any] {
                
                bookingTimerDetails = bookingTimer
            }
            
            if let cancellationReason = pushbookingStatusResponse["cancellationReason"] as? String {
                
                providerCancellationReason = cancellationReason
            }
            
        }
    }
    
    init(bookingDetailsModel:BookingDetailsModel) {//Booking Details model for Declined & Ignore Booking
        
        bookingId = bookingDetailsModel.bookingId
        bookingStatus = Booking_Status.IgnoreOrExpire.rawValue
        bookingStatusMessage = ALERTS.BOOKING_FLOW.IgnoredOrExpired
        
        statusUpdatedTime = Int(Date.dateWithDifferentTimeInterval().timeIntervalSince1970)
        
        musicianImageURL = bookingDetailsModel.providerImageURL
            
    }


}
