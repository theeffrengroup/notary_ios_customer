//
//  LiveTrackDetailsModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 03/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation


/// LiveTrack Details model
class LiveTrackDetailsModel {
    
    var providerId = ""
    var providerImageURL = ""
    var providerName = ""
    
    var providerLat = 0.0
    var providerLong = 0.0
  
    
    init(liveTrackDetails:Any) {//Booking Flow live Track details
        
        if let liveTrackDetail = liveTrackDetails as? [String:Any] {
            
           
            providerImageURL = GenericUtility.strForObj(object:liveTrackDetail["profilePic"])
            providerName = GenericUtility.strForObj(object:liveTrackDetail["firstName"])
            
            if let proId = liveTrackDetail["pid"] as? String {
                
                providerId = proId
            }
            
            
            if let proLat = liveTrackDetail["latitude"] as? Double {
                
                providerLat = proLat
            }
            
            if let proLong = liveTrackDetail["longitude"] as? Double {
                
                providerLong = proLong
            }
   
        }
    }
    
    
}
