//
//  CancelReasonModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 04/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation


/// Cancellation Reasons model
class CancelReasonModel {
    
    var reasonId = ""
    var reasonName = ""
    
    init(cancelReasonDetails:Any) {
        
        if let reason = cancelReasonDetails as? [String:Any] {
            
            reasonId = GenericUtility.strForObj(object: reason["res_id"])
            reasonName = GenericUtility.strForObj(object: reason["reason"])
            
        }
        
    }
    
}
