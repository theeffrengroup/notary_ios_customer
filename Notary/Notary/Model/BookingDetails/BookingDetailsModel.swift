//
//  BookingEventsModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 25/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation


/// Booking Details Model
class BookingDetailsModel {
    
    var address1 = ""
    var address2 = ""
    var bookingId:Int64 = 0
    var bookingStatus = 0
    var bookingModel = BookingModel.OnDemand
    var bookingStatusMessage = ""
    
    var bookingStartTime = 0
    var bookingEndTime = 0
    var eventStartTime = 0

    var providerDict:[String:Any] = [:]
    
    var providerId = ""
    var providerImageURL = ""
    var providerName = ""
    var providerPhoneNumber = ""
    var provider = ""
    var numberOfReviews = 0
    var providerRating = 4.5
    var distance = 0.0
    var providerStatus = 0
    
    var appointmentLat = 0.0
    var appointmentLong = 0.0
    
    var providerLat = 0.0
    var providerLong = 0.0
    
    var gigTimeDict:[String:Any] = [:]
    var eventDict:[String:Any] = [:]
    
    var paymentTypeText = "CASH"
    var paymentTypeValue = 1//1-Cash, 2-Card
    
    var acceptedDate = 0
    var onThewayDate = 0
    var arrivedDate = 0
    var startedDate = 0
    var invoiceRaisedDate = 0
    var completedDate = 0
    
    var timerStatus = 1
    var timerStamp = 0
    var totalTimeElapsed = 0
    
    var givenRating = 0.0
    var musicianGivenRating = 0.0
    
    var currencySymbol = ""
    
    var bookingRequestedAt = 0
    
    var mileageMatric = 0
    
    var arrayOfServices = [ServicesModel]()
    
    var cardNumber = ""
    var cardBrand = ""
    
    var cancellationFee = 0.0
    var cancellationReason = ""
    
    
    //Receipt
    var bookingTotalAmount = 0.0
    var servicesFee = 0.0
    var travelFee = 0.0
    var visitFee = 0.0
    var lastDue = 0.0
    var arrayOfAdditionlFees = [[String:Any]]()
    var arrayOfFees = [[String:Any]]()
    var isPaidByWallet = false
    var amountPaidByWallet = 0.0
    var amountPaidByCardOrCash = 0.0
    
    var signatureURL = ""

    var discountValue = 0.0
    
    var providerDistMatrix = 0
    
    init(bookingEventDetails:Any) {//My Event list booking details model
        
        if let bookingDetail = bookingEventDetails as? [String:Any] {
            
            address1 = GenericUtility.strForObj(object:bookingDetail["addLine1"])
            bookingId = Int64(GenericUtility.strForObj(object:bookingDetail["bookingId"]))!
            bookingStatus = Int(GenericUtility.strForObj(object:bookingDetail["status"]))!
            bookingStatusMessage = GenericUtility.strForObj(object: bookingDetail["statusMsg"])
            bookingStartTime = GenericUtility.intForObj(object:bookingDetail["bookingRequestedFor"])
            bookingEndTime = GenericUtility.intForObj(object:bookingDetail["bookingExpireTime"])
            bookingRequestedAt = GenericUtility.intForObj(object:bookingDetail["bookingRequestedAt"])

            eventStartTime = GenericUtility.intForObj(object:bookingDetail["eventStartTime"])

            providerId = GenericUtility.strForObj(object:bookingDetail["providerId"])
            providerImageURL = GenericUtility.strForObj(object:bookingDetail["profilePic"])
            providerName = GenericUtility.strForObj(object:bookingDetail["firstName"]) + " " + GenericUtility.strForObj(object:bookingDetail["lastName"])
            
            if providerName.hasPrefix(" ") {
                
                providerName = providerName.substring(1)
            }
            
            if providerName.hasSuffix(" ") {
                
                let endIndex = providerName.index(providerName.endIndex, offsetBy: -1)
                
                providerName = providerName.substring(to: endIndex)
            }
            
            
            providerPhoneNumber = GenericUtility.strForObj(object:bookingDetail["phone"])
            
            
            if let bookingModelData = bookingDetail["bookingModel"] as? Int {
                
                if bookingModelData == 2 {
                    
                    bookingModel = .MarketPlace
                }
            }
            
            
            if let apptLat = bookingDetail["latitude"] as? Double {
                
                appointmentLat = apptLat
            }
            
            if let apptLong = bookingDetail["longitude"] as? Double {
                
                appointmentLong = apptLong
            }
            
            if let coordinate = bookingDetail["proLocation"] as? [String:Any] {
                
                providerLat = Double(GenericUtility.strForObj(object: coordinate["latitude"]))!
                providerLong = Double(GenericUtility.strForObj(object: coordinate["longitude"]))!
            }
            
            if let event = bookingDetail["typeofEvent"] as? [String:Any] {
                
                eventDict = event
            }
            
            if let gigTime = bookingDetail["gigTime"] as? [String:Any] {
                
                gigTimeDict = gigTime
            }
            
            if let rating = bookingDetail["averageRating"] as? Double {
                
                providerRating = rating
            }
            
            if let distancevalue = bookingDetail["distance"] as? Double {
                
                distance = distancevalue
            }
            
            if let currencySym = bookingDetail["currencySymbol"] as? String {
                
                currencySymbol = currencySym
            }
            
            if let accountingDetails = bookingDetail["accounting"] as? [String:Any] {
                
                paymentTypeText = GenericUtility.strForObj(object:accountingDetails["paymentMethodText"])
                
                if let paymentType = accountingDetails["paymentMethod"] as? Int {
                    
                    paymentTypeValue = paymentType
                }
                
                if let cardNumberValue = accountingDetails["last4"] as? String {
                    
                    cardNumber = cardNumberValue
                }
                
                if let cardBrandText = accountingDetails["brand"] as? String {
                    
                    cardBrand = cardBrandText
                }
                
                if let paidByWallet = accountingDetails["paidByWallet"] as? Bool {
                    
                    isPaidByWallet = paidByWallet
                }
                
                if let walletAmount = accountingDetails["captureAmount"] as? Double {
                    
                    amountPaidByWallet = walletAmount
                }
                
                if let remainingAmount = accountingDetails["remainingAmount"] as? Double {
                    
                    amountPaidByCardOrCash = remainingAmount
                }
                
                
                //Visit Fee
                if let visitFeeAmount = accountingDetails["visitFee"] as? Double {
                    
                    visitFee = visitFeeAmount
                    
                    if visitFeeAmount > 0 {
                        
                        arrayOfFees.append([ALERTS.BOOKING_FLOW.FEES_TITLES.VisitFee: visitFeeAmount])
                    }
                }
                
                //All selected Services
                if let cartDetails = bookingDetail["cart"] as? [String:Any] {
                    
                    if let servicesArray = cartDetails["checkOutItem"] as? [[String:Any]] {
                        
                        for eachService in servicesArray {
                            
                            let serviceModel = ServicesModel.init(bookingFlowServiceDetails: eachService)
                            arrayOfServices.append(serviceModel)
                            
                            arrayOfFees.append([String(format:"%@ (x%d)", serviceModel.serviceName, serviceModel.selectedQuantity): serviceModel.serviceAmount])
                        }
                    }
                }

                
                if bookingStatus == Booking_Status.Raiseinvoice.rawValue {
                    
                    //Travel Fee Only When Booking completed
                    if let travelAmount = accountingDetails["travelFee"] as? Double {
                        
                        travelFee = travelAmount
                        
                        let travelDistance = GenericUtility.strForObj(object: accountingDetails["totalDistanceTravelText"])
                        
                        if travelAmount > 0 {
                            
                            if travelDistance.length > 0 {
                                
                                arrayOfFees.append([ALERTS.BOOKING_FLOW.FEES_TITLES.TravelFee + " (\(travelDistance))": travelAmount])

                            } else {
                                
                                arrayOfFees.append([ALERTS.BOOKING_FLOW.FEES_TITLES.TravelFee: travelAmount])

                            }
                        }
                        
                    }
                    
                    //Additional Fee Only When Booking completed
                    if let additionalFeesDetails = bookingDetail["additionalService"] as? [[String:Any]] {
                            
                        for eachAdditianalFee in additionalFeesDetails {
                            
                            arrayOfAdditionlFees.append(eachAdditianalFee)
                            arrayOfFees.append([GenericUtility.strForObj(object: eachAdditianalFee["serviceName"]): GenericUtility.floatForObj(object: eachAdditianalFee["price"])])
                        }
                    }
                }
                
                if bookingStatus == Booking_Status.CancelByCustomer.rawValue {
                    
                    if let cancellFee = accountingDetails["cancellationFee"] as? Double {
                        
                        cancellationFee = cancellFee
//                        arrayOfFees.append([ALERTS.BOOKING_FLOW.FEES_TITLES.CancellationFee: cancellFee])
                    }
                }
                
//                if bookingStatus == Booking_Status.Raiseinvoice.rawValue {
                
                if let lastDuesAmount = accountingDetails["lastDues"] as? Double {
                    
                    lastDue = lastDuesAmount
                    
                    if lastDuesAmount > 0.0 {
                        
                        arrayOfFees.append([ALERTS.BOOKING_FLOW.FEES_TITLES.LastDue: lastDuesAmount])
                    }
                }
//                }
                
                if let discount = accountingDetails["discount"] as? Double {
                    
                    discountValue = discount
                    
                    if discount > 0 {
                        
                        arrayOfFees.append([ALERTS.BOOKING_FLOW.FEES_TITLES.Discount: discount])
                    }
                }
                
                
                if let totalAmount = accountingDetails["total"] as? Double {
                    
                    bookingTotalAmount = totalAmount
                }
            
            }
            
            if let canResason = bookingDetail["cancellationReason"] as? String {
                
                cancellationReason = canResason
            }
            
            if let mileageMatricValue = bookingDetail["mileageMatric"] as? Int {
                
                mileageMatric = mileageMatricValue
            }
            
            if let proDetail = bookingDetail["reviewByProvider"] as? [String:Any] {
                
                if let rating = proDetail["rating"] as? Double {
                    
                    musicianGivenRating = rating
                }
            }
            
            if let proDetail = bookingDetail["reviewByCustomer"] as? [String:Any] {
                
                if let rating = proDetail["rating"] as? Double {
                    
                    givenRating = rating
                }
            }
           
            

        }
    }
    
    init(bookingDetails:Any) {//Booking flow booking details model
        
        if let bookingDetail = bookingDetails as? [String:Any] {
            
            address1 = GenericUtility.strForObj(object:bookingDetail["addLine1"])
            bookingId = Int64(GenericUtility.strForObj(object:bookingDetail["bookingId"]))!
            bookingStatus = Int(GenericUtility.strForObj(object:bookingDetail["status"]))!
            bookingStatusMessage = GenericUtility.strForObj(object: bookingDetail["statusMsg"])
            bookingStartTime = GenericUtility.intForObj(object:bookingDetail["bookingRequestedFor"])
            bookingEndTime = GenericUtility.intForObj(object:bookingDetail["bookingExpireTime"])
            bookingRequestedAt = GenericUtility.intForObj(object:bookingDetail["bookingRequestedAt"])

            signatureURL = GenericUtility.strForObj(object:bookingDetail["signURL"])
            
            if let bookingModelData = bookingDetail["bookingModel"] as? Int {
                
                if bookingModelData == 2 {
                    
                    bookingModel = .MarketPlace
                }
            }
            
            if let currencySym = bookingDetail["currencySymbol"] as? String {
                
                currencySymbol = currencySym
            }
            
            if let proDetail = bookingDetail["providerDetail"] as? [String:Any] {
                
                providerDict = proDetail
                
                if let coordinate = proDetail["proLocation"] as? [String:Any] {
                    
                    providerLat = Double(GenericUtility.strForObj(object: coordinate["latitude"])) ?? 0.0
                    providerLong = Double(GenericUtility.strForObj(object: coordinate["longitude"])) ?? 0.0
                }
                
                
                
                if let rating = proDetail["averageRating"] as? Double {
                    
                    providerRating = rating
                }
                
                if let distancevalue = proDetail["distance"] as? Double {
                    
                    distance = distancevalue
                }
                
                if let reviewCount = proDetail["reviewCount"] as? Int {
                    
                    numberOfReviews = reviewCount
                }
                
                if let statusValue = proDetail["proStatus"] as? Int {
                    
                    providerStatus = statusValue
                }
                
                providerId = GenericUtility.strForObj(object:proDetail["providerId"])
                providerImageURL = GenericUtility.strForObj(object:proDetail["profilePic"])
                providerName = GenericUtility.strForObj(object:proDetail["firstName"]) + " " + GenericUtility.strForObj(object:proDetail["lastName"])
                providerDistMatrix = GenericUtility.intForObj(object:proDetail["distanceMatrix"]) 
                
                
                
                if providerName.hasPrefix(" ") {
                    
                    providerName = providerName.substring(1)
                }
                
                if providerName.hasSuffix(" ") {
                    
                    let endIndex = providerName.index(providerName.endIndex, offsetBy: -1)
                    
                    providerName = providerName.substring(to: endIndex)
                }
                
                providerPhoneNumber = GenericUtility.strForObj(object:proDetail["phone"])
                
            }
            
            if let apptLat = bookingDetail["latitude"] as? Double {
                
                appointmentLat = apptLat
            }
            
            if let apptLong = bookingDetail["longitude"] as? Double {
                
                appointmentLong = apptLong
            }
            
            if let event = bookingDetail["typeofEvent"] as? [String:Any] {
                
                eventDict = event
            }
            
            if let gigTime = bookingDetail["gigTime"] as? [String:Any] {
                
                gigTimeDict = gigTime
            }
            
            if let statusTimeDict = bookingDetail["jobStatusLogs"] as? [String:Any] {
                
                if let acceptedDateValue = statusTimeDict["acceptedTime"] as? Int {
                    
                    acceptedDate = acceptedDateValue
                }
                
                if let onthewayDateValue = statusTimeDict["onthewayTime"] as? Int {
                    
                    onThewayDate = onthewayDateValue
                }
                
                if let arrivedDateValue = statusTimeDict["arrivedTime"] as? Int {
                    
                    arrivedDate = arrivedDateValue
                }
                
                if let startedDateValue = statusTimeDict["startedTime"] as? Int {
                    
                    startedDate = startedDateValue
                }
                
                if let completedDateValue = statusTimeDict["completedTime"] as? Int {
                    
                    completedDate = completedDateValue
                }
                
                if let invoiceRaisedDateValue = statusTimeDict["raiseInvoiceTime"] as? Int {
                    
                    invoiceRaisedDate = invoiceRaisedDateValue
                }
                
                
            }
            
            if let bookingTimerDetails = bookingDetail["bookingTimer"] as? [String:Any] {
                
                if let timerStatusValue = bookingTimerDetails["status"] as? Int {
                    
                    timerStatus = timerStatusValue
                }
                
                if let timerStampValue = bookingTimerDetails["startTimeStamp"] as? Int {
                    
                    timerStamp = timerStampValue
                }
                
                if let totalTimeElapsedValue = bookingTimerDetails["second"] as? Int {
                    
                    totalTimeElapsed = totalTimeElapsedValue
                }
                
                
            }
            
            
            if let completedBookingDetails = bookingDetail["completedDetails"] as? [String:Any] {
                
                if let signatureURLValue = completedBookingDetails["signURL"] as? String {
                    
                    signatureURL = signatureURLValue
                }
                
            }
            
            if let proDetail = bookingDetail["reviewByCustomer"] as? [String:Any] {
                
                if let rating = proDetail["rating"] as? Double {
                    
                    givenRating = rating
                }
            }
            
            if let proDetail = bookingDetail["reviewByProvider"] as? [String:Any] {
                
                if let rating = proDetail["rating"] as? Double {
                    
                    musicianGivenRating = rating
                }
            }
        
            
            if let accountingDetails = bookingDetail["accounting"] as? [String:Any] {
                
                paymentTypeText = GenericUtility.strForObj(object:accountingDetails["paymentMethodText"])
                
                if let paymentType = accountingDetails["paymentMethod"] as? Int {
                    
                    paymentTypeValue = paymentType
                }
                
                if let cardNumberValue = accountingDetails["last4"] as? String {
                    
                    cardNumber = cardNumberValue
                }
                
                if let cardBrandText = accountingDetails["brand"] as? String {
                    
                    cardBrand = cardBrandText
                }
                
                if let paidByWallet = accountingDetails["paidByWallet"] as? Bool {
                    
                    isPaidByWallet = paidByWallet
                }
                
                if let walletAmount = accountingDetails["captureAmount"] as? Double {
                    
                    amountPaidByWallet = walletAmount
                }
                
                if let remainingAmount = accountingDetails["remainingAmount"] as? Double {
                    
                    amountPaidByCardOrCash = remainingAmount
                }
                
                
                //Visit Fee
                if let visitFeeAmount = accountingDetails["visitFee"] as? Double {
                    
                    visitFee = visitFeeAmount
                    
                    if visitFeeAmount > 0 {
                        
                        arrayOfFees.append([ALERTS.BOOKING_FLOW.FEES_TITLES.VisitFee: visitFeeAmount])
                    }
                }
                
                //All selected Services
                if let cartDetails = bookingDetail["cart"] as? [String:Any] {
                    
                    if let servicesArray = cartDetails["checkOutItem"] as? [[String:Any]] {
                        
                        for eachService in servicesArray {
                            
                            let serviceModel = ServicesModel.init(bookingFlowServiceDetails: eachService)
                            arrayOfServices.append(serviceModel)
                            
                            arrayOfFees.append([String(format:"%@ (x%d)", serviceModel.serviceName, serviceModel.selectedQuantity): serviceModel.serviceAmount])
                        }
                    }
                }
                
                
                if bookingStatus == Booking_Status.Raiseinvoice.rawValue {
                    
                    //Travel Fee Only When Booking completed
                    if let travelAmount = accountingDetails["travelFee"] as? Double {
                        
                        travelFee = travelAmount
                        
                        let travelDistance = GenericUtility.strForObj(object: accountingDetails["totalDistanceTravelText"])
                        
                        if travelAmount > 0 {
                            
                            if travelDistance.length > 0 {
                                
                                arrayOfFees.append([ALERTS.BOOKING_FLOW.FEES_TITLES.TravelFee + " (\(travelDistance))": travelAmount])
                                
                            } else {
                                
                                arrayOfFees.append([ALERTS.BOOKING_FLOW.FEES_TITLES.TravelFee: travelAmount])
                                
                            }
                        }
                        
                    }
                    
                    //Additional Fee Only When Booking completed
                    if let additionalFeesDetails = bookingDetail["additionalService"] as? [[String:Any]] {
                        
                        for eachAdditianalFee in additionalFeesDetails {
                            
                            arrayOfAdditionlFees.append(eachAdditianalFee)
                            arrayOfFees.append([GenericUtility.strForObj(object: eachAdditianalFee["serviceName"]): GenericUtility.floatForObj(object: eachAdditianalFee["price"])])
                        }
                    }
                }
                
                if bookingStatus == Booking_Status.CancelByCustomer.rawValue {
                    
                    if let cancellFee = accountingDetails["cancellationFee"] as? Double {
                        
                        cancellationFee = cancellFee
//                        arrayOfFees.append([ALERTS.BOOKING_FLOW.FEES_TITLES.CancellationFee: cancellFee])
                    }
                }
                
//                if bookingStatus == Booking_Status.Raiseinvoice.rawValue {
                
                    if let lastDuesAmount = accountingDetails["lastDues"] as? Double {
                        
                        lastDue = lastDuesAmount
                        
                        if lastDuesAmount > 0.0 {
                            
                            arrayOfFees.append([ALERTS.BOOKING_FLOW.FEES_TITLES.LastDue: lastDuesAmount])
                        }
                    }
//                }
                
                if let discount = accountingDetails["discount"] as? Double {
                    
                    discountValue = discount
                    
                    if discount > 0 {
                        
                        arrayOfFees.append([ALERTS.BOOKING_FLOW.FEES_TITLES.Discount: discount])
                    }
                }
                
                
                if let totalAmount = accountingDetails["total"] as? Double {
                    
                    bookingTotalAmount = totalAmount
                }
                
            }
            
            
            if let canResason = bookingDetail["cancellationReason"] as? String {
                
                cancellationReason = canResason
            }
            
            if let mileageMatricValue = bookingDetail["mileageMatric"] as? Int {
                
                mileageMatric = mileageMatricValue
            }
            
            if let cartDetails = bookingDetail["cart"] as? [String:Any] {
                
                if let servicesArray = cartDetails["checkOutItem"] as? [[String:Any]] {
                    
                    for eachService in servicesArray {
                        
                        let serviceModel = ServicesModel.init(bookingFlowServiceDetails: eachService)
                        arrayOfServices.append(serviceModel)
                    }
                }
            }
        }
    }

    init(bookingInvoiceDetails:Any) {//Invoice booking details model
        
        if let bookingDetail = bookingInvoiceDetails as? [String:Any] {
            
            address1 = GenericUtility.strForObj(object:bookingDetail["addLine1"])
            bookingId = Int64(GenericUtility.strForObj(object:bookingDetail["bookingId"]))!
            bookingStatus = Int(GenericUtility.strForObj(object:bookingDetail["status"]))!
            bookingStatusMessage = GenericUtility.strForObj(object: bookingDetail["statusMsg"])
            bookingStartTime = GenericUtility.intForObj(object:bookingDetail["bookingRequestedFor"])
            bookingRequestedAt = GenericUtility.intForObj(object:bookingDetail["bookingRequestedAt"])

            signatureURL = GenericUtility.strForObj(object:bookingDetail["signURL"])
            
            if let bookingModelData = bookingDetail["bookingModel"] as? Int {
                
                if bookingModelData == 2 {
                    
                    bookingModel = .MarketPlace
                }
            }
            
            if let currencySym = bookingDetail["currencySymbol"] as? String {
                
                currencySymbol = currencySym
            }
            
            if let proDetail = bookingDetail["providerData"] as? [String:Any] {
                
                providerDict = proDetail
                
                if let coordinate = proDetail["location"] as? [String:Any] {
                    
                    providerLat = Double(GenericUtility.strForObj(object: coordinate["latitude"]))!
                    providerLong = Double(GenericUtility.strForObj(object: coordinate["longitude"]))!
                }
                
                if let rating = proDetail["averageRating"] as? Double {
                    
                    providerRating = rating
                }
                
                if let ppic = proDetail["profilePic"] as? String {
                    
                    providerImageURL = ppic
                }
            

                providerName = GenericUtility.strForObj(object:proDetail["firstName"]) + " " + GenericUtility.strForObj(object:proDetail["lastName"])
                
                if providerName.hasPrefix(" ") {
                    
                    providerName = providerName.substring(1)
                }
                
                if providerName.hasSuffix(" ") {
                    
                    let endIndex = providerName.index(providerName.endIndex, offsetBy: -1)
                    
                    providerName = providerName.substring(to: endIndex)
                }
                
                providerPhoneNumber = GenericUtility.strForObj(object:proDetail["phone"])
                
            }
            
            if let event = bookingDetail["typeofEvent"] as? [String:Any] {
                
                eventDict = event
            }
            
            if let gigTime = bookingDetail["service"] as? [String:Any] {
                
                gigTimeDict = gigTime
            }
            
            
            if let accountingDetails = bookingDetail["accounting"] as? [String:Any] {
                
                paymentTypeText = GenericUtility.strForObj(object:accountingDetails["paymentMethodText"])
                
                
                if let paymentType = accountingDetails["paymentMethod"] as? Int {
                    
                    paymentTypeValue = paymentType
                }
                
                if let cardNumberValue = accountingDetails["last4"] as? String {
                    
                    cardNumber = cardNumberValue
                }
                
                if let cardBrandText = accountingDetails["brand"] as? String {
                    
                    cardBrand = cardBrandText
                }
                
                if let paidByWallet = accountingDetails["paidByWallet"] as? Bool {
                    
                    isPaidByWallet = paidByWallet
                }
                
                if let walletAmount = accountingDetails["captureAmount"] as? Double {
                    
                    amountPaidByWallet = walletAmount
                }
                
                if let remainingAmount = accountingDetails["remainingAmount"] as? Double {
                    
                    amountPaidByCardOrCash = remainingAmount
                }
                
                
                //Visit Fee
                if let visitFeeAmount = accountingDetails["visitFee"] as? Double {
                    
                    visitFee = visitFeeAmount
                    
                    if visitFeeAmount > 0 {
                        
                        arrayOfFees.append([ALERTS.BOOKING_FLOW.FEES_TITLES.VisitFee: visitFeeAmount])
                    }
                }
                
                //All selected Services
                if let cartDetails = bookingDetail["cartData"] as? [String:Any] {
                    
                    if let servicesArray = cartDetails["checkOutItem"] as? [[String:Any]] {
                        
                        for eachService in servicesArray {
                            
                            let serviceModel = ServicesModel.init(bookingFlowServiceDetails: eachService)
                            arrayOfServices.append(serviceModel)
                            
                            arrayOfFees.append([String(format:"%@ (x%d)", serviceModel.serviceName, serviceModel.selectedQuantity): serviceModel.serviceAmount])
                        }
                    }
                }
                
                
                if bookingStatus == Booking_Status.Raiseinvoice.rawValue {
                    
                    //Travel Fee Only When Booking completed
                    if let travelAmount = accountingDetails["travelFee"] as? Double {
                        
                        travelFee = travelAmount
                        
                        let travelDistance = GenericUtility.strForObj(object: accountingDetails["totalDistanceTravelText"])
                        
                        if travelAmount > 0 {
                            
                            if travelDistance.length > 0 {
                                
                                arrayOfFees.append([ALERTS.BOOKING_FLOW.FEES_TITLES.TravelFee + " (\(travelDistance))": travelAmount])
                                
                            } else {
                                
                                arrayOfFees.append([ALERTS.BOOKING_FLOW.FEES_TITLES.TravelFee: travelAmount])
                                
                            }
                        }
                        
                    }
                    
                    //Additional Fee Only When Booking completed
                    if let additionalFeesDetails = bookingDetail["additionalService"] as? [[String:Any]] {
                        
                        for eachAdditianalFee in additionalFeesDetails {
                            
                            arrayOfAdditionlFees.append(eachAdditianalFee)
                            arrayOfFees.append([GenericUtility.strForObj(object: eachAdditianalFee["serviceName"]): GenericUtility.floatForObj(object: eachAdditianalFee["price"])])
                        }
                    }
                }
                
                if bookingStatus == Booking_Status.CancelByCustomer.rawValue {
                    
                    if let cancellFee = accountingDetails["cancellationFee"] as? Double {
                        
                        cancellationFee = cancellFee
//                        arrayOfFees.append([ALERTS.BOOKING_FLOW.FEES_TITLES.CancellationFee: cancellFee])
                    }
                }
                
//                if bookingStatus == Booking_Status.Raiseinvoice.rawValue {
                
                    if let lastDuesAmount = accountingDetails["lastDues"] as? Double {
                        
                        lastDue = lastDuesAmount
                        
                        if lastDuesAmount > 0.0 {
                            
                            arrayOfFees.append([ALERTS.BOOKING_FLOW.FEES_TITLES.LastDue: lastDuesAmount])
                        }
                    }
//                }
                
                if let discount = accountingDetails["discount"] as? Double {
                    
                    discountValue = discount
                    
                    if discount > 0 {
                        
                        arrayOfFees.append([ALERTS.BOOKING_FLOW.FEES_TITLES.Discount: discount])
                    }
                }
                
                
                if let totalAmount = accountingDetails["total"] as? Double {
                    
                    bookingTotalAmount = totalAmount
                }
                
            }
            
            if let cardNumberValue = bookingDetail["last4"] as? String {
                
                cardNumber = cardNumberValue
            }
            
            if let proDetail = bookingDetail["reviewByCustomer"] as? [String:Any] {
                
                if let rating = proDetail["rating"] as? Double {
                    
                    givenRating = rating
                }
            }
            
            if let proDetail = bookingDetail["reviewByProvider"] as? [String:Any] {
                
                if let rating = proDetail["rating"] as? Double {
                    
                    musicianGivenRating = rating
                }
            }
            
            if let mileageMatricValue = bookingDetail["mileageMatric"] as? Int {
                
                mileageMatric = mileageMatricValue
            }
            
            if let cartDetails = bookingDetail["cartData"] as? [String:Any] {
                
                if let servicesArray = cartDetails["checkOutItem"] as? [[String:Any]] {
                    
                    for eachService in servicesArray {
                        
                        let serviceModel = ServicesModel.init(bookingFlowServiceDetails: eachService)
                        arrayOfServices.append(serviceModel)
                    }
                }
            }
        }
    }
}
