//
//  ChatMessageResponseModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 19/12/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation


/// Chat Message Send model
class ChatMessageRecievedModel {
    
    var messageType = MessageType.text
    var messageTimestamp = 0
    var messageContent = ""
    var bookingId = ""
    var musicianId = ""
    var musicianProfilePicURL = ""
    var musicianName = ""
    
    init(chatMessageRecieved:Any) {
        
        if let chatMessage = chatMessageRecieved as? [String:Any] {
            
            if let chatMessageType = chatMessage["type"] as? NSNumber {
                
                switch chatMessageType {
                    
                    case 2:
                        messageType = MessageType.photo
                    
                    case 3:
                        
                        break
                    
                    default:
                        
                        messageType = MessageType.text
                        break
                }

            }
            
            if let content = chatMessage["content"] as? String {
                
                messageContent = content
            }
            
            if let timestamp = chatMessage["timestamp"] as? NSNumber {
                
                messageTimestamp = Int(timestamp)
            }
            
            if let musId = chatMessage["fromID"] as? String {
                
                musicianId = musId
            }
            
            if let bid = chatMessage["bid"] as? String {
                
                bookingId = bid
            }
            
            if let profilePic = chatMessage["profilePic"] as? String {
                
                musicianProfilePicURL = profilePic
            }
            
            if let name = chatMessage["name"] as? String {
                
                musicianName = name
            }
        }
    }
}
